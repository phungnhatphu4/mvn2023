import Link from 'next/link';

const IconArrow = () => {
  return (
    <svg className="mr-2" width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0.5 0.5L5 5L0.5 9.5" stroke="white" strokeMiterlimit="10" strokeLinecap="round" strokeinejoin="round" />
    </svg>
  )
}

export const Breadcrumb = ({ listBreadcrumb = [] }) => {
  return (
    <div className="w-full bg-[#1f1f21]">
      <div className="container">
        <nav className="hidden md:flex py-[12px]" aria-label="Breadcrumb">
          <ol className="inline-flex items-center space-x-1 md:space-x-3 text-[12px] text-white">
            {
              listBreadcrumb.map((item, index) => {
                if (index === 0) {
                  return (
                    <li className="inline-flex items-center" key={item.link}>
                      <Link href={item.link}>
                        <a className="inline-flex items-center font-medium text-[#ffffff99] dark:hover:text-white">
                          {item.name}
                        </a>
                      </Link>
                    </li>
                  )
                }
                if (index === (listBreadcrumb.length - 1)) {
                  return (
                    <li className="inline-flex items-center" key={item.name}>
                      <IconArrow />
                      <span>{item.name}</span>
                    </li>)
                } else {
                  return (
                    <li className="inline-flex items-center" key={item.link}>
                      <Link href={item.link}>
                        <a className="inline-flex items-center font-medium text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                          <IconArrow />
                          {item.name}
                        </a>
                      </Link>
                    </li>
                  )
                }
              })
            }
          </ol>
        </nav>
      </div>
    </div>
  )
}