import { useStep } from 'hooks/useStep'
import clsx from 'clsx'

const STEPS = [
  {
    id: 1,
    label: '1. Chọn Chi Nhánh',
  },
  {
    id: 2,
    label: '2. Chọn Vị Trí',
  },
  {
    id: 3,
    label: '3. Chọn Set Menu',
  },
  {
    id: 4,
    label: '4. Chọn Dịch Vụ',
  },
  {
    id: 5,
    label: '5. Thanh Toán',
  },
]

const StepProgressBarVertical = () => {
  const { currentStep } = useStep()
  return (
    <div className="bg-[#000000] px-6 py-8 bg-opacity-50 rounded-3xl text-white">
      <ul className="relative pl-[24px] before:inline-block before:content-[''] before:absolute before:top-[50%] before:-translate-y-1/2 before:left-[0px] before:h-[calc(100%_-_14px)] before:w-[2px] before:bg-[#D9D9D9]">
        {STEPS.map((step) => {
          return (
            <li
              key={step.id}
              className={clsx(
                "relative pb-12 last:pb-0 after:content-[''] after:absolute after:inline-block after:top-[5px] after:left-[-29px] after:w-3 after:h-3 after:bg-white after:rounded-full select-none",
                step.id <= currentStep && 'after:bg-primary-linear',
                step.id < currentStep &&
                  "before:inline-block before:content-[''] before:absolute before:top-[7px] before:left-[-24px] before:h-full before:w-[2px] before:bg-primary-linear",
              )}
            >
              <strong
                className={clsx(
                  'text-base',
                  step.id <= currentStep && 'text-[#F0C753]',
                )}
              >
                {step.label}
              </strong>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default StepProgressBarVertical
