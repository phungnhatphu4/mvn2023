import Button from 'components/common/Button/Button'
import { isEmpty } from 'lodash'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectPositionData, selectStep1Data } from 'selectors/booking.selector'
import {
  getListPositions,
  getListBookedPositions,
} from 'services/position.service'
import { bookingActions } from 'slices/booking'
import Title from '../Title/Title'
import PositionInfo from './PositionInfo/PositionInfo'
import SwiperSelectLocation from './Swiper/SwiperSelectLocation'

const ChoosePosition = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const positionData = useSelector(selectPositionData)
  const [listPositions, setListPositions] = useState([])
  const [activePosition, setActivePosition] = useState(positionData)
  const dataStep1 = useSelector(selectStep1Data)

  useEffect(() => {
    if (isEmpty(dataStep1)) router.push('/dat-ban/chon-chi-nhanh')
    else {
      fetchListPositions()
    }
  }, [])
  const handleChangeActivePosition = (position) => {
    setActivePosition(position)
  }

  const fetchListPositions = async () => {
    const { data: dataPosition } = await getListPositions(
      dataStep1?.selectedBranch?.id,
    ) // can them branchId ở step 1
    const { data: dataBookedPosition } = await getListBookedPositions(
      dataStep1?.selectedBranch?.id,
      dataStep1?.checkInTime,
    ) // can them branchId, check_in_time ở step 1

    console.log(dataBookedPosition);
    const positions = dataPosition.list_deals.data
    console.log('position----');
	console.log(positions);
    const bookedPositions = dataBookedPosition.list_served_positions.data

    console.log(bookedPositions);
    const convertDataPositions = positions.map((position) => {
      const bookedPosition = bookedPositions.find(
        (p) => (p.deal_id = position.id),
      )

      console.log('position2----');
      console.log(positions);
      const numberOfAvailableTables =
        position.number_of_tables - (bookedPosition?.booked_count ?? 0)

        console.log('posnumberOfAvailableTables----');
        console.log(numberOfAvailableTables);
      return {
        id: position.id,
        name: position.name,
        imageUrls: position.image_urls,
        numberOfTables: position.number_of_tables,
        numberOfAvailableTables,
      }
    })
    
    setListPositions(convertDataPositions)
    const index = convertDataPositions.findIndex(
      (position) => position.id === activePosition?.id,
    )
    if (index === -1) {
      setActivePosition(convertDataPositions[0])
    }
  }

  return (
    <div>
      <div className="flex justify-center md:justify-start mb-4 md:mb-8">
        <Title title="Chọn vị trí" checked={!!activePosition} />
      </div>
      <div className="-mr-3 mb-4 min-w-0 lg:mr-0">
        <SwiperSelectLocation
          listPositions={listPositions}
          activePosition={activePosition}
          onChangeActivePosition={handleChangeActivePosition}
        />
      </div>
      <div className="mb-6">
        <PositionInfo activePosition={activePosition} />
      </div>
      <div className="text-center">
        <Button
          className="text-[14px] leading-[16.8px] text-white w-full py-[14px] md:w-[350px] "
          disabled={!activePosition?.numberOfAvailableTables}
          onClick={() => {
            dispatch(bookingActions.setChoosePosition(activePosition))
            router.push('/dat-ban/chon-menu')
          }}
        >
          2/5 Chọn vị trí này
        </Button>
      </div>
    </div>
  )
}

export default ChoosePosition
