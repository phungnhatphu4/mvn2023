import SwiperLocationImages from '../Swiper/SwiperLocationImages'

const PositionInfo = ({ activePosition }) => {
  return (
    <div className="bg-[#444444] rounded-xl pt-4 pb-5">
      <div className="text-center mb-4">
        <div className="text-[#F0C753] text-xl leading-6 mb-1">
          {activePosition?.name}
        </div>
        <div className="text-white text-sm leading-[17px]">
          Tình trạng: {activePosition?.numberOfAvailableTables} bàn trống
        </div>
      </div>
      <div>
        <SwiperLocationImages images={activePosition?.imageUrls} />
      </div>
    </div>
  )
}

export default PositionInfo
