import clsx from 'clsx'

const SwiperSelectLocation = ({
  listPositions = [],
  activePosition,
  onChangeActivePosition,
}) => {
  return (
    <div>
      <div className="flex items-center overflow-x-auto gap-x-3">
        {listPositions.map((position) => {
          return (
            <div
              className={clsx(
                'rounded-lg overflow-hidden flex-shrink-0 cursor-pointer',
                position.id === activePosition.id
                  ? 'w-[63px] border border-[#F0C753] lg:w-[120px]'
                  : 'w-[50px] lg:w-[96px]',
              )}
              key={position.id}
              onClick={() => {
                onChangeActivePosition(position)
              }}
            >
              <div
                className={clsx(
                  'w-full pt-[100%] relative',
                  position.numberOfAvailableTables === 0 && 'grayscale',
                )}
              >
                <div className="absolute top-0 w-full h-full">
                  <img
                    src={position?.imageUrls?.[0]}
                    alt={position.name}
                    className="w-full h-full object-cover"
                  />
                </div>
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default SwiperSelectLocation
