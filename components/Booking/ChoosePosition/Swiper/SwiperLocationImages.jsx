import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation } from 'swiper'
import clsx from 'clsx'

const SwiperLocationImages = ({ images = [], onSlideChange }) => {
  return (
    <div>
      <Swiper
        slidesPerView="auto"
        spaceBetween={-16}
        centeredSlides
        loop={images.length > 2}
        breakpoints={{
          320: {
            slidesPerView: 1.5,
            spaceBetween: -16,
          },
          1024: {
            slidesPerView: 2.5,
            spaceBetween: -16,
          },
        }}
        modules={[Navigation]}
        onSlideChange={onSlideChange}
        slideToClickedSlide
      >
        {images.map((slide, index) => {
          return (
            <SwiperSlide key={index}>
              {({ isActive }) => {
                return (
                  <div
                    className={clsx(
                      'w-full pt-[75%] relative rounded-xl border border-[#F0C753] overflow-hidden',
                      isActive ? 'scale-100' : 'scale-[75%]',
                    )}
                  >
                    <div className="absolute top-0 w-full h-full">
                      <img
                        src={slide}
                        alt=""
                        className="w-full h-full object-cover"
                      />
                    </div>
                  </div>
                )
              }}
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  )
}

export default SwiperLocationImages
