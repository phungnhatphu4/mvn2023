import Button from 'components/common/Button/Button'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectMenuData } from 'selectors/booking.selector'
import { getListMenus } from 'services/menu.service'
import { bookingActions } from 'slices/booking'
import Title from '../Title/Title'
import SwiperChooseMenu from './Swiper/SwiperChooseMenu'
import SwiperMenuImages from './Swiper/SwiperMenuImages'

const ChooseMenu = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const menuData = useSelector(selectMenuData)
  const [listMenus, setListMenus] = useState([])
  const [activeMenu, setActiveMenu] = useState({})
  useEffect(() => {
    fetchListMenus()
  }, [])

  const fetchListMenus = async () => {
    const { data } = await getListMenus() // can them branchId ở step 1
    const menus = data.list_deals.data
    const convertDataMenus = menus.map((menu) => {
      return {
        id: menu.id,
        name: menu.name,
        description: menu.description,
        imageUrls: menu.image_urls,
        price: menu.price,
        salePrice: menu.sale_price,
      }
    })
    setListMenus(convertDataMenus)
    setActiveMenu(convertDataMenus[0])
  }

  return (
    <div>
      <div className="flex justify-center md:justify-start mb-4 md:mb-8">
        <Title title="Chọn set menu" checked={!!activeMenu} />
      </div>
      <div>
        <div className="mb-4 min-w-0">
          <SwiperChooseMenu
            menus={listMenus}
            onSlideChange={(e) => {
              setActiveMenu(listMenus[e.realIndex])
            }}
          />
        </div>
        <div className="mb-4">
          <SwiperMenuImages images={activeMenu?.imageUrls} />
        </div>
        <div className="text-[14px] leading-[21px] text-[#BDBDBD] mb-3">
          {activeMenu?.description}
        </div>
        <div className="flex justify-between text-[20px] leading-[24px] text-[#F0C753] mb-6">
          <div>Giá:</div>
          <div>{activeMenu?.salePrice?.toLocaleString('de-DE')}đ</div>
        </div>
        <div className='text-center'>
          <Button
            disabled={!activeMenu}
            className="text-[14px] leading-[16.8px] text-white w-full py-[14px] md:w-[350px]"
            onClick={() => {
              dispatch(bookingActions.setChooseMenu(activeMenu))
              router.push('/dat-ban/chon-dich-vu')
            }}
          >
            3/5 Chọn set menu này
          </Button>
        </div>
      </div>
    </div>
  )
}

export default ChooseMenu
