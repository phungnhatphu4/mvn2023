import { Swiper, SwiperSlide } from 'swiper/react'
import { EffectCoverflow } from 'swiper'

const SwiperMenuImages = ({ images = [] }) => {
  if (!images.length) return null
  return (
    <div>
      <Swiper
        effect="coverflow"
        grabCursor={true}
        centeredSlides={true}
        slidesPerView="auto"
        // loop
        coverflowEffect={{
          rotate: 0,
          stretch: 210,
          depth: 300,
          modifier: 1,
          slideShadows: true,
        }}
        modules={[EffectCoverflow]}
      >
        {images.map((url, index) => {
          return (
            <SwiperSlide key={index} className="max-w-[275px]">
              <div className="max-w-[275px] flex-col">
                <div className="pt-[100%] relative">
                  <div className="absolute top-0 w-full h-full">
                    <img
                      src={url}
                      alt="menu"
                      className="w-full h-full object-contain"
                    />
                  </div>
                </div>
              </div>
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  )
}

export default SwiperMenuImages
