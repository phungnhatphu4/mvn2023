import {
  ArrowDropDownIcon,
  ArrowDropUpIcon,
  LiftHeartIcon,
  PickHeartIcon,
  UnpickHeartIcon,
} from 'assets/index'
import clsx from 'clsx'
import Collapsible from 'react-collapsible'
import { useState } from 'react'
import ClientOnly from 'components/ClientOnly'
import Image from 'next/image'

const ServiceItem = ({
  hasSelected = false,
  serviceName = '',
  description = '',
  children,
  price,
  thumbnail,
}) => {
  const [open, setOpen] = useState(false)

  return (
    <div className="border-b border-white border-opacity-50 pb-2">
      <div className="flex items-center">
        <div className="mr-2">
          {hasSelected ? <PickHeartIcon /> : <UnpickHeartIcon />}
        </div>
        <div className="flex-1">
          <div className="flex">
            <div className="w-[66px] h-[50px] overflow-hidden rounded-xl mr-2">
              <Image src={thumbnail} className="w-full h-full object-cover" />
            </div>
            <div className="flex-1">
              <div className="flex items-center">
                <div className="mr-1">
                  <LiftHeartIcon />
                </div>
                <div className="text-[13px] leading-[18.2px] text-white">
                  {serviceName}
                </div>
              </div>
              <div className="text-[10px] leading-[14px] text-white">
                {description}
              </div>
            </div>
          </div>
          {hasSelected && (
            <div className="text-[13px] leading-[19.5px] text-white text-right mt-1">
              {price?.toLocaleString('de-DE')}đ
            </div>
          )}
        </div>
        <div className="flex">
          <button className="p-2" onClick={() => setOpen(!open)}>
            {open ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
          </button>
        </div>
      </div>
      <ClientOnly>
        <Collapsible open={open}>
          <div
            className={clsx(
              open && 'border-t border-white border-opacity-50 mt-2',
              'overflow-hidden ml-[28px]',
            )}
            style={{
              transition: 'all ease 0.5s',
            }}
          >
            {children}
          </div>
        </Collapsible>
      </ClientOnly>
    </div>
  )
}

export default ServiceItem
