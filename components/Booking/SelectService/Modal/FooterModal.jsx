import { MoneyIcon, PickHeartIcon, UnpickHeartIcon } from 'assets/index'
import Button from 'components/common/Button/Button'

const FooterModal = ({ isDisabledButton = true, price = 0, onClickButton }) => {
  return (
    <div className="flex justify-between items-center">
      <div>
        <Button
          className="flex"
          disabled={isDisabledButton}
          onClick={onClickButton}
        >
          <span className="mr-1">Chọn</span>
          <span>
            {isDisabledButton ? <UnpickHeartIcon /> : <PickHeartIcon />}
          </span>
        </Button>
      </div>
      <div className="flex items-center">
        <div className="mr-2">
          <MoneyIcon />
        </div>
        <div className="text-[13px] leading-[15.6px] font-medium">
          {price?.toLocaleString('de-DE')}đ
        </div>
      </div>
    </div>
  )
}

export default FooterModal
