import { PickHeartIcon, UnpickHeartIcon } from 'assets/index'
import RadioButton from 'components/common/RadioButton/RadioButton'
import ModalCommon from 'components/Modal/ModalCommon'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectSelectedCake } from 'selectors/booking.selector'
import SwiperChooseCake from '../Swiper/SwiperChooseCake'
import FooterModal from './FooterModal'
import { bookingActions } from 'slices/booking'

const ModalSelectCake = ({ isOpen = false, closeModal, cakeData = [] }) => {
  const selectedCake = useSelector(selectSelectedCake)
  const dispatch = useDispatch()
  const [currentCake, setCurrentCake] = useState(null)
  const [currentVariant, setCurrentVariant] = useState(null)
  const [initialSlide, setInitialSlide] = useState(0)
  const [isReady, setIsReady] = useState(false)
  useEffect(() => {
    if (isOpen) {
      if (selectedCake) {
        setCurrentCake(selectedCake)
        setCurrentVariant(selectedCake.selectedVariant)
        const indexCake = cakeData.findIndex(
          (cake) => cake.id === selectedCake.id,
        )
        if (indexCake !== -1) {
          setInitialSlide(indexCake)
        }
      } else {
        setCurrentCake(cakeData[0])
      }
      setIsReady(true)
    } else {
      setIsReady(false)
    }
  }, [isOpen])
  const handleOk = () => {
    dispatch(
      bookingActions.setSelectedCake({
        ...currentCake,
        selectedVariant: currentVariant,
      }),
    )
    closeModal()
  }

  return (
    <ModalCommon isOpen={isOpen} title="Bánh kem" closeModal={closeModal}>
      <div>
        <div className="py-4">
          <div className="mb-4">
            {isReady && (
              <SwiperChooseCake
                cakes={cakeData}
                initialSlide={initialSlide}
                onSlideChange={(e) => {
                  setCurrentCake(cakeData[e.realIndex])
                  setCurrentVariant(null)
                }}
              />
            )}
          </div>
          <div>
            <div className="flex items-center justify-center mb-3">
              <div className="mr-1">
                {!!currentCake && !!currentVariant ? (
                  <PickHeartIcon />
                ) : (
                  <UnpickHeartIcon />
                )}
              </div>
              <div className="text-xl leading-6 text-[#F0C753] max-w-[200px]">
                {currentCake?.name}
              </div>
            </div>
            <div className="flex justify-center">
              <div className="flex-col">
                {currentCake?.variants?.map((variant) => {
                  return (
                    <div key={variant.id}>
                      <RadioButton
                        label={
                          <span className="text-[13px] leading-[18.2px]">
                            {variant.type} -{' '}
                            {variant.salePrice?.toLocaleString('de-DE')}đ
                          </span>
                        }
                        value={variant.id}
                        checked={variant.id === currentVariant?.id}
                        onChange={(e) => {
                          const { value: variantId } = e.target
                          const newVariant = currentCake?.variants?.find(
                            (v) => v.id === +variantId,
                          )
                          setCurrentVariant(newVariant)
                        }}
                        name="variant"
                      />
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </div>
        <div className="border-t border-t-[#F0C753] px-4 py-3">
          <FooterModal
            isDisabledButton={!currentCake || !currentVariant}
            onClickButton={handleOk}
            price={!!currentVariant ? currentVariant.salePrice : 0}
          />
        </div>
      </div>
    </ModalCommon>
  )
}

export default ModalSelectCake
