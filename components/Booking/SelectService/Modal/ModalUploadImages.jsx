import ModalCommon from 'components/Modal/ModalCommon'
import { useEffect, useRef, useState } from 'react'
import { uploadFileApi } from 'services/file.service'
import FooterModal from './FooterModal'
import UploadImageLabel from './UploadImageLabel'

const ModalUploadImages = ({
  isOpen = false,
  title = '',
  numberOfImage = 1,
  closeModal,
  imageUrls = [],
  onOk,
  price = 0,
}) => {
  const [images, setImages] = useState([])
  const [isUploading, setIsUploading] = useState(false)
  const inputFileRef = useRef(null)
  useEffect(() => {
    if (isOpen) {
      setImages(imageUrls)
    } else {
      setImages([])
    }
  }, [isOpen])
  const handleChange = async (e) => {
    const targetImages = e.target.files
    if (images.length + targetImages.length > numberOfImage) {
      return
    }
    const fileUploads = Array.from(targetImages).map((file) =>
      uploadFileApi(file),
    )
    setIsUploading(true)
    Promise.all(fileUploads)
      .then((values) => {
        const urls = values.map((value) => value?.data?.url)
        setImages([...images, ...urls])
      })
      .finally(() => {
        setIsUploading(false)
        if (inputFileRef.current) {
          inputFileRef.current.value = ''
        }
      })
  }
  const onRemoveImage = (index) => {
    const selectedImages = [...images]
    selectedImages.splice(index, 1)
    setImages(selectedImages)
  }
  const renderImageGallery = () => {
    const gallery = []
    for (let i = 0; i < numberOfImage; i++) {
      gallery.push(
        <UploadImageLabel
          htmlFor="images"
          index={i}
          key={i}
          urlImage={images[i]}
          onRemove={onRemoveImage}
        />,
      )
    }
    return gallery
  }

  return (
    <ModalCommon isOpen={isOpen} title={title} closeModal={closeModal}>
      <div>
        <div className="p-3">
          <input
            type="file"
            id="images"
            accept="image/*"
            multiple
            className="mb-4 hidden"
            onChange={handleChange}
            ref={inputFileRef}
          />
          <div className="grid gap-3 grid-cols-3">{renderImageGallery()}</div>
        </div>
        <div className="border-t border-t-[#F0C753] px-4 py-3">
          <FooterModal
            onClickButton={() => onOk(images)}
            isDisabledButton={isUploading}
            price={price}
          />
        </div>
      </div>
    </ModalCommon>
  )
}

export default ModalUploadImages
