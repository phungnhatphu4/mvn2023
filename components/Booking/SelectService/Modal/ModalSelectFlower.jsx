import { PickHeartIcon } from 'assets/index'
import ModalCommon from 'components/Modal/ModalCommon'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectFlowerService } from 'selectors/booking.selector'
import { bookingActions } from 'slices/booking'
import SwiperChooseFlower from '../Swiper/SwiperChooseFlower'
import FooterModal from './FooterModal'

const ModalSelectFlower = ({ isOpen = false, closeModal, flowerData = [] }) => {
  const { selectedFlower } = useSelector(selectFlowerService)
  const dispatch = useDispatch()
  const [currentFlower, setCurrentFlower] = useState(null)
  const [initialSlide, setInitialSlide] = useState(0)
  const [isReady, setIsReady] = useState(false)
  useEffect(() => {
    if (isOpen) {
      if (selectedFlower) {
        setCurrentFlower(selectedFlower)
        const indexFlower = flowerData.findIndex(
          (flower) => flower.id === selectedFlower.id,
        )
        if (indexFlower !== -1) {
          setInitialSlide(indexFlower)
        }
      } else {
        setCurrentFlower(flowerData[0])
      }
      setIsReady(true)
    } else {
      setIsReady(false)
    }
  }, [isOpen])

  const handleOk = () => {
    dispatch(bookingActions.setSelectedFlower(currentFlower))
    closeModal()
  }

  return (
    <ModalCommon isOpen={isOpen} title="Hoa tươi" closeModal={closeModal}>
      <div>
        <div className="py-4">
          <div className="mb-4">
            {isReady && (
              <SwiperChooseFlower
                flowers={flowerData}
                initialSlide={initialSlide}
                onSlideChange={(e) => {
                  setCurrentFlower(flowerData[e.realIndex])
                }}
              />
            )}
          </div>
          <div>
            <div className="flex items-center justify-center">
              <div className="mr-1">
                <PickHeartIcon />
              </div>
              <div className="text-xl leading-6 text-[#F0C753] max-w-[200px]">
                {currentFlower?.name}
              </div>
            </div>
          </div>
        </div>
        <div className="border-t border-t-[#F0C753] px-4 py-3">
          <FooterModal
            isDisabledButton={!currentFlower}
            onClickButton={handleOk}
            price={!!currentFlower ? currentFlower.salePrice : 0}
          />
        </div>
      </div>
    </ModalCommon>
  )
}

export default ModalSelectFlower
