import ImagePlusLine from 'assets/images/image-plus-line.png'
import { XRemoveIcon } from 'assets/index'
import Image from 'next/image'

const UploadImageLabel = ({ htmlFor, urlImage, onRemove, index }) => {
  return !urlImage ? (
    <label htmlFor={htmlFor}>
      <div className="bg-[#3B3D40] rounded-xl p-3">
        <div className="relative pt-[100%]">
          <div className="absolute top-0 left-0 w-full h-full">
            <Image src={ImagePlusLine} className="w-full h-full" />
          </div>
        </div>
      </div>
    </label>
  ) : (
    <div className="rounded-xl overflow-hidden">
      <div className="relative pt-[100%]">
        <div className="absolute top-0 left-0 w-full h-full">
          <img src={urlImage} className="w-full h-full object-cover" />
        </div>
        <div className="flex absolute top-1 right-1">
          <button
            onClick={() => {
              onRemove?.(index)
            }}
          >
            <XRemoveIcon />
          </button>
        </div>
      </div>
    </div>
  )
}

export default UploadImageLabel
