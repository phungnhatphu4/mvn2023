import Button from 'components/common/Button/Button'
import Input from 'components/common/Input/Input'
import ModalCommon from 'components/Modal/ModalCommon'
import clsx from 'clsx'
import { useEffect, useState } from 'react'

const ModalTextArea = ({
  title,
  inputPlaceholder,
  isOpen = false,
  closeModal,
  inputValue = '',
  onOk,
  maxLengthValue = 60,
}) => {
  const [value, setValue] = useState()
  const [isInvalidValue, setIsInvalidValue] = useState(false)
  useEffect(() => {
    setValue(inputValue)
  }, [inputValue])
  const onChange = (e) => {
    const { value } = e.target
    setValue(value)
    if (value.length > maxLengthValue) {
      setIsInvalidValue(true)
    } else {
      setIsInvalidValue(false)
    }
  }

  return (
    <ModalCommon isOpen={isOpen} title={title} closeModal={closeModal}>
      <div className="px-4 pb-4">
        <div className="flex mb-4">
          <Input
            type="textarea"
            placeholder={inputPlaceholder}
            rows={8}
            className="w-full"
            value={value}
            onChange={onChange}
            isInvalid={isInvalidValue}
          />
        </div>
        <div className="flex justify-between items-center">
          <div
            className={clsx(
              'text-[10px] leading-[14px]',
              isInvalidValue && 'text-[#FD3E30]',
            )}
          >
            Giới hạn {maxLengthValue} ký tự
          </div>
          <div>
            <Button
              onClick={() => {
                onOk(value)
              }}
              disabled={isInvalidValue}
            >
              Xong
            </Button>
          </div>
        </div>
      </div>
    </ModalCommon>
  )
}

export default ModalTextArea
