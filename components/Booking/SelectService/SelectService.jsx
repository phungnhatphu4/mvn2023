import FormCreamCake from './Form/FormCreamCake'
import Title from '../Title/Title'
import ServiceItem from './ServiceItem/ServiceItem'
import FormFlower from './Form/FormFlower'
import FormFreePhotoWash from './Form/FormFreePhotoWash'
import FormWashHangingPictures from './Form/FormWashHangingPictures'
import FormHangingLetters from './Form/FormHangingLetters'
import FormSpreadRose from './Form/FormSpreadRose'
import { useSelector } from 'react-redux'
import { selectServiceData } from 'selectors/booking.selector'
import FormDecoration from './Form/FormDecoration'
import Button from 'components/common/Button/Button'
import { useRouter } from 'next/router'
import CakeThumbnail from 'assets/images/booking/choose-service/cake.png'
import FlowerThumbnail from 'assets/images/booking/choose-service/flower.png'
import FreePictureThumbnail from 'assets/images/booking/choose-service/free-picture.png'
import HangingPictureThumbnail from 'assets/images/booking/choose-service/hanging-pictures.png'
import HangingLetterThumbnail from 'assets/images/booking/choose-service/hanging-letter.png'
import SpeadRoseThumbnail from 'assets/images/booking/choose-service/spread-rose.png'
import DecorationThumbnail from 'assets/images/booking/choose-service/decoration.png'
import { useEffect, useState } from 'react'
import { getServiceData } from 'services/service.service'

const SelectService = () => {
  const [serviceData, setServiceData] = useState({})

  const fetchData = async () => {
    // fix cứng category id dựa trên admin
    const categoryIds = {
      cake: 3,
      flower: 4,
      picture: 5,
      hangPicture: 6,
      customText: 7,
      roseTable: 8,
      decoration: 9,
    }
    const { data: cakeServiceData } = await getServiceData(categoryIds.cake)
    const { data: flowerServiceData } = await getServiceData(categoryIds.flower)
    const { data: freePhotoWashServiceData } = await getServiceData(
      categoryIds.picture,
    )
    const { data: washHangingPicturesServiceData } = await getServiceData(
      categoryIds.hangPicture,
    )
    const { data: hangingLettersServiceData } = await getServiceData(
      categoryIds.customText,
    )
    const { data: spreadRoseServiceData } = await getServiceData(
      categoryIds.roseTable,
    )
    const { data: decorationServiceData } = await getServiceData(
      categoryIds.decoration,
    )
    const cakeData = cakeServiceData?.list_deals?.data?.map((cake) => {
      const variants = cake?.deal_exts?.map((variant) => {
        return {
          id: variant.id,
          type: variant.name,
          salePrice: variant.sale_price,
          price: variant.price,
        }
      })
      return {
        id: cake.id,
        imageUrl: cake.image_urls[0],
        name: cake.name,
        variants,
      }
    })
    const flowerData = flowerServiceData?.list_deals?.data?.map((flower) => {
      return {
        id: flower.id,
        imageUrl: flower.image_urls[0],
        name: flower.name,
        salePrice: flower.sale_price,
        price: flower.price,
      }
    })
    const freePhotoWashData = {
      id: freePhotoWashServiceData?.list_deals?.data?.[0]?.id,
      salePrice: freePhotoWashServiceData?.list_deals?.data?.[0]?.sale_price,
      price: freePhotoWashServiceData?.list_deals?.data?.[0]?.price,
    }
    const washHangingPictureData = {
      id: washHangingPicturesServiceData?.list_deals?.data?.[0]?.id,
      salePrice:
        washHangingPicturesServiceData?.list_deals?.data?.[0]?.sale_price,
      price: washHangingPicturesServiceData?.list_deals?.data?.[0]?.price,
    }
    const hangingLettersData = {
      id: hangingLettersServiceData?.list_deals?.data?.[0]?.id,
      salePrice: hangingLettersServiceData?.list_deals?.data?.[0]?.sale_price,
      price: hangingLettersServiceData?.list_deals?.data?.[0]?.price,
    }
    const spreadRoseData = {
      id: spreadRoseServiceData?.list_deals?.data?.[0]?.id,
      salePrice: spreadRoseServiceData?.list_deals?.data?.[0]?.sale_price,
      price: spreadRoseServiceData?.list_deals?.data?.[0]?.price,
    }
    const decorationData = {
      id: decorationServiceData?.list_deals?.data?.[0]?.id,
      salePrice: decorationServiceData?.list_deals?.data?.[0]?.sale_price,
      price: decorationServiceData?.list_deals?.data?.[0]?.price,
    }

    const newServiceData = {
      cakeData,
      flowerData,
      freePhotoWashData,
      washHangingPictureData,
      hangingLettersData,
      spreadRoseData,
      decorationData,
    }
    setServiceData(newServiceData)
  }

  useEffect(() => {
    fetchData()
  }, [])

  const router = useRouter()
  const {
    cakeService,
    flowerService,
    freePictureService,
    hangingPictureService,
    hangingLetterService,
    spreadRoseService,
    decorationService,
  } = useSelector(selectServiceData)
  const hasSelectedCake = !!cakeService?.selectedCake
  const hasSelectedFlower = !!flowerService?.selectedFlower
  const hasSelectedFreePicture = !!freePictureService?.images?.length
  const hasSelectedHangingPicture = !!hangingPictureService?.images?.length
  const hasSelectedHangingLetter = !!hangingLetterService
  const hasSelectedSpreadRose = !!spreadRoseService
  const hasSelectedDecoration = !!decorationService
  const titleChecked =
    hasSelectedCake ||
    hasSelectedFlower ||
    hasSelectedFreePicture ||
    hasSelectedHangingPicture ||
    hasSelectedHangingLetter ||
    hasSelectedSpreadRose ||
    hasSelectedDecoration
  return (
    <div>
      <div className="flex justify-center md:justify-start mb-4 md:mb-8">
        <Title title="Chọn dịch vụ" checked={titleChecked} />
      </div>
      <div className="mb-6">
        <div className="mb-4">
          <ServiceItem
            hasSelected={hasSelectedCake}
            price={cakeService?.selectedCake?.selectedVariant?.salePrice}
            serviceName="Bánh kem"
            description={
              hasSelectedCake
                ? `${cakeService?.selectedCake?.name} - ${cakeService?.selectedCake?.selectedVariant?.type}`
                : 'Chọn loại bánh kem và kích thước phù hợp với bạn'
            }
            thumbnail={CakeThumbnail}
          >
            <div className="py-2">
              <FormCreamCake cakeData={serviceData.cakeData} />
            </div>
          </ServiceItem>
        </div>
        <div className="mb-4">
          <ServiceItem
            hasSelected={hasSelectedFlower}
            price={flowerService?.selectedFlower?.salePrice}
            serviceName="Hoa tươi"
            description={
              hasSelectedFlower
                ? flowerService?.selectedFlower?.name
                : 'Chọn bó hoa thể hiện sự yêu thương của bạn'
            }
            thumbnail={FlowerThumbnail}
          >
            <div className="py-2">
              <FormFlower flowerData={serviceData.flowerData} />
            </div>
          </ServiceItem>
        </div>
        <div className="mb-4">
          <ServiceItem
            hasSelected={hasSelectedFreePicture}
            price={freePictureService?.price}
            serviceName="Rửa hình miễn phí"
            description={
              hasSelectedFreePicture
                ? `[${freePictureService?.images?.length} hình]`
                : '2 tấm ảnh đẹp được đặt vào khung để trang trí bàn tiệc'
            }
            thumbnail={FreePictureThumbnail}
          >
            <div className="py-2">
              <FormFreePhotoWash
                freePictureData={serviceData.freePhotoWashData}
              />
            </div>
          </ServiceItem>
        </div>
        <div className="mb-4">
          <ServiceItem
            hasSelected={hasSelectedHangingPicture}
            price={hangingPictureService?.salePrice}
            serviceName="Rửa hình treo dây"
            description={
              hasSelectedHangingPicture
                ? `[${hangingPictureService?.images?.length} hình]`
                : '8 tấm ảnh đẹp được treo dây trang trí background bàn tiệc'
            }
            thumbnail={HangingPictureThumbnail}
          >
            <div className="py-2">
              <FormWashHangingPictures
                hangingPictureData={serviceData.washHangingPictureData}
              />
            </div>
          </ServiceItem>
        </div>
        <div className="mb-4">
          <ServiceItem
            hasSelected={hasSelectedHangingLetter}
            price={hangingLetterService?.salePrice}
            serviceName="Chữ treo yêu cầu"
            description={
              hasSelectedHangingLetter
                ? hangingLetterService?.text
                : 'Những dòng chữ ý nghĩa sẽ được trang trí tại không gian của bạn'
            }
            thumbnail={HangingLetterThumbnail}
          >
            <div className="py-2">
              <FormHangingLetters
                hangingLettersData={serviceData.hangingLettersData}
              />
            </div>
          </ServiceItem>
        </div>
        <div className="mb-4">
          <ServiceItem
            hasSelected={hasSelectedSpreadRose}
            price={spreadRoseService?.salePrice}
            serviceName="Rải hoa hồng trên bàn"
            description="Bàn tiệc của bạn sẽ thêm hoa hồng trên bàn để bữa tiệc thêm phần lãng mạn"
            thumbnail={SpeadRoseThumbnail}
          >
            <div className="py-2">
              <FormSpreadRose spreadRoseData={serviceData.spreadRoseData} />
            </div>
          </ServiceItem>
        </div>
        <div className="mb-4">
          <ServiceItem
            hasSelected={hasSelectedDecoration}
            price={decorationService?.salePrice}
            serviceName="Dịch vụ trang trí"
            description="Hãy để bữa tiệc của bạn thật lung linh"
            thumbnail={DecorationThumbnail}
          >
            <div className="py-2">
              <FormDecoration decorationData={serviceData.decorationData} />
            </div>
          </ServiceItem>
        </div>
      </div>
      <div className="text-center">
        <Button
          className="text-[14px] leading-[16.8px] text-white w-full py-[14px] md:w-[350px]"
          onClick={() => {
            router.push('/dat-ban/thanh-toan')
          }}
        >
          4/5 Chọn các dịch vụ này
        </Button>
      </div>
    </div>
  )
}

export default SelectService
