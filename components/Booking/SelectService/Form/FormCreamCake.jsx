import RadioButton from 'components/common/RadioButton/RadioButton'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectCakeService } from 'selectors/booking.selector'
import { bookingActions } from 'slices/booking'
import ModalSelectCake from '../Modal/ModalSelectCake'
import ModalTextArea from '../Modal/ModalTextArea'

const CANDLE = {
  number: {
    value: '1',
    label: 'Nến số',
  },
  normal: {
    value: '2',
    label: 'Nến thường',
  },
}

const FormCreamCake = ({ cakeData }) => {
  const dispatch = useDispatch()
  const cakeService = useSelector(selectCakeService)
  const [isOpenModalSelectCake, setIsOpenModalSelectCake] = useState(false)
  const [isOpenModalEnterMessage, setIsOpenModalEnterMessage] = useState(false)

  const handleChangeTypeCandle = (e) => {
    const { value } = e.target
    const candle = {
      value,
      label:
        value === CANDLE.number.value
          ? CANDLE.number.label
          : CANDLE.normal.label,
      number: '',
    }
    dispatch(bookingActions.setCakeCandle(candle))
  }

  const handleChangeInputCandle = (e) => {
    const { value } = e.target
    dispatch(
      bookingActions.setCakeCandle({
        ...cakeService?.candle,
        number: value,
      }),
    )
  }

  return (
    <>
      <div>
        <div className="flex items-center mb-3">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            Bánh kem
          </div>
          <div className="basis-1/2">
            <button
              type="button"
              className="w-full flex justify-center rounded-2xl bg-white py-2 px-3"
              onClick={() => {
                setIsOpenModalSelectCake(true)
              }}
            >
              {!cakeService?.selectedCake ? (
                <span className="text-[10px] leading-[14px] text-[#141416] opacity-50">
                  Chọn bánh kem
                </span>
              ) : (
                <span className="text-[10px] leading-[14px] text-[#141416]">
                  {cakeService?.selectedCake?.name} -{' '}
                  {cakeService?.selectedCake?.selectedVariant?.type}
                </span>
              )}
            </button>
          </div>
        </div>
        <div className="flex items-center mb-3">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px] pr-1">
            Thông điệp khắc trên bánh
          </div>
          <div className="basis-1/2">
            <button
              type="button"
              className="w-full flex justify-center bg-transparent py-2 px-3 rounded-xl border border-white"
              onClick={() => {
                setIsOpenModalEnterMessage(true)
              }}
            >
              {!cakeService?.message ? (
                <span className="text-[10px] leading-[14px] text-white opacity-50">
                  Nhập thông điệp tại đây
                </span>
              ) : (
                <span className="text-[10px] leading-[14px] text-white line-clamp-2 break-all">
                  {cakeService.message}
                </span>
              )}
            </button>
          </div>
        </div>
        <div className="flex items-center mb-3">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            <RadioButton
              label={CANDLE.number.label}
              name="type_candle"
              value={CANDLE.number.value}
              checked={cakeService?.candle?.value === CANDLE.number.value}
              onChange={handleChangeTypeCandle}
            />
          </div>
          <div className="basis-1/2">
            <input
              disabled={cakeService?.candle?.value !== CANDLE.number.value}
              type="text"
              placeholder="1-99"
              className="w-full rounded-xl border border-white py-2 px-3 bg-transparent outline-none text-center text-[10px] leading-[14px] text-white"
              value={
                cakeService?.candle?.value === CANDLE.number.value &&
                cakeService?.candle?.number
                  ? cakeService.candle.number
                  : ''
              }
              onChange={handleChangeInputCandle}
            />
          </div>
        </div>
        <div className="flex items-center">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            <RadioButton
              label={CANDLE.normal.label}
              name="type_candle"
              value={CANDLE.normal.value}
              checked={cakeService?.candle?.value === CANDLE.normal.value}
              onChange={handleChangeTypeCandle}
            />
          </div>
          <div className="basis-1/2">
            <input
              disabled={cakeService?.candle?.value !== CANDLE.normal.value}
              type="text"
              placeholder="Số cây"
              className="w-full rounded-xl border border-white py-2 px-3 bg-transparent outline-none text-center text-[10px] leading-[14px] text-white "
              value={
                cakeService?.candle?.value === CANDLE.normal.value &&
                cakeService?.candle?.number
                  ? cakeService.candle.number
                  : ''
              }
              onChange={handleChangeInputCandle}
            />
          </div>
        </div>
      </div>
      <ModalSelectCake
        isOpen={isOpenModalSelectCake}
        closeModal={() => setIsOpenModalSelectCake(false)}
        cakeData={cakeData}
      />
      <ModalTextArea
        title="Thông điệp khắc trên bánh"
        inputPlaceholder="Nhập thông điệp"
        isOpen={isOpenModalEnterMessage}
        closeModal={() => {
          setIsOpenModalEnterMessage(false)
        }}
        inputValue={cakeService?.message ?? ''}
        onOk={(value) => {
          dispatch(bookingActions.setCakeMessage(value))
          setIsOpenModalEnterMessage(false)
        }}
      />
    </>
  )
}

export default FormCreamCake
