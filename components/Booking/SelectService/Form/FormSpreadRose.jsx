import RadioButton from 'components/common/RadioButton/RadioButton'
import { useDispatch, useSelector } from 'react-redux'
import { selectSpearRoseService } from 'selectors/booking.selector'
import { bookingActions } from 'slices/booking'

const FormSpreadRose = ({ spreadRoseData }) => {
  const dispatch = useDispatch()
  const spreadRoseService = useSelector(selectSpearRoseService)
  return (
    <>
      <div>
        <div className="flex items-center">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            <RadioButton
              label="Chọn"
              checked={!!spreadRoseService}
              onChange={() => {
                dispatch(
                  bookingActions.setSpreadRose({
                    id: spreadRoseData?.id,
                    price: spreadRoseData?.price,
                    salePrice: spreadRoseData?.salePrice,
                  }),
                )
              }}
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default FormSpreadRose
