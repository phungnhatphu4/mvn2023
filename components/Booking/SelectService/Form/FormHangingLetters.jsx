import RadioButton from 'components/common/RadioButton/RadioButton'
import { useDispatch, useSelector } from 'react-redux'
import { selectHangingLetterService } from 'selectors/booking.selector'
import { bookingActions } from 'slices/booking'

const LETTER = {
  happyBirthday: {
    value: '1',
    label: 'Happy Birthday',
  },
  anniversary: {
    value: '2',
    label: 'Anniversary',
  },
  custom: {
    value: '3',
    label: 'Theo yêu cầu',
  },
}

const FormHangingLetters = ({ hangingLettersData }) => {
  const dispatch = useDispatch()
  const hangingLetterService = useSelector(selectHangingLetterService)
  const handleChangeType = (e) => {
    const { value } = e.target
    const data = {
      id: hangingLettersData?.id,
      value,
      text: '',
      price: hangingLettersData?.price,
      salePrice: hangingLettersData?.salePrice,
    }
    if (value !== LETTER.custom.value) {
      data.text =
        value === LETTER.happyBirthday.value
          ? LETTER.happyBirthday.label
          : LETTER.anniversary.label
    }
    dispatch(bookingActions.setHangingLetter(data))
  }
  return (
    <>
      <div>
        <div className="flex items-center mb-3">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            <RadioButton
              label={LETTER.happyBirthday.label}
              value={LETTER.happyBirthday.value}
              name="type_letter"
              checked={
                hangingLetterService?.value === LETTER.happyBirthday.value
              }
              onChange={handleChangeType}
            />
          </div>
        </div>
        <div className="flex items-center mb-3">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            <RadioButton
              label={LETTER.anniversary.label}
              value={LETTER.anniversary.value}
              name="type_letter"
              onChange={handleChangeType}
              checked={hangingLetterService?.value === LETTER.anniversary.value}
            />
          </div>
        </div>
        <div className="flex items-center">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            <RadioButton
              label={LETTER.custom.label}
              value={LETTER.custom.value}
              name="type_letter"
              onChange={handleChangeType}
              checked={hangingLetterService?.value === LETTER.custom.value}
            />
          </div>
          <div className="basis-1/2">
            <input
              type="text"
              placeholder="Chữ yêu cầu"
              className="w-full rounded-xl border border-white py-2 px-3 bg-transparent outline-none text-center text-[10px] leading-[14px] text-white"
              disabled={hangingLetterService?.value !== LETTER.custom.value}
              value={
                hangingLetterService?.value === LETTER.custom.value &&
                hangingLetterService?.text
                  ? hangingLetterService.text
                  : ''
              }
              onChange={(e) => {
                const { value } = e.target
                dispatch(
                  bookingActions.setHangingLetter({
                    ...hangingLetterService,
                    text: value,
                  }),
                )
              }}
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default FormHangingLetters
