import RadioButton from 'components/common/RadioButton/RadioButton'
import { useDispatch, useSelector } from 'react-redux'
import { selectDecorationService } from 'selectors/booking.selector'
import { bookingActions } from 'slices/booking'

const FormDecoration = ({ decorationData }) => {
  const dispatch = useDispatch()
  const decorationService = useSelector(selectDecorationService)
  return (
    <>
      <div>
        <div className="flex items-center">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            <RadioButton
              label="Chọn"
              checked={!!decorationService}
              onChange={() => {
                dispatch(
                  bookingActions.setDecoration({
                    id: decorationData?.id,
                    price: decorationData?.price,
                    salePrice: decorationData?.salePrice,
                  }),
                )
              }}
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default FormDecoration
