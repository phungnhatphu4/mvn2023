import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectFlowerService } from 'selectors/booking.selector'
import { bookingActions } from 'slices/booking'
import ModalSelectFlower from '../Modal/ModalSelectFlower'
import ModalTextArea from '../Modal/ModalTextArea'

const FormFlower = ({ flowerData }) => {
  const dispatch = useDispatch()
  const { selectedFlower, message, smallCardMessage } =
    useSelector(selectFlowerService)
  const [isOpenModalSelectFlower, setIsOpenModalSelectFlower] = useState(false)
  const [isOpenModalEnterWishes, setIsOpenModalEnterWishes] = useState(false)
  const [isOpenModalEnterSmallCard, setIsOpenModalEnterSmallCard] =
    useState(false)

  return (
    <>
      <div>
        <div className="flex items-center mb-3">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            Bó hoa
          </div>
          <div className="basis-1/2">
            <button
              type="button"
              className="w-full flex justify-center rounded-2xl bg-white py-2 px-3"
              onClick={() => {
                setIsOpenModalSelectFlower(true)
              }}
            >
              {!selectedFlower ? (
                <span className="text-[10px] leading-[14px] text-[#141416] opacity-50">
                  Chọn bó hoa
                </span>
              ) : (
                <span className="text-[10px] leading-[14px] text-[#141416]">
                  {selectedFlower.name}
                </span>
              )}
            </button>
          </div>
        </div>
        <div className="flex items-center mb-3">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            Thêm banner câu chúc
          </div>
          <div className="basis-1/2">
            <button
              type="button"
              className="w-full flex justify-center bg-transparent py-2 px-3 rounded-xl border border-white"
              onClick={() => {
                setIsOpenModalEnterWishes(true)
              }}
            >
              {!message ? (
                <span className="text-[10px] leading-[14px] text-white opacity-50">
                  Nhập câu chúc tại đây
                </span>
              ) : (
                <span className="text-[10px] leading-[14px] text-white line-clamp-2 break-all">
                  {message}
                </span>
              )}
            </button>
          </div>
        </div>
        <div className="flex items-center">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            Thêm tấm thiệp nhỏ
          </div>
          <div className="basis-1/2">
            <button
              type="button"
              className="w-full flex justify-center bg-transparent py-2 px-3 rounded-xl border border-white"
              onClick={() => {
                setIsOpenModalEnterSmallCard(true)
              }}
            >
              {!smallCardMessage ? (
                <span className="text-[10px] leading-[14px] text-white opacity-50">
                  Nhập nội dung thiệp tại đây
                </span>
              ) : (
                <span className="text-[10px] leading-[14px] text-white line-clamp-2 break-all">
                  {smallCardMessage}
                </span>
              )}
            </button>
          </div>
        </div>
      </div>
      <ModalSelectFlower
        isOpen={isOpenModalSelectFlower}
        closeModal={() => setIsOpenModalSelectFlower(false)}
        flowerData={flowerData}
      />
      <ModalTextArea
        title="Thêm Banner câu chúc"
        inputPlaceholder="Nhập câu chúc tại đây"
        isOpen={isOpenModalEnterWishes}
        closeModal={() => {
          setIsOpenModalEnterWishes(false)
        }}
        inputValue={message ?? ''}
        onOk={(value) => {
          dispatch(bookingActions.setFlowerMessage(value))
          setIsOpenModalEnterWishes(false)
        }}
      />
      <ModalTextArea
        title="Thêm tấm thiệp nhỏ"
        inputPlaceholder="Nhập thông điệp tại đây"
        isOpen={isOpenModalEnterSmallCard}
        closeModal={() => {
          setIsOpenModalEnterSmallCard(false)
        }}
        maxLengthValue={600}
        inputValue={smallCardMessage ?? ''}
        onOk={(value) => {
          dispatch(bookingActions.setFlowerSmallCardMessage(value))
          setIsOpenModalEnterSmallCard(false)
        }}
      />
    </>
  )
}

export default FormFlower
