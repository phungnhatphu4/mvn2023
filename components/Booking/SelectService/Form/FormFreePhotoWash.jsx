import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectFreePictureService } from 'selectors/booking.selector'
import { bookingActions } from 'slices/booking'
import ModalUploadImages from '../Modal/ModalUploadImages'

const FormFreePhotoWash = ({ freePictureData }) => {
  const [isOpenModalUploadImages, setIsOpenModalUploadImages] = useState(false)
  const dispatch = useDispatch()
  const { images } = useSelector(selectFreePictureService)
  return (
    <>
      <div>
        <div className="flex items-center">
          <div className="basis-1/2 text-white text-[13px] leading-[18.2px]">
            Rửa hình
          </div>
          <div className="basis-1/2">
            <button
              type="button"
              className="w-full flex justify-center rounded-2xl bg-white py-2 px-3"
              onClick={() => setIsOpenModalUploadImages(true)}
            >
              {!images?.length ? (
                <span className="text-[10px] leading-[14px] text-[#141416] opacity-50">
                  Tải ảnh lên
                </span>
              ) : (
                <span className="text-[10px] leading-[14px] text-[#141416]">
                  [{images.length} hình]
                </span>
              )}
            </button>
          </div>
        </div>
      </div>
      <ModalUploadImages
        isOpen={isOpenModalUploadImages}
        closeModal={() => setIsOpenModalUploadImages(false)}
        title="Rửa hình miễn phí"
        numberOfImage={2}
        imageUrls={images}
        onOk={(imageUrls) => {
          dispatch(
            bookingActions.setFreeImages({
              id: freePictureData?.id,
              images: imageUrls,
              price: imageUrls?.length ? freePictureData?.salePrice : 0,
            }),
          )
          setIsOpenModalUploadImages(false)
        }}
      />
    </>
  )
}

export default FormFreePhotoWash
