import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation } from 'swiper'
import clsx from 'clsx'

const SwiperChooseFlower = ({
  flowers = [],
  initialSlide = 0,
  onSlideChange,
}) => {
  return (
    <div>
      <Swiper
        slidesPerView="auto"
        spaceBetween={-16}
        centeredSlides
        loop={flowers.length > 2}
        breakpoints={{
          320: {
            slidesPerView: 1.5,
            spaceBetween: -16,
          },
        }}
        initialSlide={initialSlide}
        modules={[Navigation]}
        onSlideChange={onSlideChange}
        slideToClickedSlide
        runCallbacksOnInit={false}
      >
        {flowers.map((flower) => {
          return (
            <SwiperSlide key={flower.id}>
              {({ isActive }) => {
                return (
                  <div
                    className={clsx(
                      'w-full pt-[75%] relative rounded-xl border border-[#F0C753] overflow-hidden',
                      isActive ? 'scale-100' : 'scale-[75%]',
                    )}
                  >
                    <div className="absolute top-0 w-full h-full">
                      <img
                        src={flower.imageUrl}
                        alt={flower.name}
                        className="w-full h-full"
                      />
                    </div>
                  </div>
                )
              }}
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  )
}

export default SwiperChooseFlower
