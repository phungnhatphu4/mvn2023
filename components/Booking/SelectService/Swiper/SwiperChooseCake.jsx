import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation } from 'swiper'
import clsx from 'clsx'

const SwiperChooseCake = ({ cakes = [], initialSlide = 0, onSlideChange }) => {
  return (
    <div>
      <Swiper
        slidesPerView="auto"
        spaceBetween={-16}
        centeredSlides
        loop={cakes.length > 2}
        breakpoints={{
          320: {
            slidesPerView: 1.5,
            spaceBetween: -16,
          },
        }}
        initialSlide={initialSlide}
        modules={[Navigation]}
        onSlideChange={onSlideChange}
        slideToClickedSlide
        runCallbacksOnInit={false}
      >
        {cakes.map((cake) => {
          return (
            <SwiperSlide key={cake.id}>
              {({ isActive }) => {
                return (
                  <div
                    className={clsx(
                      'w-full pt-[75%] relative rounded-xl border border-[#F0C753] overflow-hidden',
                      isActive ? 'scale-100' : 'scale-[75%]',
                    )}
                  >
                    <div className="absolute top-0 w-full h-full">
                      <img
                        src={cake.imageUrl}
                        alt={cake.name}
                        className="w-full h-full"
                      />
                    </div>
                  </div>
                )
              }}
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  )
}

export default SwiperChooseCake
