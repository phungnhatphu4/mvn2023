import { XIcon } from 'assets/index'
import clsx from 'clsx'
import Modal from './Modal'

const ModalCommon = ({
  isOpen,
  title,
  closeModal,
  hasIconClose = true,
  children,
  isFullScreen = false,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={() => {
        closeModal?.()
      }}
      wrapperContentClassName={clsx(isFullScreen && 'p-0 h-full')}
    >
      <div
        className={clsx(
          'flex flex-col max-h-[90vh] rounded-2xl w-full text-white bg-[#000000] overflow-hidden',
          isFullScreen &&
            'rounded-none max-h-full h-full sm:rounded-2xl sm:max-h-[90vh] sm:h-auto',
        )}
      >
        <div className="p-4 relative px-4">
          <div className="text-center text-lg leading-[25.2px] text-[#F0C753] ">
            {title}
          </div>
          {hasIconClose && (
            <div className="flex absolute top-1/2 -translate-y-1/2 right-4">
              <button
                onClick={() => {
                  closeModal?.()
                }}
              >
                <XIcon />
              </button>
            </div>
          )}
        </div>
        <div className="overflow-auto">{children}</div>
      </div>
    </Modal>
  )
}

export default ModalCommon
