import { XIcon } from 'assets/index'
import Modal from './Modal'

const ModalAlert = ({
  isOpen,
  closeModal,
  hasIconClose = true,
  message,
  subMessage,
  titleButton,
  icon,
  onClickButton,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={() => {
        closeModal?.()
      }}
    >
      <div className="bg-[#252525] rounded-2xl w-full text-white">
        <div className="px-3 py-8 relative">
          {hasIconClose && (
            <div className="absolute top-3 right-3">
              <button
                onClick={() => {
                  closeModal?.()
                }}
              >
                <XIcon />
              </button>
            </div>
          )}
          <div className="flex-col">
            <div className="flex justify-center mb-6">{icon}</div>
            <div className="text-center text-[#F0C753] text-xl leading-6 mb-4">
              {message}
            </div>
            <div className="text-center text-[#797979] text-[15px] leading-6 mb-6">
              {subMessage}
            </div>
            <div className="flex justify-center">
              <button
                className="bg-primary-linear px-8 py-2 text-[13px] leading-[15.6px] rounded"
                onClick={onClickButton}
              >
                {titleButton}
              </button>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default ModalAlert
