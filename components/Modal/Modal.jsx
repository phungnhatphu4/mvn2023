import clsx from 'clsx'
import ReactModal from 'react-modal'

const Modal = ({
  isOpen,
  children,
  shouldCloseOnOverlayClick,
  shouldCloseOnEsc,
  onRequestClose,
  overlayClassName,
  wrapperContentClassName,
}) => {
  return (
    <ReactModal
      isOpen={isOpen}
      overlayClassName={clsx(
        'fixed inset-0 bg-[#1C1C1C] bg-opacity-80 flex items-center z-50',
        overlayClassName,
      )}
      className={clsx(
        'flex items-center p-4 relative w-full sm:max-w-[500px] sm:m-auto sm:px-0 sm:py-7',
        wrapperContentClassName,
      )}
      shouldCloseOnOverlayClick={shouldCloseOnOverlayClick}
      shouldCloseOnEsc={shouldCloseOnEsc}
      onRequestClose={onRequestClose}
      ariaHideApp={false}
    >
      {children}
    </ReactModal>
  )
}

export default Modal
