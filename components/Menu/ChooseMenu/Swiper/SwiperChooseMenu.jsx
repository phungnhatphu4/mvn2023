import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation } from 'swiper'
import { NextIcon, PrevIcon } from 'assets/index'
import { useRef } from 'react'
import clsx from 'clsx'

const SwiperChooseMenu = ({ menus, onSlideChange }) => {
  const navigationPrevRef = useRef(null)
  const navigationNextRef = useRef(null)

  return (
    <div className="flex items-center">
      <div className="flex">
        <button ref={navigationPrevRef}>
          <PrevIcon />
        </button>
      </div>
      <Swiper
        navigation={{
          prevEl: navigationPrevRef.current,
          nextEl: navigationNextRef.current,
        }}
        slidesPerView="auto"
        breakpoints={{
          320: {
            slidesPerView: 1,
          },
        }}
        onSlideChange={onSlideChange}
        loop
        modules={[Navigation]}
        centeredSlides
        className="flex-1"
      >
        {menus.map((menu) => {
          return (
            <SwiperSlide key={menu.id}>
              <div className="text-xl leading-6 text-[#F0C753] text-center">
                Bạn đang chọn: {menu.name}
              </div>
            </SwiperSlide>
          )
        })}
      </Swiper>
      <div className="flex">
        <button ref={navigationNextRef}>
          <NextIcon />
        </button>
      </div>
    </div>
  )
}

export default SwiperChooseMenu
