import { ArrowDropDownIcon, ArrowDropUpIcon } from 'assets/index'
import clsx from 'clsx'
import {
  standardizedServiceData,
  standardizedStepChooseBranchData,
  standardizedStepChooseMenuData,
  standardizedStepChoosePositionData,
} from 'helpers/standardizedData'
import { useStep } from 'hooks/useStep'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import Collapsible from 'react-collapsible'
import { useSelector } from 'react-redux'
import { selectBookingData } from 'selectors/booking.selector'

const CallapsibleStep = ({
  open = false,
  stepId,
  openStepId = 0,
  onToggleContentStep,
  title,
  data = [],
  onClickButtonChange,
}) => {
  const { currentStep } = useStep()
  const [openContent, setOpenContent] = useState(false)
  useEffect(() => {
    if (!open) {
      setOpenContent(false)
    }
  }, [open])
  return (
    <Collapsible
      open={open && (openStepId === 0 || openStepId === stepId)}
      openedClassName={clsx(
        'border-b border-b-black last:border-b-0',
        openContent && 'border-b-0',
        currentStep && currentStep < stepId && 'bg-black',
      )}
    >
      <div
        className={clsx(
          'flex px-3 py-2 items-center justify-between',
          openContent && 'border-b border-b-[#F0C753]',
        )}
      >
        <div className="text-[13px] leading-[18.2px] select-none">{title}</div>
        {currentStep && currentStep > stepId && (
          <div>
            <button
              className="p-2"
              onClick={() => {
                onToggleContentStep?.(stepId, !openContent)
                setOpenContent(!openContent)
              }}
            >
              {openContent ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
            </button>
          </div>
        )}
      </div>
      <Collapsible open={openContent}>
        <div>
          <div className="pl-3 border-b border-b-[#F0C753]">
            {data.map((item) => {
              return (
                <div
                  className="px-3 py-3 border-b border-[#F0C753] last:border-b-0"
                  key={item?.fieldName}
                >
                  <div className="flex justify-between items-center text-[10px] leading-[14px]">
                    <div className="basis-1/2">{item?.fieldName}</div>
                    <div className="basis-1/2 border border-white rounded-2xl px-3 py-1 text-center">
                      {item?.fieldValue}
                    </div>
                  </div>
                  {item?.salePrice && (
                    <div className="flex justify-end mt-2 text-[13px] leading-[18.2px]">
                      {item?.salePrice?.toLocaleString('de-DE')}đ
                    </div>
                  )}
                </div>
              )
            })}
          </div>
          <div className="flex pr-3 py-3 justify-end">
            <button
              className="rounded-2xl bg-white text-[#F0C753] py-1 px-12 text-[10px] leading-[14px]"
              onClick={onClickButtonChange}
            >
              Thay đổi
            </button>
          </div>
        </div>
      </Collapsible>
    </Collapsible>
  )
}

const Plugin = () => {
  const router = useRouter()
  const { chooseBranch, choosePosition, chooseMenu, chooseService } =
    useSelector(selectBookingData)
  const { currentStep } = useStep()
  const [open, setOpen] = useState(false)
  const [openStepId, setOpenStepId] = useState(0)
  const handleToggleContentStep = (stepId, isOpen) => {
    if (isOpen) {
      setOpenStepId(stepId)
    } else {
      setOpenStepId(0)
    }
  }
  useEffect(() => {
    setOpen(false)
    setOpenStepId(0)
  }, [currentStep])
  return (
    <div
      className={clsx(
        'relative w-[272px] bg-primary-linear rounded-r-2xl border border-[#F0C753] border-l-0 text-white transition-all duration-700',
        open && 'rounded-br-none',
      )}
    >
      <div
        className={clsx(
          'px-3 py-2 text-[13px] leading-[18.2px] border-b-2 border-b-transparent cursor-pointer select-none',
          open && 'border-b-black',
        )}
        onClick={() => {
          if (open) {
            setOpenStepId(0)
          }
          setOpen(!open)
        }}
      >
        Bữa tiệc hoàn hảo: {currentStep}/5
      </div>
      <div
        className={clsx(
          'absolute w-full box-content z-10 bg-primary-linear rounded-br-2xl overflow-hidden',
          open && ' border border-[#F0C753] border-t-0 border-l-0',
        )}
      >
        <CallapsibleStep
          open={open}
          openStepId={openStepId}
          stepId={1}
          title="Bước 1: Chọn chi nhánh"
          onToggleContentStep={handleToggleContentStep}
          data={standardizedStepChooseBranchData(chooseBranch)}
          onClickButtonChange={() => {
            router.push('/dat-ban/chon-chi-nhanh')
          }}
        />
        <CallapsibleStep
          open={open}
          stepId={2}
          openStepId={openStepId}
          title="Bước 2: Chọn vị trí"
          onToggleContentStep={handleToggleContentStep}
          data={standardizedStepChoosePositionData(choosePosition)}
          onClickButtonChange={() => {
            router.push('/dat-ban/chon-vi-tri')
          }}
        />
        <CallapsibleStep
          open={open}
          stepId={3}
          openStepId={openStepId}
          title="Bước 3: Chọn set menu"
          onToggleContentStep={handleToggleContentStep}
          data={standardizedStepChooseMenuData(chooseMenu)}
          onClickButtonChange={() => {
            router.push('/dat-ban/chon-menu')
          }}
        />
        <CallapsibleStep
          open={open}
          stepId={4}
          openStepId={openStepId}
          title="Bước 4: Chọn dịch vụ"
          onToggleContentStep={handleToggleContentStep}
          data={standardizedServiceData(chooseService)}
          onClickButtonChange={() => {
            router.push('/dat-ban/chon-dich-vu')
          }}
        />
        <CallapsibleStep
          open={open}
          stepId={5}
          openStepId={openStepId}
          title="Bước 5: Hoàn tất"
          onToggleContentStep={handleToggleContentStep}
          data={[]}
        />
      </div>
    </div>
  )
}

export default Plugin
