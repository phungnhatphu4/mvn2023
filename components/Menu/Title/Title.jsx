import { PickHeartIcon, UnpickHeartIcon } from 'assets/index'

const Title = ({ title = '', checked = false }) => {
  return (
    <div className="flex items-center">
      <div className="mr-3">
        {checked ? <PickHeartIcon /> : <UnpickHeartIcon />}
      </div>
      <div className="text-xl leading-6 text-white uppercase">{title}</div>
    </div>
  )
}

export default Title
