import { LogoMobile, TrigramIcon, XIcon } from 'assets/index'
import { isEmpty } from 'lodash'
import { useRouter } from 'next/router'
import SignIn from 'components/common/sign_in'
import SignUp from 'components/common/sign_up'
import { useState } from 'react'
import { useSelector } from 'react-redux'
import { getUserInfoSelector } from 'selectors/auth'
import Link from 'next/link'

const Header = () => {
  const router = useRouter()
  const [openSignInModal, setOpenSignInModal] = useState(false)
  const [openSignUpModal, setOpenSignUpModal] = useState(false)
  const userInfo = useSelector(getUserInfoSelector)

  const getMenuActive = (path) => {
    return router.pathname === path ? 'text-[#F0C753]' : '';
  }

  return (
    <>
      <SignIn
        isOpen={openSignInModal}
        closeModal={() => setOpenSignInModal(false)}
        openModalSignUp={() => setOpenSignUpModal(true)}
      />
      <SignUp
        isOpen={openSignUpModal}
        closeModal={() => setOpenSignUpModal(false)}
        openModalSignIn={() => setOpenSignInModal(true)}
      />
      <div className="bg-[#141416]">
        <div className="container">
          <div className="flex items-center justify-between px-[12px] py-[5px] border-b border-white lg:border-b-0 lg:py-[14px] xl:px-0">
            <div className="flex items-center lg:hidden">
              <button>
                <TrigramIcon
                  onClick={() => {
                    document.getElementById('menu-overlay').style.height =
                      '100%'
                  }}
                />
              </button>
            </div>
            <div>
              <Link href="/">
                <a>
                  <LogoMobile />
                </a>
              </Link>
            </div>
            <div className="hidden lg:block">
              <div className="text-sm leading-5 text-white">
                <Link href="/">
                  <a className={`${getMenuActive('/')} mr-6`}>Trang chủ</a>
                </Link>
                <Link href="/chi-nhanh">
                  <a className={`${getMenuActive('/chi-nhanh')} mr-6`}>Chi nhánh</a>
                </Link>
                <Link href="/menu">
                  <a className={`${getMenuActive('/menu')} mr-6`}>Menu</a>
                </Link>
                <Link href="/dich-vu">
                  <a className={`${getMenuActive('/dich-vu')} mr-6`}>Dịch vụ</a>
                </Link>
                <Link href="/su-kien">
                  <a className={`${getMenuActive('/su-kien')} mr-6`}>Sự kiện &#38; bài viết</a>
                </Link>
                <Link href="/lien-he">
                  <a className={`${getMenuActive('/lien-he')} mr-6`}>Liên hệ</a>
                </Link>
              </div>
            </div>
            <div className="flex items-center">
              <div className="mr-4 hidden lg:block text-white">
                {isEmpty(userInfo) ? (
                  <span
                    className="text-white text-sm leading-5 cursor-pointer select-none"
                    onClick={() => setOpenSignInModal(true)}
                  >
                    Đăng nhập
                  </span>
                ) : (
                  userInfo.name
                )}
              </div>
              <div>
                <button
                  style={{
                    background:
                      'linear-gradient(90deg, #8E7A49 0%, #FCCF64 49.48%, #8E7A49 100%)',
                  }}
                  className="px-[12px] py-[9px] border border-white rounded text-[13px] font-bold leading-[15.6px] text-white"
                  onClick={() => {
                    router.push('/dat-ban/chon-chi-nhanh')
                  }}
                >
                  Đặt bàn
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        id="menu-overlay"
        className="w-full h-0 fixed left-0 top-0 overflow-x-hidden bg-[#1c1c1c] bg-opacity-20 backdrop-blur-[25px] duration-500 z-10"
      >
        <div className="relative w-full p-3">
          <div className="absolute flex justify-center right-3">
            <button
              onClick={() => {
                document.getElementById('menu-overlay').style.height = '0'
              }}
            >
              <XIcon />
            </button>
          </div>
          <div className="flex justify-center mb-8">
            <Link href="/">
              <a className="text-[#F0C753] mr-6">
                <LogoMobile />
              </a>
            </Link>
          </div>
          <div className="flex flex-col items-center uppercase text-white font-bold text-[15px] leading-[21px]">
            <Link href="/chi-nhanh">
              <a className="mb-4">Chi nhánh</a>
            </Link>
            <Link href="/coming-soon">
              <a className="mb-4">Menu</a>
            </Link>
            <Link href="/coming-soon">
              <a className="mb-4">Dịch vụ</a>
            </Link>
            <Link href="/coming-soon">
              <a className="mb-4">Sự kiện &#38; bài viết</a>
            </Link>
            <Link href="/coming-soon">
              <a>Liên hệ</a>
            </Link>
          </div>
          <hr className="my-7 h-0 border border-white" />
          <div className="uppercase text-white text-[15px] leading-[21px] font-bold text-center">
            {isEmpty(userInfo) ? (
              <>
                <span
                  className="text-white text-sm leading-5"
                  onClick={() => setOpenSignInModal(true)}
                >
                  Đăng nhập
                </span>
                <span> / </span>
                <span
                  className="text-white text-sm leading-5"
                  onClick={() => setOpenSignUpModal(true)}
                >
                  Đăng ký
                </span>
              </>
            ) : (
              userInfo.name
            )}
          </div>
        </div>
      </div>
    </>
  )
}

export default Header
