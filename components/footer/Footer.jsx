import {
  AmericanExpressIcon,
  FacebookIcon,
  JCBIcon,
  LinkedinIcon,
  LocationIcon,
  LogoFooter,
  MasterCardIcon,
  RightArrowIcon,
  TelephoneIcon,
  TwitterIcon,
  VisaIcon,
  YoutubeIcon,
} from 'assets/index'

const Footer = () => {
  return (
    <div className="bg-[#111418]">
      <div className="text-white container">
        <div className="px-4 pt-4 pb-6 xl:px-0">
          <div className='lg:flex lg:justify-between'>
            <div className="mb-7">
              <div className="flex mb-3">
                <a href="#">
                  <LogoFooter />
                </a>
              </div>
              <div>
                <div className="flex items-center mb-2">
                  <span className="mr-2">
                    <LocationIcon />
                  </span>
                  <span className="text-xs leading-[16.8px]">
                    Vintage House - 4/1 Hoàng Dư Khương, Quận 10, HCM
                  </span>
                </div>
                <div className="flex items-center mb-2">
                  <span className="mr-2">
                    <LocationIcon />
                  </span>
                  <span className="text-xs leading-[16.8px]">
                    Wine Cellar - 176/9 Lê Văn Sỹ, Q. Phú Nhuận
                  </span>
                </div>
                <div className="flex items-center mb-2">
                  <span className="mr-2">
                    <LocationIcon />
                  </span>
                  <span className="text-xs leading-[16.8px]">
                    Garden in Island - 88 Nguyễn Đức Cảnh, phường Tân Phong,
                    Quận 7, HCM
                  </span>
                </div>
                <div className="flex items-center">
                  <span className="mr-2">
                    <TelephoneIcon />
                  </span>
                  <span className="text-xs leading-[16.8px]">1900.277270</span>
                </div>
              </div>
            </div>
            <div className="mb-7">
              <div className="text-lg leading-[25.56px] font-semibold mb-3">
                Về chúng tôi
              </div>
              <div className="text-xs leading-[16.8px]">
                <div className="mb-[14px]">
                  Mở cửa 5:00 PM - 10:00 PM hàng ngày
                </div>
                <div className="mb-[14px]">
                  <a href="#">Liên hệ</a>
                </div>
                <div className="mb-[14px]">
                  <a href="#">Chính sách bảo mật</a>
                </div>
                <div className="mb-[14px]">
                  <a href="#">Gửi ý kiến phản hồi</a>
                </div>
                <div>
                  <a href="#">Câu hỏi thường gặp</a>
                </div>
              </div>
            </div>
            <div className="mb-4">
              <div className="text-lg leading-[25.56px] font-semibold mb-3">
                Theo dõi chúng tôi
              </div>
              <div>
                <div className="mb-3">
                  <div className="bg-[#828282] rounded-full overflow-hidden">
                    <div className="flex p-[3px] items-center justify-between">
                      <div className="flex items-center flex-1">
                        <input
                          placeholder="Nhập mail để nhận ưu đãi"
                          className="w-full text-xs text-white bg-inherit outline-none px-2 placeholder-[#E0E0E0]"
                        />
                      </div>
                      <div className="flex items-center bg-[#F0C753] rounded-full overflow-hidden">
                        <button className="text-white px-[18px] py-[6px]">
                          <RightArrowIcon />
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="flex">
                  <a href="#" className="mr-[7px]">
                    <FacebookIcon />
                  </a>
                  <a href="#" className="mr-[7px]">
                    <YoutubeIcon />
                  </a>
                  <a href="#" className="mr-[7px]">
                    <TwitterIcon />
                  </a>
                  <a href="#">
                    <LinkedinIcon />
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="bg-[#828282] bg-opacity-20 rounded mb-3">
            <div className="p-3 flex items-center justify-between">
              <div className="text-xs leading-[16.8px] text-[#F0C753]">
                Chấp nhận phương thức thanh toán
              </div>
              <div className="flex">
                <span className="mr-[10px]">
                  <VisaIcon />
                </span>
                <span className="mr-[10px]">
                  <MasterCardIcon />
                </span>
                <span className="mr-[10px]">
                  <JCBIcon />
                </span>
                <span>
                  <AmericanExpressIcon />
                </span>
              </div>
            </div>
          </div>
          <div>
            <div className="text-[13px]">Copyright © 2022 GoE</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer
