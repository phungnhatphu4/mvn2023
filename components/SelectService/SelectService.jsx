import { useRouter } from 'next/router'

const SelectBranch = ({

 
  listBranch = [],
  onClickBranch = () => {},
  selectedBranch,
  zoomSelected = false
}) => {
  const router = useRouter()
  if (!listBranch.length) return <></>

  const getColClass = (branch) => {

   
    if (!zoomSelected) {
      return `basis-2/6 flex flex-col`;
    } else {
      if (!isMobile) {
        return `w-1/4`;
      } else {
        return `w-1/2`;
      }
    }
  }

  return (

    
    <div className="flex flex-wrap mx-auto">
      {listBranch.map((branch, index) => {
        return (

          <div
          
            className="c3col md:p-1" onClick={() => {

              router.push({
                pathname: "/dich-vu/"+branch.id,
                });
               
           
                }}
            key={branch.name}
           
          >
           
            <div
              className={`pt-[100%] relative ${
                branch?.id === selectedBranch?.id
                  ? 'border-[#F0C753]'
                  : 'border-white'
              }`}
            >
              <div className="absolute w-full h-full top-0">
                <img
                  src={branch.image_urls}
                  alt={branch.name}
                  className="w-full h-full object-contain"
                />
              </div>
            </div>
             <div
              className={`${
                
                branch?.id === selectedBranch?.id
                  ? ''
                  : ''
              } ${zoomSelected ? 'h-[30px]' : ''} flex-1 text-center text-white text-[8px] leading-[9.6px] py-[5px] uppercase lg:text-sm lg:leading-5`}
            >
              {branch.name}
            </div>
          </div>

          
        )
      })}
    </div>
  )
}

export default SelectBranch
