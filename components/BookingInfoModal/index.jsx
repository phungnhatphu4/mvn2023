import React, { useState } from 'react'
import ModalCommon from 'components/Modal/ModalCommon'
import Button from 'components/common/Button/Button'

const BookingInfoModal = ({ isOpen = false, closeModal, onSubmit }) => {
  const [name, setName] = useState('')
  const [isErrorName, setErrorName] = useState(false)

  const [phone, setPhone] = useState('')
  const [errorMessage, setErrorMessage] = useState('')

  const [note, setNote] = useState('')

  const onChangeName = (e) => {
    const value = e.target.value
    setName(value)
    if (!value) {
      setErrorName(true)
    } else {
      setErrorName(false)
    }
  }

  const handleCheckPhone = (phoneNumber) => {
    if (!phoneNumber) {
      setErrorMessage('Vui lòng nhập số điện thoại')
    } else {
      let phoneNumReg = /^(\+84|\+840|84|084|840|0)([9235678]{1}[0-9]{8})$/
      const isPhoneNumberValidate = phoneNumReg.test(phoneNumber)
      if (phoneNumber && !isPhoneNumberValidate) {
        setErrorMessage('Số điện thoại sai định dạng')
      } else {
        setErrorMessage('')
      }
    }
  }

  const onChangePhone = (e) => {
    const value = e.target.value
    setPhone(value)
    handleCheckPhone(value)
  }

  const onChangeNote = (e) => {
    const value = e.target.value
    setNote(value)
  }

  return (
    <ModalCommon
      isOpen={isOpen}
      title={'Thông tin đặt bàn'}
      closeModal={closeModal}
    >
      <div>
        <div className="p-6">
          <label className="block mb-4">
            <span className="block text-sm font-medium text-white font-[500] text-[15px]">
              Họ và Tên*
            </span>
            <input
              name="phone-number"
              className="mt-2 p-3 border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 bg-transparent"
              placeholder="Nhập họ tên"
              onChange={onChangeName}
            />
            {isErrorName ? (
              <div className="mt-2 text-[12px] text-[#FD3E30]">
                Vui lòng nhập tên
              </div>
            ) : (
              <></>
            )}
          </label>
          <label className="block mb-4">
            <span className="block text-sm font-medium text-white font-[500] text-[15px]">
              Số điện thoại*
            </span>
            <input
              name="phone-number"
              className="mt-2 p-3 border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 bg-transparent"
              placeholder="Nhập số điện thoại"
              onChange={onChangePhone}
            />
            {errorMessage ? (
              <div className="mt-2 text-[12px] text-[#FD3E30]">
                {errorMessage}
              </div>
            ) : (
              <></>
            )}
          </label>
          <label className="block mb-6">
            <span className="block text-sm font-medium text-white font-[500] text-[15px]">
              Ghi chú
            </span>
            <input
              className="mt-2 p-3 border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 bg-transparent"
              placeholder="Nhập mật khẩu"
              onChange={onChangeNote}
            />
          </label>
          <Button
            className="w-full"
            onClick={() =>
              onSubmit({
                customer_name: name,
                customer_phone: phone,
                note,
              })
            }
          >
            Xác nhận
          </Button>
        </div>
      </div>
    </ModalCommon>
  )
}

export default BookingInfoModal
