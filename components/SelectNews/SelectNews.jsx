import { useRouter } from 'next/router'

const SelectBranch = ({

 
  ListNews = [],
  onClickBranch = () => {},
  selectedBranch,
  zoomSelected = false
}) => {
  const router = useRouter()
  if (!ListNews.length) return <></>

  const getColClass = (branch) => {

   
    if (!zoomSelected) {
      return `basis-2/6 flex flex-col`;
    } else {
      if (!isMobile) {
        return `w-1/4`;
      } else {
        return `w-1/2`;
      }
    }
  }

  return (

    
    <div className="flex flex-wrap mx-auto">
      {ListNews.map((news, index) => {
        return (

          <div
          
            className="c3col md:p-1" onClick={() => {

              router.push({
                pathname: "/su-kien/"+news.id,
                });
               
           
                }}
            key={news.name}
           
          >

            <div className='block'>
           
            <div
              className={`pt-[100%] relative ${
                news?.id === selectedBranch?.id
                  ? 'border-[#F0C753]'
                  : 'border-white'
              }`}
            >
              <div className="absolute w-full h-full top-0">
                <img
                  src={news.cover_img}
                  alt={news.name}
                  className="w-full h-full object-contain"
                />
              </div>
            </div>
             <div
              className={`${
                
                news?.id === selectedBranch?.id
                  ? ''
                  : ''
              } ${zoomSelected ? '' : ''} flex-1 text-center text-white text-[8px] leading-[9.6px] py-[5px] uppercase lg:text-sm lg:leading-5`}
            >
              {news.name}
            </div>

            <div
              className={`${
                
                news?.id === selectedBranch?.id
                  ? ''
                  : ''
              } ${zoomSelected ? '' : ''} flex-1 text-center text-white text-[8px] leading-[9.6px] py-[5px] uppercase lg:text-sm lg:leading-5`}
            >
              {news.created_at}
            </div>

            <div
              className={`${
                
                news?.id === selectedBranch?.id
                  ? ''
                  : ''
              } ${zoomSelected ? '' : ''} flex-1 text-center text-white text-[8px] leading-[9.6px] py-[5px] uppercase lg:text-sm lg:leading-5`}
            >
             <p>{news.description.length > 250 ?
    `${news.description.substring(0, 150)}...` : news.description
  } </p>
            </div>
          </div>
</div>
          
        )
      })}
    </div>
  )
}

export default SelectBranch
