import React, { useMemo, useState } from 'react'

const FormEnterPassword = () => {
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [passwordHidden, setPasswordHidden] = useState(true)
  const [confirmPasswordHidden, setConfirmPasswordHidden] = useState(true)

  const isDisabled = useMemo(
    () => password === '' || password !== confirmPassword,
    [password, confirmPassword],
  )

  const onChangePassword = (event) => {
    const { value } = event.target
    setPassword(value)
  }

  const onChangeConfirmPassword = (event) => {
    const { value } = event.target
    setConfirmPassword(value)
  }

  const onSubmit = (event) => {
    event.preventDefault()
  }

  return (
    <div className="w-[410px] p-10">
      <h3 className="font-medium text-2xl">Tạo mật khẩu mới</h3>
      <p className="text-content-secondary">Vui lòng nhập mật khẩu mong muốn</p>
      <form className="mt-8" onSubmit={onSubmit}>
        <div className="flex justify-between items-center mb-6 border-b">
          <input
            type={passwordHidden ? 'password' : 'text'}
            value={password}
            onChange={onChangePassword}
            name="password"
            id="password"
            placeholder="Mật khẩu mới"
            className="flex-1 outline-none w-full py-2"
          />
          {/* <button
            type="button"
            onClick={() => setPasswordHidden(!passwordHidden)}
          >
            {passwordHidden ? (
              <IconEyeOff width={18} height={18} className="opacity-60" />
            ) : (
              <IconEye width={18} height={18} className="opacity-60" />
            )}
          </button> */}
        </div>
        <div className="flex justify-between items-center border-b">
          <input
            type={confirmPasswordHidden ? 'password' : 'text'}
            value={confirmPassword}
            onChange={onChangeConfirmPassword}
            name="confirmPassword"
            id="confirm-password"
            placeholder="Xác nhận mật khẩu"
            className="flex-1 outline-none w-full py-2"
          />
          <button
            type="button"
            onClick={() => setConfirmPasswordHidden(!confirmPasswordHidden)}
          >
            {confirmPasswordHidden ? (
              <IconEyeOff width={18} height={18} className="opacity-60" />
            ) : (
              <IconEye width={18} height={18} className="opacity-60" />
            )}
          </button>
        </div>
        <button
          type="submit"
          className="w-full p-3 mt-8 bg-accent-primary border-none outline-none rounded-[4px] text-white font-medium text-xl  disabled:bg-bg-secondary disabled:cursor-not-allowed"
          disabled={isDisabled}
        >
          Tạo mật khẩu
        </button>
      </form>
      <p className="mt-8 text-xs">
        Qua việc đăng nhập hoặc tạo tài khoản, bạn đồng ý với các{' '}
        <a href="#" className="text-blue-500">
          Điều khoản và Điều kiện
        </a>{' '}
        cũng như{' '}
        <a href="#" className="text-blue-500">
          Chính sách An toàn và Bảo mật
        </a>{' '}
        của chúng tôi
      </p>
    </div>
  )
}

export default FormEnterPassword
