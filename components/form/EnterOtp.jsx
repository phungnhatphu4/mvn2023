import React, { useState, useEffect, useMemo } from 'react'
import { useDispatch } from 'react-redux'
import { DISPLAY_TYPE } from 'utils/constants'
import firebase from 'firebase/compat/app'
import Button from 'components/common/Button/Button'

const FormEnterOtp = (props) => {
  const { onChangeView, setFormInfo, formInfo } = props

  const dispatch = useDispatch()

  const [code, setcode] = useState(new Array(6).fill(''))
  const [countDown, setCountDown] = useState(30)
  const [isShowCountDown, setIsShowCountDown] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')

  const isDisabled = useMemo(() => code.some((item) => item === ''), [code])

  const handleChange = (element, index) => {
    if (isNaN(element.value)) return false
    setcode([...code.map((d, indx) => (indx === index ? element.value : d))])
    if (element.nextSibling && element.value !== '') {
      element.nextSibling.focus()
    }
  }

  const handleVerifyOtp = async (otp) => {
    setErrorMessage('')
    try {
      await formInfo.confirmationResult.confirm(otp)
      const token = await firebase.auth().currentUser.getIdToken(false)
      setFormInfo({ ...formInfo, token })
      onChangeView(DISPLAY_TYPE.ENTER_USER_INFO)
      firebase.auth().signOut()
    } catch (err) {
      console.log('err', err)
      switch (err.code) {
        case 'auth/invalid-verification-code': {
          setErrorMessage('Mã xác thực không đúng')
          break
        }
        case 'auth/code-expired': {
          setErrorMessage(
            'Mã xác thực hết hạn. Vui lòng bấm gửi lại mã xác thực',
          )
          break
        }
        default:
          break
      }
    }
  }

  const onSubmit = (event) => {
    event.preventDefault()
    if (isDisabled) return
    const otp = code.join('')
    handleVerifyOtp(otp)
    setIsShowCountDown(true)
  }

  useEffect(() => {
    if (isShowCountDown) {
      const interval = setInterval(() => {
        if (countDown > 0) {
          setCountDown(countDown - 1)
        } else {
          setIsShowCountDown(false)
          setCountDown(60)
        }
      }, 1000)
      return () => clearInterval(interval)
    }
  }, [isShowCountDown, countDown])

  return (
    <div className="px-4 pb-6 authentication bg-black md:p-6">
      <p className="text-[#fff] text-[15px] font-[500] font-['Bitter'] mb-4">
        Vui lòng nhập mã xác thực được gửi đến số điện thoại của bạn để tạo mật
        khẩu mới.
      </p>
      <form className="md:text-center" onSubmit={onSubmit}>
        <div className="flex space-x-4 mb-4 md:w-full md:justify-center">
          {code.map((data, index) => {
            return (
              <input
                type="text"
                className="otp-field w-[36px] border-b text-center text-3xl py-2 outline-none rounded-[6px] text-black"
                name="otp"
                maxLength={1}
                key={index}
                value={data}
                onChange={(e) => handleChange(e.target, index)}
                onFocus={(e) => e.target.select}
                autoFocus={index === 0}
              />
            )
          })}
        </div>

        <p className="text-[#fff] text-[15px] font-[500] font-['Bitter'] mb-6">
          Không nhận được mã?{' '}
          <span className="text-[#F0C753]">Gửi lại sau {countDown}s</span>
        </p>

        {errorMessage ? (
          <div className="text-[12px] text-[#FD3E30] mb-4 text-center">
            {errorMessage}
          </div>
        ) : (
          <></>
        )}
        <Button
          className="py-[14px] text-[14px] leading-[16.8px] text-white w-full mb-4"
          disabled={isDisabled || isShowCountDown}
          onClick={onSubmit}
        >
          Xác thực
        </Button>
      </form>
    </div>
  )
}

export default FormEnterOtp
