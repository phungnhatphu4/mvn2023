import { CalenderIcon, ClockIcon, PartyIcon, UserIcon } from 'assets/index'
import { useMemo } from 'react'
import ReactDatePicker from 'react-datepicker'
import Select from 'react-select'

const customStyles = {
  control: () => ({
    display: 'flex',
  }),
  valueContainer: (provided) => ({
    ...provided,
    paddingLeft: '0',
    paddingRight: '0',
  }),
  indicatorSeparator: () => ({
    display: 'none',
  }),
  singleValue: (provided) => {
    return {
      ...provided,
      fontSize: '12px',
      lineHeight: '14.4px',
      color: '#ffffff',
    }
  },
  placeholder: (provided) => {
    return {
      ...provided,
      fontSize: '12px',
      lineHeight: '14.4px',
      color: '#ffffff',
      marginLeft: '0',
      marginRight: '0',
    }
  },
}

const ChooseOrderTime = (props) => {
  const {
    startTime,
    setStartTime,
    numberPeople,
    setNumberPeople,
    partyType,
    setPartyType,
    checkInTime,
    setCheckInTime,
  } = props
  const numberPeopleOptions = useMemo(() => {
    let result = []
    for (let i = 1; i < 21; i++) {
      result.push({ value: i, label: i })
    }
    return result
  }, [])

  const handleChangDate = (date) => {
    setCheckInTime(date)
  }

  const handleChangeTimePicker = (value) => {
    setStartTime(value)
  }

  const handleChangeNumber = (selectedOption) => {
    setNumberPeople(selectedOption.value)
  }

  const handleChangeType = (selectedOption) => {
    setPartyType(selectedOption.value)
  }

  const partyTypeOptions = useMemo(() => {
    return [
      { value: 'Sinh nhật', label: 'Sinh nhật' },
      { value: 'Kỷ niệm', label: 'Kỷ niệm' },
      { value: 'Hẹn hò', label: 'Hẹn hò' },
      { value: 'Cầu hôn', label: 'Cầu hôn' },
    ]
  }, [])

  return (
    <div className="border border-solid border-[#AF8643] p-3 flex flex-col my-6 md:mt-4">
      <div className="w-full flex mb-4">
        <div className="flex items-center w-1/2 border-r-2 border-solid border-[#FFFFFF]">
          <CalenderIcon className="mr-2 min-w-[20px]" />
          <ReactDatePicker
            className="text-[12px] file:border-none bg-transparent text-white w-full outline-none placeholder:text-white"
            selected={checkInTime}
            onChange={handleChangDate}
            dateFormat="dd/MM/yyyy"
            placeholderText="Ngày đặt"
            minDate={new Date()}
          />
        </div>
        <div className="flex items-center w-1/2 pl-3">
          <ClockIcon className="mr-2 min-w-[20px]" />
          <ReactDatePicker
            className="text-[12px] border-none bg-transparent text-white w-full outline-none placeholder:text-white"
            selected={startTime}
            placeholderText="Giờ nhận bàn"
            onChange={(date) => handleChangeTimePicker(date)}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption="Giờ"
            dateFormat="HH:mm"
            timeFormat="HH:mm"
            minTime={new Date(0, 0, 0, 17, 0)}
            maxTime={new Date(0, 0, 0, 22, 0)}
          />
        </div>
      </div>
      <div className="w-full bg-[#F0C753] h-px" />
      <div className="w-full flex mt-4">
        <div className="flex items-center w-1/2 border-r-2 border-solid border-[#FFFFFF]">
          <UserIcon className="mr-2 min-w-[20px]" />
          <Select
            styles={customStyles}
            options={numberPeopleOptions}
            className="border-none"
            value={numberPeopleOptions.find(
              (item) => item.value === numberPeople,
            )}
            placeholder="Số lượng người"
            onChange={handleChangeNumber}
            isSearchable={false}
          />
        </div>
        <div className="flex items-center w-1/2 pl-3">
          <PartyIcon className="mr-2 min-w-[20px]" />
          <Select
            styles={customStyles}
            options={partyTypeOptions}
            className="border-none"
            value={partyTypeOptions.find((item) => item.value === partyType)}
            onChange={handleChangeType}
            placeholder="Chọn loại tiệc"
            isSearchable={false}
          />
        </div>
      </div>
    </div>
  )
}

export default ChooseOrderTime
