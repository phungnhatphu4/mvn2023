import Link from 'next/link'
import { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { checkPhoneNumberHandle } from 'redux/actions/auth.action'
import { DISPLAY_TYPE } from 'utils/constants'
import firebase from 'firebase/compat/app'
import Button from 'components/common/Button/Button'
import toast from 'react-hot-toast'

const FormEnterPhone = (props) => {
  const { onChangeView, formInfo, setFormInfo, openModalSignIn, closeModal } =
    props

  const dispatch = useDispatch()

  const [phoneNumber, setPhoneNumber] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  const [applicationVerifier, setApplicationVerifier] = useState(null)
  const [hasSubmit, setHasSubmit] = useState(false)

  const renderRecapcha = () => {
    setHasSubmit(false)

    const tempApplicationVerifier = new firebase.auth.RecaptchaVerifier(
      'recaptcha-container',
      {
        callback: () => {
          setHasSubmit(true)
        },
      },
    )
    firebase.auth().languageCode = 'vi'
    tempApplicationVerifier.render().then((widgetId) => {
      window.recaptchaWidgetId = widgetId
    })
    setApplicationVerifier(tempApplicationVerifier)
  }

  useEffect(() => {
    renderRecapcha()
  }, [])

  const onSuccess = (res) => {
    if (!res.data.isExist) {
      setErrorMessage('')
      const tempPhoneNumber = phoneNumber.slice(1, phoneNumber.length)
      firebase
        .auth()
        .signInWithPhoneNumber(`+84${tempPhoneNumber}`, applicationVerifier)
        .then((confirmationResult) => {
          setFormInfo({ ...formInfo, phone: phoneNumber, confirmationResult })
          applicationVerifier.clear()
          onChangeView(DISPLAY_TYPE.ENTER_OTP)
        })
        .catch((err) => {
          switch (err.code) {
            case 'auth/too-many-requests': {
              setErrorMessage(
                'Số điện thoại đã bị khóa do yêu cầu mã xác thực quá nhiều',
              )
              break
            }

            default:
              break
          }
        })
    } else {
      setErrorMessage('Số điện thoại đã tồn tại')
    }
  }

  const onError = (err) => {
    toast.error('Có lỗi xảy ra vui lòng thử lại!')
    console.log(err)
  }

  const handleCheckPhone = (phoneNumber) => {
    if (!phoneNumber) {
      setErrorMessage('Vui lòng nhập số điện thoại')
    } else {
      let phoneNumReg = /^(\+84|\+840|84|084|840|0)([9235678]{1}[0-9]{8})$/
      const isPhoneNumberValidate = phoneNumReg.test(phoneNumber)
      if (phoneNumber && !isPhoneNumberValidate) {
        setErrorMessage('Số điện thoại sai định dạng')
      } else {
        setErrorMessage('')
      }
    }
  }

  const onChangePhoneNumber = (e) => {
    const value = e.target.value
    setPhoneNumber(value)
    handleCheckPhone(value)
  }

  const onSubmit = (event) => {
    event.preventDefault()

    dispatch(checkPhoneNumberHandle(phoneNumber, onSuccess, onError))
  }

  return (
    <div className="px-4 pb-6 authentication bg-[#000] md:p-6">
      <p className="text-[#fff] text-[15px] font-[500] font-['Bitter'] mb-[5px]">
        Đăng ký/đăng nhập - Tích điểm và nhận ưu đãi
      </p>
      <p className="text-[#BDBDBD] text-[13px] font-[400] font-['Bitter'] mb-6">
        Đăng ký liền tay, rinh ngay quyền lợi.
      </p>
      <label className="block mb-4">
        <span className="block text-sm font-medium text-white font-[500] text-[15px]">
          Số điện thoại*
        </span>
        <input
          className="mt-2 p-3 border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 bg-transparent"
          placeholder="Nhập số điện thoại"
          onChange={onChangePhoneNumber}
        />
        {errorMessage ? (
          <div className="mt-2 text-[12px] text-[#FD3E30]">{errorMessage}</div>
        ) : (
          <></>
        )}
      </label>

      <div
        className="recaptcha"
        id="recaptcha-container"
        style={{ marginTop: 8, marginBottom: 8 }}
      />

      <Button
        disabled={!phoneNumber || errorMessage || !hasSubmit}
        className="py-[14px] text-[14px] leading-[16.8px] text-white w-full mb-4"
        onClick={onSubmit}
      >
        Đăng ký
      </Button>
      <p className="text-white text-[13px] font-[400] text-center md:mb-[36px]">
        Tôi đồng ý với{' '}
        <a href="#" className="text-blue-500">
          Chính sách bảo mật
        </a>{' '}
        và{' '}
        <a href="#" className="text-blue-500">
          Điều khoảng hoạt động
        </a>{' '}
        của Marsvenus
      </p>

      <p className="text-white text-[15px] font-[400] text-center">
        Đã có tài khoản?{' '}
        <button
          onClick={() => {
            closeModal()
            openModalSignIn()
          }}
          className="text-[#F0C753]"
        >
          Đăng nhập
        </button>
      </p>
    </div>
  )
}

export default FormEnterPhone
