import toast from 'react-hot-toast'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { verifyTokenFirebaseHandle } from 'redux/actions/auth.action'
import Button from 'components/common/Button/Button'

const FormEnterUserInfo = ({ formInfo, openModalSignIn, closeModal }) => {
  const [name, setName] = useState('')
  const [isErrorName, setErrorName] = useState(false)
  const [isErrorPassword, setErrorPassWord] = useState(false)

  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [errorPasswordCf, setErrorPassWordCf] = useState(false)

  const dispatch = useDispatch()

  const handleCreateUserInfo = () => {
    dispatch(
      verifyTokenFirebaseHandle(
        formInfo.phone,
        name,
        formInfo.token,
        password,
        (res) => {
          closeModal()
          openModalSignIn()
          toast('Tạo tài khoản thành công!')
        },
        (err) => {
          toast.error('Có lỗi xảy ra, vui lòng thử lại sau!')
          console.log(err)
        },
      ),
    )
  }

  const onSubmit = () => {
    handleCreateUserInfo()
  }

  const onChangeName = (e) => {
    const value = e.target.value
    setName(value)
    if (!value) {
      setErrorName(true)
    } else {
      setErrorName(false)
    }
  }

  const onChangePassword = (e) => {
    const value = e.target.value
    setPassword(value)
    if (!value) {
      setErrorPassWord(true)
    } else {
      setErrorPassWord(false)
    }
  }

  const onChangeConfirm = (e) => {
    const value = e.target.value
    setConfirmPassword(value)
    if (!value) {
      setErrorPassWordCf('Vui lòng nhập')
    } else if (value !== password) {
      setErrorPassWordCf('Mật khẩu không trùng khớp')
    } else {
      setErrorPassWordCf('')
    }
  }

  return (
    <div className="px-4 pb-6 authentication bg-black">
      <label className="block mb-4">
        <span className="block text-sm font-medium text-white font-[500] text-[15px]">
          Họ và tên
        </span>
        <input
          autoComplete=""
          className="mt-2 p-3 border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 bg-transparent"
          placeholder="Nhập họ và tên"
          onChange={onChangeName}
        />
        {isErrorName ? (
          <div className="mt-2 text-[12px] text-[#FD3E30]">
            Vui lòng nhập tên
          </div>
        ) : (
          <></>
        )}
      </label>
      <label className="block mb-4">
        <span className="block text-sm font-medium text-white font-[500] text-[15px]">
          Tạo mật khẩu (Tối thiểu 8 ký tự)
        </span>
        <input
          autoComplete="false"
          type="password"
          name="phone-number"
          className="mt-2 p-3 border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 bg-transparent"
          placeholder="Nhập mật khẩu"
          onChange={onChangePassword}
          minLength={8}
        />
        {isErrorPassword ? (
          <div className="mt-2 text-[12px] text-[#FD3E30]">
            Vui lòng nhập mật khẩu
          </div>
        ) : (
          <></>
        )}
      </label>
      <label className="block mb-6">
        <span className="block text-sm font-medium text-white font-[500] text-[15px]">
          Xác nhận mật khẩu
        </span>
        <input
          autoComplete="false"
          type="password"
          className="mt-2 p-3 border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 bg-transparent"
          placeholder="Xác nhận lại mật khẩu"
          onChange={onChangeConfirm}
        />

        {errorPasswordCf ? (
          <div className="mt-2 text-[12px] text-[#FD3E30]">
            {errorPasswordCf}
          </div>
        ) : (
          <></>
        )}
      </label>

      <Button
        className="py-[14px] text-[14px] leading-[16.8px] text-white w-full mb-4"
        onClick={onSubmit}
        disabled={
          !name ||
          !password ||
          !confirmPassword ||
          isErrorName ||
          isErrorPassword ||
          errorPasswordCf
        }
      >
        Tạo tài khoản
      </Button>
    </div>
  )
}

export default FormEnterUserInfo
