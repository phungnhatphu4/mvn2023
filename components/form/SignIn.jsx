import Button from 'components/common/Button/Button'
import Link from 'next/link'
import toast from 'react-hot-toast'
import React, { useState } from 'react'

import { useMemo } from 'react'
import { useDispatch } from 'react-redux'
import { loginHandle } from 'redux/actions/auth.action'

const FormSignIn = ({ closeModal, openModalSignUp }) => {
  const [phoneNumber, setPhoneNumber] = useState('')
  const [password, setPassword] = useState('')

  const dispatch = useDispatch()

  const onSuccess = (res) => {
    closeModal()
  }

  const onError = (err) => {
    toast.error('Thông tin đăng nhập không đúng')
  }

  const handleLogin = () => {
    dispatch(loginHandle({ password, phone: phoneNumber }, onSuccess, onError))
  }

  const isDisabled = useMemo(
    () => phoneNumber === '' || password === '',
    [phoneNumber, password],
  )

  const onChangePhoneNumber = (e) => {
    const value = e.target.value.replace(/\D/g, '')
    setPhoneNumber(value)
  }

  const onChangePassword = (event) => {
    const { value } = event.target
    setPassword(value)
  }

  return (
    <div className="p-6">
      <div className="mb-6">
        <p className="text-[#fff] text-[15px] leading-[21.3px] font-medium mb-1">
          Chào mừng bạn! Hãy đăng nhập để tiếp tục
        </p>
        <p className="text-[#BDBDBD] text-[13px] leading-[18.46px] font-normal">
          Đăng ký/đăng nhập liền tay, rinh ngay quyền lợi.
        </p>
      </div>
      <label className="block mb-4">
        <span className="block text-sm font-medium text-white font-[500] text-[15px]">
          Số điện thoại*
        </span>
        <input
          name="phone-number"
          className="mt-2 p-3 border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 bg-transparent"
          placeholder="Nhập số điện thoại"
          onChange={onChangePhoneNumber}
        />
      </label>
      <label className="block mb-3">
        <span className="block text-sm font-medium text-white font-[500] text-[15px]">
          Mật khẩu *
        </span>
        <input
          name="password"
          type="password"
          className="mt-2 p-3 border border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-sm focus:ring-1 bg-transparent"
          placeholder="Nhập mật khẩu"
          onChange={onChangePassword}
        />
      </label>
      <div className="text-[#F0C753] text-right mb-[52px]">
        <Link href="forgot_password">Quên mật khẩu?</Link>
      </div>
      <Button
        className="w-full mb-4 p-[14px]"
        disabled={isDisabled}
        onClick={handleLogin}
      >
        Tiếp tục
      </Button>
      <p className="text-white text-[13px] font-[400] text-center mb-9">
        Tôi đồng ý với{' '}
        <a href="#" className="text-blue-500">
          Chính sách bảo mật
        </a>{' '}
        và{' '}
        <a href="#" className="text-blue-500">
          Điều khoảng hoạt động
        </a>{' '}
        của Marsvenus
      </p>
      <p className="text-white text-[15px] font-[400] text-center">
        Bạn chưa có tài khoản?{' '}
        <button
          onClick={() => {
            openModalSignUp()
            closeModal()
          }}
          className="text-[#F0C753]"
        >
          Đăng ký
        </button>
      </p>
    </div>
  )
}

export default FormSignIn
