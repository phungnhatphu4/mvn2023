import FormSignIn from 'components/form/SignIn'
import React from 'react'
import ModalCommon from 'components/Modal/ModalCommon'

const SignIn = ({ isOpen = false, closeModal, openModalSignUp }) => {
  return (
    <ModalCommon
      isOpen={isOpen}
      title={'Đăng nhập'}
      closeModal={closeModal}
      isFullScreen
    >
      <div>
        <FormSignIn closeModal={closeModal} openModalSignUp={openModalSignUp} />
      </div>
    </ModalCommon>
  )
}

export default SignIn
