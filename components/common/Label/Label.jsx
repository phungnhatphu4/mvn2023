const Label = ({ required = false, children, ...restProps }) => {
  return (
    <label {...restProps} className="text-[13px] leading-[18.2px]">
      {children} {required && <span className="text-[#FD3E30]">*</span>}
    </label>
  )
}

export default Label
