import clsx from 'clsx'

const Input = ({
  type = 'text',
  isInvalid = false,
  className,
  ...restProps
}) => {
  if (type === 'textarea') {
    return (
      <textarea
        {...restProps}
        className={clsx(
          'py-2 px-3 border border-[#F0C753] rounded-xl outline-none bg-transparent text-[13px] leading-[18.2px] resize-none',
          isInvalid && 'border-[#FD3E30]',
          className,
        )}
      />
    )
  }
  return (
    <input
      type={type}
      {...restProps}
      className={clsx(
        'py-2 px-3 border border-[#F0C753] rounded-xl outline-none bg-transparent text-[13px] leading-[18.2px]',
        isInvalid && 'border-[#FD3E30]',
        className,
      )}
    />
  )
}

export default Input
