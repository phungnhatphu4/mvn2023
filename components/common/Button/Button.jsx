import clsx from 'clsx'

const Button = ({
  type = 'button',
  children,
  className,
  isSmallButton = false,
  ...restProps
}) => {
  return (
    <button
      type={type}
      {...restProps}
      className={clsx(
        'px-8 py-2 bg-primary-linear text-[13px] leading-[15.6px] disabled:bg-none disabled:bg-[#797979]',
        isSmallButton ? 'rounded-[16px] ' : 'rounded md:py-[14px]',
        className,
      )}
    >
      {children}
    </button>
  )
}
export default Button
