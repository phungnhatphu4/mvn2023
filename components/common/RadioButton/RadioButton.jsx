const RadioButton = ({ label, onChange, ...restProps }) => {
  return (
    <label className="inline-block items-center relative pl-5 cursor-pointer select-none text-white">
      {label}
      <input
        type="radio"
        {...restProps}
        className="peer absolute opacity-0 cursor-pointer h-0 w-0"
        onChange={onChange}
      />
      <span className="absolute top-2/4 left-0 -translate-y-2/4 h-[16px] w-[16px] border border-[#F0C753] rounded-full after:content-[''] after:absolute after:hidden after:w-[10px] after:h-[10px] after:bg-[#F0C753] after:rounded-full after:top-[2px] after:left-[2px]  peer-checked:after:block"></span>
    </label>
  )
}

export default RadioButton
