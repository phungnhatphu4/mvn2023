import { useCallback, useState } from 'react'

import FormEnterOtp from 'components/form/EnterOtp'
import FormEnterPhone from 'components/form/EnterPhone'
import { DISPLAY_TYPE } from 'utils/constants'
import FormEnterUserInfo from 'components/form/EnterUserInfo'
import ModalCommon from 'components/Modal/ModalCommon'

const SignUp = ({ isOpen = false, closeModal, openModalSignIn }) => {
  const [stackView, setStackView] = useState([])
  const [displayType, setDisplayType] = useState(DISPLAY_TYPE.ENTER_PHONE)
  const [formInfo, setFormInfo] = useState({})

  const handleChangeView = useCallback(
    (type) => {
      setDisplayType(type)
      setStackView((prev) => [...prev, displayType])
    },
    [displayType],
  )

  const handleBack = useCallback(() => {
    const lastView = stackView.pop()
    if (lastView) {
      setDisplayType(lastView)
      setStackView(stackView)
    }
  }, [displayType, stackView])

  const renderContent = useCallback(() => {
    switch (displayType) {
      case DISPLAY_TYPE.ENTER_PHONE:
        return (
          <FormEnterPhone
            onChangeView={handleChangeView}
            setFormInfo={setFormInfo}
            formInfo={formInfo}
            openModalSignIn={openModalSignIn}
            closeModal={closeModal}
          />
        )

      case DISPLAY_TYPE.ENTER_OTP:
        return (
          <FormEnterOtp
            onChangeView={handleChangeView}
            onBack={handleBack}
            setFormInfo={setFormInfo}
            formInfo={formInfo}
          />
        )

      case DISPLAY_TYPE.ENTER_USER_INFO:
        return (
          <FormEnterUserInfo
            formInfo={formInfo}
            openModalSignIn={openModalSignIn}
            closeModal={closeModal}
          />
        )

      default:
        return null
    }
  }, [displayType, stackView])

  const renderTitleModal = () => {
    switch (displayType) {
      case DISPLAY_TYPE.ENTER_PHONE:
        return 'Đăng ký tài khoản'

      case DISPLAY_TYPE.ENTER_OTP:
        return 'Xác thực số điện thoại'

      case DISPLAY_TYPE.ENTER_USER_INFO:
        return 'Tạo tài khoản'

      default:
        return ''
    }
  }

  return (
    <ModalCommon
      isOpen={isOpen}
      title={renderTitleModal()}
      closeModal={closeModal}
      isFullScreen
    >
      <div>{renderContent()}</div>
    </ModalCommon>
  )
}

export default SignUp
