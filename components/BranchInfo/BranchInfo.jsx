import SwiperCoverFlow from 'components/swiper/SwiperCoverFlow'
import _isEmpty from 'lodash/isEmpty'
import ChooseOrderTime from 'components/form/ChooseOrderTime'

import { StartIcon, CloseTimeIcon, OpenTimeIcon } from 'assets/index'

const BranchInfo = ({
  selectedBranch = {},
  startTime,
  setStartTime,
  numberPeople,
  setNumberPeople,
  partyType,
  setPartyType,
  checkInTime,
  setCheckInTime,
  isShowChooseOrderTime = true,
}) => {
  if (_isEmpty(selectedBranch)) return <></>
  return (
    <div className="md:flex md:items-center md:gap-x-14">
      <div className="mb-4 md:w-[375px] md:mb-0">
        <SwiperCoverFlow slides={selectedBranch?.image_urls} />
      </div>
      <div className="flex-1 text-left">
        <div className="text-lg leading-[26px] uppercase text-white mb-2 md:text-[28px]">
          {selectedBranch?.name}
        </div>
        <div className="text-sm leading-[21px] text-[#BDBDBD] mb-2 md:text-base">
          {selectedBranch?.address}
        </div>
        <div className="flex items-center mb-3">
          <StartIcon className="mr-1" />
          <StartIcon className="mr-1" />
          <StartIcon className="mr-1" />
          <StartIcon className="mr-1" />
          <StartIcon className="mr-[25px]" />
          <span className="text-[#2F80ED]">
            {selectedBranch?.number_of_feedback} đánh giá
          </span>
        </div>
        <div className="flex mb-3">
          <div className="flex items-baseline mr-6">
            <OpenTimeIcon />
            <span className="font-[13px] text-[#C1A982] ml-2">
              {selectedBranch?.open_time}
            </span>
          </div>
          <div className="flex items-baseline">
            <CloseTimeIcon />
            <span className="font-[13px] text-[#C1A982] ml-2">
              {selectedBranch?.close_time}
            </span>
          </div>
        </div>
        <span className="text-[#BDBDBD] line-clamp-5 text-[14px] leading-[21px]">
          {selectedBranch?.description}
        </span>
        {isShowChooseOrderTime && (
          <ChooseOrderTime
            startTime={startTime}
            setStartTime={setStartTime}
            numberPeople={numberPeople}
            setNumberPeople={setNumberPeople}
            partyType={partyType}
            setPartyType={setPartyType}
            checkInTime={checkInTime}
            setCheckInTime={setCheckInTime}
          />
        )}
      </div>
    </div>
  )
}

export default BranchInfo
