import Image from 'next/image'

const PostItem = ({ image }) => {
  return (
    <div className="w-full flex-col">
      <div className="pt-[59.25%] relative mb-2">
        <div className="w-full h-full absolute top-0">
          <Image
            src={image}
            alt="thumbnail-post"
            className="object-cover"
            layout="fill"
          />
        </div>
      </div>
      <div>
        <div className="text-[13px] leading-[18.46px] text-white mb-[5px]">
          Thêm không gian, thên chốn hẹn hò
        </div>
        <div className="text-[10px] leading-3 text-[#858688] mb-1">
          Ngày 08 Tháng 07 Năm 2022
        </div>
        <div className="text-[13px] leading-[18.46px] text-white truncate">
          Những ngày cuối tuần là khoảng thời gian để “refresh” bản thân, cho
          mình giây phút thư giãn trước khi bắt đầu tuần mới. Vì thế, hãy thưởng
          ngay cho mình một bữa tối sang trọng và lãng mạn cùng người thương tại
          hệ thống Nhà hàng Mars Venus.
        </div>
      </div>
    </div>
  )
}

export default PostItem
