const SelectBranch = ({
  listBranch = [],
  onClickBranch = () => {},
  selectedBranch,
  zoomSelected = false
}) => {
  if (!listBranch.length) return <></>

  const getColClass = (branch) => {
    if (!zoomSelected) {
      return `basis-2/6 flex flex-col`;
    } else {
      if (branch?.id === selectedBranch?.id) {
        return `w-[40%] md:w-full`;
      } else {
        return `w-[30%] md:w-full`;
      }
    }
  }

  return (
    <div className="flex md:gap-x-6 justify-items-stretch">
      {listBranch.map((branch, index) => {
        return (
          <div
            className={getColClass(branch)}
            key={branch.name}
            onClick={() => onClickBranch(branch)}
          >
            <div
              className={`${
                branch?.id === selectedBranch?.id
                  ? 'bg-primary-linear border-[#F0C753]'
                  : 'bg-[#797979] border-white'
              } ${zoomSelected ? 'h-[30px]' : ''} flex-1 text-center text-white text-[8px] leading-[9.6px] border py-[5px] uppercase lg:text-sm lg:leading-5`}
            >
              {branch.name}
            </div>
            <div
              className={`pt-[100%] relative border ${
                branch?.id === selectedBranch?.id
                  ? 'border-[#F0C753]'
                  : 'border-white grayscale'
              }`}
            >
              <div className="absolute w-full h-full top-0">
                <img
                  src={branch.image_url}
                  alt={branch.name}
                  className="w-full h-full object-contain"
                />
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default SelectBranch
