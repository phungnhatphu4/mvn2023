import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation, Thumbs } from 'swiper'
import { useState } from 'react'
import Image from 'next/image'

import Position1 from 'assets/images/home/position_1.jpg'
import Position2 from 'assets/images/home/position_2.jpg'
import Position3 from 'assets/images/home/position_3.jpg'
import Position4 from 'assets/images/home/position_4.jpg'
import Position5 from 'assets/images/home/position_5.jpg'
import Position6 from 'assets/images/home/position_6.jpg'
import Position7 from 'assets/images/home/position_7.jpg'
import Position8 from 'assets/images/home/position_8.jpg'
import Position9 from 'assets/images/home/position_9.jpg'

const slides = [
  {
    image: Position1,
    name: 'Sảnh đồng quê kiểu mỹ',
  },
  {
    image: Position2,
    name: 'Không gian view sông',
  },
  {
    image: Position3,
    name: 'Không gian hàng rào gỗ',
  },
  {
    image: Position4,
    name: 'Sản Vintage kiểu Ý cổ điển',
  },
  {
    image: Position5,
    name: 'Không gian cửa kính',
  },
  {
    image: Position6,
    name: 'Sảnh Vintage kiểu Ý tân cổ điển',
  },
  {
    image: Position7,
    name: 'Không gian cửa kính',
  },
  {
    image: Position8,
    name: 'Không gian ngoài trời',
  },
  {
    image: Position9,
    name: 'Không gian hầm rượu',
  },
]

const SwiperPerfectSpace = ({selectedBranch}) => {
  
  const brachid = selectedBranch?.id;
  console.log(brachid);
  const [thumbsSwiper, setThumbsSwiper] = useState(null)
  return (
    <div>
      <div className="flex text-white gap-[13px]">
        <div className="w-[78px] md:w-[200px]">
          <div>
            <Swiper
              onSwiper={setThumbsSwiper}
              direction="vertical"
              spaceBetween={12}
              slidesPerView="auto"
              className="max-h-[289px] md:max-h-[500px]"
              loop
              modules={[Navigation, Thumbs]}
            >
              {slides.map((slide, index) => {
                return (
                  <SwiperSlide key={index}>
                    <div className="w-full flex-col">
                      <div className="pt-[100%] relative mb-2">
                        <div className="absolute top-0 w-full h-full">
                          <Image
                            src={slide.image}
                            alt="title"
                            className="object-cover"
                            layout="fill"
                          />
                        </div>
                      </div>
                      <div className="text-[10px] leading-[14px] text-center">
                        {slide.name}
                      </div>
                    </div>
                  </SwiperSlide>
                )
              })}
            </Swiper>
          </div>
        </div>
        <div className="w-[calc(100%_-_78px)] md:w-[calc(100%_-_200px)]">
          <div>
            <Swiper
              thumbs={{
                swiper:
                  thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null,
              }}
              direction="horizontal"
              slidesPerView={1}
              spaceBetween={32}
              loop
              className="w-full overflow-hidden"
              modules={[Navigation, Thumbs]}
            >
              {slides.map((slide, index) => {
                return (
                  <SwiperSlide key={index}>
                    <div className="w-full flex-col pb-[14px]">
                      <div className="pt-[100%] relative mb-2 md:pt-[60%]">
                        <div className="absolute top-0 w-full h-full">
                          <Image
                            src={slide.image}
                            alt="title"
                            className="object-cover"
                            layout="fill"
                          />
                        </div>
                        <div className="text-[13px] lg:text-[20px] leading-[16.9px] text-center absolute bottom-[-22px] md:bottom-3 translate-x-[-50%] left-[50%]">
                          {slide.name}
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                )
              })}
            </Swiper>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SwiperPerfectSpace
