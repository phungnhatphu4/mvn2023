import { NextIcon, PrevIcon } from 'assets/index'
import clsx from 'clsx'
import { useEffect, useRef, useState } from 'react'
import { Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import Image from 'next/image'

import SpecialFood1 from 'assets/images/home/special_food_1.png'
import SpecialFood2 from 'assets/images/home/special_food_2.png'
import SpecialFood3 from 'assets/images/home/special_food_3.png'
import SpecialFood4 from 'assets/images/home/special_food_4.png'

const slides = [SpecialFood1, SpecialFood2, SpecialFood3, SpecialFood4]

const SwiperSpecialDelicacies = () => {
  const navigationPrevRef = useRef(null)
  const navigationNextRef = useRef(null)
  const [isClient, setIsClient] = useState(false)
  useEffect(() => {
    setIsClient(true)
  }, [])

  return (
    <div className="flex items-center">
      <div className="flex">
        <button ref={navigationPrevRef}>
          <PrevIcon />
        </button>
      </div>
      <Swiper
        navigation={{
          prevEl: navigationPrevRef.current,
          nextEl: navigationNextRef.current,
        }}
        slidesPerView="auto"
        breakpoints={{
          320: {
            slidesPerView: 1,
          },
          768: {
            slidesPerView: 3,
          },
        }}
        loop
        slideToClickedSlide
        modules={[Navigation]}
        centeredSlides
        className="flex-1"
      >
        {slides.map((slide, index) => {
          return (
            <SwiperSlide key={index}>
              {({ isActive }) => {
                return (
                  <div
                    className={clsx(
                      'w-full p-[22px] md:p-0',
                      !isActive && 'scale-[80%]',
                    )}
                  >
                    <div className="pt-[100%] relative">
                      <div className="absolute top-0 w-full h-full">
                        <Image
                          src={slide}
                          alt="title"
                          className="object-cover"
                          layout="fill"
                        />
                      </div>
                    </div>
                  </div>
                )
              }}
            </SwiperSlide>
          )
        })}
      </Swiper>
      <div className="flex">
        <button ref={navigationNextRef}>
          <NextIcon />
        </button>
      </div>
    </div>
  )
}

export default SwiperSpecialDelicacies
