import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation, Thumbs } from 'swiper'
import Image from 'next/image'

import Service1 from 'assets/images/home/service_1.png'
import Service2 from 'assets/images/home/service_2.png'
import Service3 from 'assets/images/home/service_3.png'
import Service4 from 'assets/images/home/service_4.png'

const slides = [Service1, Service2, Service3, Service4]

const SwiperFeaturedService = () => {
  return (
    <div>
      <Swiper
        direction="horizontal"
        slidesPerView="auto"
        breakpoints={{
          320: {
            slidesPerView: 1.3,
            spaceBetween: 12,
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 18,
          },
          1024: {
            slidesPerView: 4,
            spaceBetween: 24,
          },
        }}
        loop
        className="w-full overflow-hidden"
        modules={[Navigation, Thumbs]}
      >
        {slides.map((slide, index) => {
          return (
            <SwiperSlide key={index}>
              <div className="flex-col">
                <div className="pt-[100%] relative">
                  <div className="absolute top-0 w-full h-full">
                    <Image
                      src={slide}
                      alt="title"
                      className="object-cover"
                      layout="fill"
                    />
                  </div>
                </div>
              </div>
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  )
}

export default SwiperFeaturedService
