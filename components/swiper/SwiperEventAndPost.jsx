import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation, Thumbs } from 'swiper'
import PostItem from 'components/PostItem/PostItem'

import New1 from 'assets/images/home/new_1.png'
import New2 from 'assets/images/home/new_2.png'
import New3 from 'assets/images/home/new_3.png'
import New4 from 'assets/images/home/new_4.png'
import New5 from 'assets/images/home/new_5.png'

const slides = [New1, New2, New3, New4, New5]

const SwiperEventAndPost = () => {
  return (
    <div>
      <Swiper
        direction="horizontal"
        slidesPerView="auto"
        breakpoints={{
          320: {
            slidesPerView: 1.3,
            spaceBetween: 12,
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 18,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 24,
          },
        }}
        loop
        className="w-full overflow-hidden"
        modules={[Navigation, Thumbs]}
      >
        {slides.map((slide, index) => {
          return (
            <SwiperSlide key={index}>
              <PostItem image={slide} />
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  )
}

export default SwiperEventAndPost
