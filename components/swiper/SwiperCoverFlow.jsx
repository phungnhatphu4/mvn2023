import { EffectCoverflow } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

const SwiperCoverFlow = ({ slides = [] }) => {
  if (!slides.length) return null
  return (
    <div>
      <Swiper
        effect="coverflow"
        grabCursor={true}
        centeredSlides={true}
        slidesPerView="auto"
        loop={slides.length > 2}
        coverflowEffect={{
          rotate: 0,
          stretch: 210,
          depth: 300,
          modifier: 1,
          slideShadows: true,
        }}
        modules={[EffectCoverflow]}
        className="mySwiper"
        // breakpoints={BREAKPOINTS_SLIDE_OPTIONS}
      >
        {slides.map((slide, index) => {
          return (
            <SwiperSlide key={index} className="max-w-[275px]">
              <div className="max-w-[275px] flex-col">
                <div className="pt-[100%] relative">
                  <div className="absolute top-0 w-full h-full">
                    <img
                      src={slide}
                      alt="title"
                      className="w-full h-full object-contain"
                    />
                  </div>
                </div>
              </div>
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  )
}

export default SwiperCoverFlow
