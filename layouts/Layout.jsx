import Footer from 'components/footer/Footer'
import Header from 'components/header/Header'

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <main className="bg-[#141416] min-height-content">{children}</main>
      <Footer />
    </>
  )
}

export default Layout
