import Plugin from 'components/Booking/Plugin/Plugin'
import StepProgressBarVertical from 'components/Booking/StepProgressBarVertical/StepProgressBarVertical'
import ClientOnly from 'components/ClientOnly'
import Layout from './Layout'

const BookingLayout = ({ children }) => {
  return (
    <>
      <Layout>
        <div className="container">
          <div className="py-4">
            <ClientOnly className="mb-6">
              <Plugin />
            </ClientOnly>
            <div className="px-3 flex gap-x-6 xl:px-0">
              <div className="hidden lg:block">
                <StepProgressBarVertical />
              </div>
              <div className="min-w-0 flex-1">{children}</div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  )
}

export default BookingLayout
