import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

const ROUTER_STEPS = [
  {
    step: 1,
    routes: ['/dat-ban/chon-chi-nhanh'],
  },
  {
    step: 2,
    routes: ['/dat-ban/chon-vi-tri'],
  },
  {
    step: 3,
    routes: ['/dat-ban/chon-menu'],
  },
  {
    step: 4,
    routes: ['/dat-ban/chon-dich-vu'],
  },
  {
    step: 5,
    routes: ['/dat-ban/thanh-toan', '/dat-ban/hoan-tat'],
  },
]

export const useStep = () => {
  const [currentStep, setCurrentStep] = useState(null)
  const router = useRouter()
  useEffect(() => {
    const routeStep = ROUTER_STEPS.find((item) =>
      item.routes.includes(router.pathname),
    )
    if (routeStep) {
      setCurrentStep(routeStep.step)
    } else setCurrentStep(null)
  }, [router.pathname])
  return { currentStep }
}
