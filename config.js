const config = {
  apiEndpoint: process.env.API_URL || '',
  graphqlUri: process.env.GRAPHQL_URL || '',
}

export default config
