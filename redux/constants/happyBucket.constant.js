import { actionTypes } from '../actionTypes';

export const GET_HAPPY_BUCKET_BY_ID = actionTypes(
  'deal/GET_HAPPY_BUCKET_BY_ID'
);

export const GET_LIST_HAPPY_BUCKET_DEAL_HOME = actionTypes(
  'deal/GET_LIST_HAPPY_BUCKET_DEAL_HOME'
);

export const GET_LIST_HAPPY_BUCKET = actionTypes(
  'happyBucket/GET_LIST_HAPPY_BUCKET'
);
