import { actionTypes } from '../actionTypes';

export const GET_LIST_FOODS_MENU = actionTypes('order/GET_LIST_FOODS_MENU');
export const REQUEST_BOOKINGS_FOODS = actionTypes(
  'order/REQUEST_BOOKINGS_FOODS'
);
export const ADD_LIST_SELECTED = actionTypes('order/ADD_LIST_SELECTED');
export const UPDATE_LIST_SELECTED = actionTypes('order/UPDATE_LIST_SELECTED');

export const CLEAR_LIST_SELECTED = actionTypes('order/CLEAR_LIST_SELECTED');
