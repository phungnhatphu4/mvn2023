import { actionTypes } from '../actionTypes';

export const GET_LIST_PROMOTION = actionTypes('deal/GET_LIST_PROMOTION');

export const GET_LIST_PROMOTION_HOME = actionTypes(
  'news/GET_LIST_PROMOTION_HOME'
);

export const GET_PROMOTION_BY_ID = actionTypes(
  'news/GET_PROMOTION_BY_ID'
);
