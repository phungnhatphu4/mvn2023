import { actionTypes } from '../actionTypes';

export const ADD_VOUCHER_TO_CART = actionTypes('cart/ADD_VOUCHER_TO_CART');
export const UPDATE_LIST_VOUCHER_IN_CART = actionTypes(
  'cart/UPDATE_LIST_VOUCHER_IN_CART'
);

export const ADD_DELIVERY_TO_CART = actionTypes('cart/ADD_DELIVERY_TO_CART');
export const UPDATE_LIST_DELIVERY_IN_CART = actionTypes(
  'cart/UPDATE_LIST_DELIVERY_IN_CART'
);


export const EMPTY_LIST_DELIVERY_IN_CART = actionTypes(
  'cart/EMPTY_LIST_DELIVERY_IN_CART'
);
