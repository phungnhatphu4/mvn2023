/* eslint-disable import/prefer-default-export */
import { actionTypes } from '../actionTypes';

export const GET_HISTORY_TRANSACTION_POINT_VIP = actionTypes(
  'history/GET_HISTORY_TRANSACTION_POINT_VIP'
);
