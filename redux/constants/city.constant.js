import { actionTypes } from '../actionTypes';

// eslint-disable-next-line import/prefer-default-export
export const GET_LIST_CITIES = actionTypes('city/GET_LIST_CITIES');