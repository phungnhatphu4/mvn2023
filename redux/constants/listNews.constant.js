/* eslint-disable import/prefer-default-export */
import { actionTypes } from '../actionTypes';

export const GET_LIST_NEWS = actionTypes(
    'news/GET_LIST_NEWS'
);

export const GET_LIST_NEWS_CUSTOMER = actionTypes(
    'news/GET_LIST_NEWS_CUSTOMER'
);
