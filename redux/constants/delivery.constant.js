import { actionTypes } from '../actionTypes';

export const GET_LIST_FOODS_DELIVERY = actionTypes(
  'delivery/GET_LIST_FOODS_DELIVERY'
);

export const CREATE_DELIVERY_FOOD = actionTypes(
  'delivery/CREATE_DELIVERY_FOOD'
);

export const GET_SHIPPING_FEE = actionTypes('delivery/GET_SHIPPING_FEE');

export const GET_LIST_DELIVERY_HISTORY = actionTypes(
  'delivery/GET_LIST_DELIVERY_HISTORY'
);

export const GET_LIST_GROUP_TYPE_DELIVERY = actionTypes(
  'delivery/GET_LIST_GROUP_TYPE_DELIVERY'
);

export const GET_ALL_LIST_PRODUCT = actionTypes(
  'delivery/GET_ALL_LIST_PRODUCT'
);