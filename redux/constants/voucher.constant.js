import { actionTypes } from '../actionTypes';

export const GET_VOUCHER_BY_ID = actionTypes('voucher/GET_VOUCHER_BY_ID');

export const GET_LIST_HOT_VOUCHER_HOME = actionTypes(
  'voucher/GET_LIST_HOT_VOUCHER_HOME'
);

export const GET_LIST_HOT_VOUCHER = actionTypes('voucher/GET_LIST_HOT_VOUCHER');

export const GET_LIST_FEATURE_VOUCHER_HOME = actionTypes(
  'voucher/GET_LIST_FEATURE_VOUCHER_HOME'
);
export const GET_LIST_FEATURE_VOUCHER = actionTypes(
  'voucher/GET_LIST_FEATURE_VOUCHER'
);

export const GET_LIST_FEATURE_VOUCHER_HOME_LUCBUU = actionTypes(
  'voucher/GET_LIST_FEATURE_VOUCHER_HOME_LUCBUU'
);

export const GET_LIST_FEATURE_VOUCHER_LUCBUU = actionTypes(
  'voucher/GET_LIST_FEATURE_VOUCHER_LUCBUU'
);

export const CHECK_PROMOTION_CODE = actionTypes('voucher/CHECK_PROMOTION_CODE');
export const BUY_VOUCHER = actionTypes('voucher/BUY_VOUCHER');

export const GET_LIST_VOUCHER_BY_TYPE = actionTypes(
  'voucher/GET_LIST_VOUCHER_BY_TYPE'
);

export const GET_LIST_VOUCHER_BY_MERCHANT = actionTypes(
  'voucher/GET_LIST_VOUCHER_BY_MERCHANT'
);

export const GET_LIST_VOUCHER_HISTORY = actionTypes(
  'voucher/GET_LIST_VOUCHER_HISTORY'
);

export const GET_LIST_VOUCHER_TYPE = actionTypes(
  'voucher/GET_LIST_VOUCHER_TYPE'
);

export const GET_LIST_CODE_REQUIRED_VOUCHER_HISTORY = actionTypes(
  'voucher/GET_LIST_CODE_REQUIRED_VOUCHER_HISTORY'
);

export const DELETE_VOUCHER_HISTORY = actionTypes(
  'voucher/DELETE_VOUCHER_HISTORY'
);

export const GET_VOUCHERS_BY_SEARCH = actionTypes(
  'voucher/GET_VOUCHERS_BY_SEARCH'
);

export const USE_VOUCHER_HISTORY = actionTypes('voucher/USE_VOUCHER_HISTORY');

export const SET_KEYWORD_FOR_SEARCH = actionTypes(
  'voucher/SET_KEYWORD_FOR_SEARCH'
);
