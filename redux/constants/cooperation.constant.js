/* eslint-disable import/prefer-default-export */
import { actionTypes } from '../actionTypes';

export const POST_COOPERATION_INFO = actionTypes(
  'cooperationRecruitment/POST_COOPERATION_INFO'
);
