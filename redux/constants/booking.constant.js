/* eslint-disable import/prefer-default-export */
import { actionTypes } from '../actionTypes';

export const REQUEST_BOOKING = actionTypes('booking/REQUEST_BOOKING');
