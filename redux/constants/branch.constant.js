import { actionTypes } from '../actionTypes';

export const GET_LIST_BRANCH_HOME_BY_MERCHANT_ID = actionTypes(
  'branch/GET_LIST_BRANCH_HOME_BY_MERCHANT_ID'
);

export const GET_LIST_BRANCH_BY_ID = actionTypes('order/GET_LIST_BRANCH_BY_ID');

export const GET_LIST_BRANCH_HOME = actionTypes('branch/GET_LIST_BRANCH_HOME')
