import { actionTypes } from '../actionTypes';

export const GET_LIST_RECRUITMENT = actionTypes(
  'recruitment/GET_LIST_RECRUITMENT'
);

export const GET_LIST_RECRUITMENT_HOME = actionTypes(
  'recruitment/GET_LIST_RECRUITMENT_HOME'
);

export const GET_RECRUITMENT_BY_ID = actionTypes(
  'news/GET_RECRUITMENT_BY_ID'
);
