import { actionTypes } from '../actionTypes';

export const GET_LIST_RECRUITMENT = actionTypes(
  'fillRecruitment/GET_LIST_JOBS_RECRUITMENT'
);

export const POST_RECRUITMENT_INFO = actionTypes(
  'fillRecruitment/POST_RECRUITMENT_INFO'
);
