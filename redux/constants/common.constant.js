import { actionTypes } from '../actionTypes';

export const GET_LIST_BANNER = actionTypes('common/GET_LIST_BANNER');
export const GET_BANNER_CATEGORY = actionTypes('common/GET_BANNER_CATEGORY');

export const GET_BANK_INFO = actionTypes('common/GET_BANK_INFO');
export const GET_LIST_NOTIFICATIONS = actionTypes(
  'common/GET_LIST_NOTIFICATIONS'
);
export const SET_CURRENT_LOCATION = 'common/SET_CURRENT_LOCATION';
export const GET_READ_NOTIFICATION = actionTypes(
  'common/GET_READ_NOTIFICATION'
);
export const MAKE_ALL_READ_NOTIFICATIONS = actionTypes(
  'common/MAKE_ALL_READ_NOTIFICATIONS'
);

export const GET_LIST_FAQ = actionTypes('common/GET_LIST_FAQ');
