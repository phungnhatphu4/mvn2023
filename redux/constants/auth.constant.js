import { actionTypes } from '../actionTypes';

export const LOGIN = actionTypes('auth/LOGIN');
export const VERIFY_OTP = actionTypes('auth/VERIFY_OTP');
export const LOGOUT = actionTypes('auth/LOGOUT');
export const CREATE_PASSWORD = actionTypes('auth/CREATE_PASSWORD');

export const REQUEST_OTP_TO_REGISTER = actionTypes(
  'auth/REQUEST_OTP_TO_REGISTER'
);
export const GET_USER_INFO = actionTypes('auth/GET_USER_INFO');
export const UPDATE_FIRST_TIME_USER_INFO = actionTypes(
  'auth/UPDATE_FIRST_TIME_USER_INFO'
);

export const UPDATE_USER_INFO = actionTypes('auth/UPDATE_USER_INFO');

export const ADD_TEMP_ACCESS_TOKEN = 'auth/ADD_TEMP_ACCESS_TOKEN';

export const ADD_DEVICE_TOKEN = 'auth/ADD_DEVICE_TOKEN';

export const REQUEST_OTP_TO_RESET_PASSWORD = actionTypes(
  'auth/REQUEST_OTP_TO_RESET_PASSWORD'
);

export const VERIFY_OTP_TO_RESET_PASSWORD = actionTypes(
  'auth/VERIFY_OTP_TO_RESET_PASSWORD'
);

export const RESET_PASSWORD = actionTypes('auth/RESET_PASSWORD');

export const CHANGE_PASSWORD = actionTypes('auth/CHANGE_PASSWORD');

export const CHECK_EXIST_PHONE_NUMBER = actionTypes(
  'auth/CHECK_EXIST_PHONE_NUMBER'
);

export const VERIFY_TOKEN_FIREBASE = actionTypes('auth/VERIFY_TOKEN_FIREBASE');

export const VERIFY_TOKEN_FIREBASE_RESET_PASSWORD = actionTypes(
  'auth/VERIFY_TOKEN_FIREBASE_RESET_PASSWORD'
);

export const CHECK_PHONE_NUMBER = actionTypes('auth/CHECK_PHONE_NUMBER');
