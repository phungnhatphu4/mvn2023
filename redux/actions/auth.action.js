import {
  CREATE_PASSWORD,
  GET_USER_INFO,
  LOGIN,
  LOGOUT,
  REQUEST_OTP_TO_REGISTER,
  VERIFY_OTP,
  UPDATE_FIRST_TIME_USER_INFO,
  ADD_TEMP_ACCESS_TOKEN,
  UPDATE_USER_INFO,
  REQUEST_OTP_TO_RESET_PASSWORD,
  RESET_PASSWORD,
  VERIFY_OTP_TO_RESET_PASSWORD,
  CHANGE_PASSWORD,
  CHECK_EXIST_PHONE_NUMBER,
  VERIFY_TOKEN_FIREBASE,
  VERIFY_TOKEN_FIREBASE_RESET_PASSWORD,
  CHECK_PHONE_NUMBER,
  ADD_DEVICE_TOKEN,
} from '../constants/auth.constant'

export const logoutHandle = () => ({
  type: LOGOUT.HANDLER,
})

export const loginHandle = (payload, onSuccess, onError) => ({
  type: LOGIN.HANDLER,
  payload,
  onSuccess,
  onError,
})

export const loginSuccess = (payload) => ({
  type: LOGIN.SUCCESS,
  payload,
})

export const loginFailure = (error) => ({
  type: LOGIN.FAILURE,
  error,
})

export const verifyOtpHandle = (payload, onSuccess, onError) => ({
  type: VERIFY_OTP.HANDLER,
  payload,
  onSuccess,
  onError,
})

export const verifyOtpSuccess = (payload) => ({
  type: VERIFY_OTP.SUCCESS,
  payload,
})

export const verifyOtpFailure = (error) => ({
  type: VERIFY_OTP.FAILURE,
  error,
})

export const createPasswordHandle = (payload, onSuccess) => ({
  type: CREATE_PASSWORD.HANDLER,
  payload,
  onSuccess,
})

export const createPasswordSuccess = (payload) => ({
  type: CREATE_PASSWORD.SUCCESS,
  payload,
})

export const createPasswordFailure = (error) => ({
  type: CREATE_PASSWORD.FAILURE,
  error,
})

export const requestOtpToRegisterHandle = (phone, onSuccess) => ({
  type: REQUEST_OTP_TO_REGISTER.HANDLER,
  phone,
  onSuccess,
})

export const requestOtpToRegisterSuccess = (payload) => ({
  type: REQUEST_OTP_TO_REGISTER.SUCCESS,
  payload,
})

export const requestOtpToRegisterFailure = (error) => ({
  type: REQUEST_OTP_TO_REGISTER.FAILURE,
  error,
})

export const getUserInfoHandle = () => ({
  type: GET_USER_INFO.HANDLER,
})

export const getUserInfoSuccess = (userInfo) => ({
  type: GET_USER_INFO.SUCCESS,
  userInfo,
})

export const getUserInfoFailure = (error) => ({
  type: GET_USER_INFO.FAILURE,
  error,
})

export const updateFirstTimeUserInfoHandle = (userInfo, onSuccess) => ({
  type: UPDATE_FIRST_TIME_USER_INFO.HANDLER,
  userInfo,
  onSuccess,
})

export const updateFirstTimeUserInfoSuccess = (userInfo) => ({
  type: UPDATE_FIRST_TIME_USER_INFO.SUCCESS,
  userInfo,
})

export const updateFirstTimeUserInfoFailure = (error) => ({
  type: UPDATE_FIRST_TIME_USER_INFO.FAILURE,
  error,
})

export const addTempAccessToken = (payload) => ({
  type: ADD_TEMP_ACCESS_TOKEN,
  payload,
})

export const addDeviceToken = (deviceToken) => ({
  type: ADD_DEVICE_TOKEN,
  deviceToken,
})

export const updateUserInfoHandle = (userInfo) => ({
  type: UPDATE_USER_INFO.HANDLER,
  userInfo,
})

export const updateUserInfoSuccess = (userInfo) => ({
  type: UPDATE_USER_INFO.SUCCESS,
  userInfo,
})

export const updateUserInfoFailure = (error) => ({
  type: UPDATE_USER_INFO.FAILURE,
  error,
})

export const requestOtpToResetPasswordHandle = (phone, onSuccess) => ({
  type: REQUEST_OTP_TO_RESET_PASSWORD.HANDLER,
  phone,
  onSuccess,
})

export const requestOtpToResetPasswordSuccess = (payload) => ({
  type: REQUEST_OTP_TO_RESET_PASSWORD.SUCCESS,
  payload,
})

export const requestOtpToResetPasswordFailure = (error) => ({
  type: REQUEST_OTP_TO_RESET_PASSWORD.FAILURE,
  error,
})

export const verifyOtpToResetPasswordHandle = (
  payload,
  onSuccess,
  onError,
) => ({
  type: VERIFY_OTP_TO_RESET_PASSWORD.HANDLER,
  payload,
  onSuccess,
  onError,
})

export const verifyOtpResetPasswordSuccess = (payload) => ({
  type: VERIFY_OTP_TO_RESET_PASSWORD.SUCCESS,
  payload,
})

export const verifyOtpResetPasswordFailure = (error) => ({
  type: VERIFY_OTP_TO_RESET_PASSWORD.FAILURE,
  error,
})

export const resetPasswordHandle = (payload, onSuccess) => ({
  type: RESET_PASSWORD.HANDLER,
  payload,
  onSuccess,
})

export const resetPasswordSuccess = (payload) => ({
  type: RESET_PASSWORD.SUCCESS,
  payload,
})

export const resetPasswordFailure = (error) => ({
  type: RESET_PASSWORD.FAILURE,
  error,
})

export const changePasswordHandle = (payload, onSuccess, onError) => ({
  type: CHANGE_PASSWORD.HANDLER,
  payload,
  onSuccess,
  onError,
})

export const changePasswordSuccess = (payload) => ({
  type: CHANGE_PASSWORD.SUCCESS,
  payload,
})

export const changePasswordFailure = (error) => ({
  type: CHANGE_PASSWORD.FAILURE,
  error,
})

export const checkExistPhoneNumberHandle = (phone, onSuccess, onError) => ({
  type: CHECK_EXIST_PHONE_NUMBER.HANDLER,
  phone,
  onSuccess,
  onError,
})

export const checkExistPhoneNumberSuccess = (payload) => ({
  type: CHECK_EXIST_PHONE_NUMBER.SUCCESS,
  payload,
})

export const checkExistPhoneNumberFailure = (error) => ({
  type: CHECK_EXIST_PHONE_NUMBER.FAILURE,
  error,
})

export const verifyTokenFirebaseHandle = (
  phone,
  name,
  token,
  password,
  onSuccess,
  onError,
) => ({
  type: VERIFY_TOKEN_FIREBASE.HANDLER,
  phone,
  name,
  token,
  password,
  onSuccess,
  onError,
})

export const verifyTokenFirebaseSuccess = (payload) => ({
  type: VERIFY_TOKEN_FIREBASE.SUCCESS,
  payload,
})

export const verifyTokenFirebaseFailure = (error) => ({
  type: VERIFY_TOKEN_FIREBASE.FAILURE,
  error,
})

export const verifyTokenFirebaseResetPasswordHandle = (
  phone,
  token,
  onSuccess,
  onError,
) => ({
  type: VERIFY_TOKEN_FIREBASE_RESET_PASSWORD.HANDLER,
  phone,
  token,
  onSuccess,
  onError,
})

export const verifyTokenFirebaseResetPasswordSuccess = (payload) => ({
  type: VERIFY_TOKEN_FIREBASE_RESET_PASSWORD.SUCCESS,
  payload,
})

export const verifyTokenFirebaseResetPasswordFailure = (error) => ({
  type: VERIFY_TOKEN_FIREBASE_RESET_PASSWORD.FAILURE,
  error,
})

export const checkPhoneNumberHandle = (phone, onSuccess, onError) => ({
  type: CHECK_PHONE_NUMBER.HANDLER,
  phone,
  onSuccess,
  onError,
})

export const checkPhoneNumberSuccess = (payload) => ({
  type: CHECK_PHONE_NUMBER.SUCCESS,
  payload,
})

export const checkPhoneNumberFailure = (error) => ({
  type: CHECK_PHONE_NUMBER.FAILURE,
  error,
})
