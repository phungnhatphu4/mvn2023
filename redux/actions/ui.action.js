export const ADD_MODAL = 'ADD_MODAL';

export const addModal = (modal) => ({
  type: ADD_MODAL,
  payload: modal,
});
