import { createSelector } from '@reduxjs/toolkit'
import { initialState } from 'slices/branch'

const selectSlice = (state) => state.branch || initialState

export const selectListBranch = createSelector(
  [selectSlice],
  (state) => state.list || [],
)

export const selectLoadingBranch = createSelector(
  [selectSlice],
  (state) => state.loading,
)
