/* eslint-disable no-underscore-dangle */
// import { createSelector } from 'reselect';

export const authSelector = (state) => state.auth
export const getAccessTokenSelector = (state) => state.auth.accessToken
export const getUserInfoSelector = (state) => state.auth.userInfo
export const isLoggedSelector = (state) => state.auth.userInfo._id
export const isShowLoginSelector = (state) => !!state.auth.showLogin
export const isShowRegisterSelector = (state) => state.auth.showRegister
export const getDeviceTokenSelector = (state) => state.auth.deviceToken
export const getTempTokenSelector = (state) => state.auth.tempAccessToken
