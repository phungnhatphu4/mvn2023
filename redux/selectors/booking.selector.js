import { createSelector } from '@reduxjs/toolkit'
import { initialState } from 'slices/booking'

const selectSlice = (state) => state.booking || initialState

export const selectBookingData = createSelector(
  [selectSlice],
  (state) => state,
)

export const selectPositionData = createSelector(
  [selectSlice],
  (state) => state.choosePosition,
)

export const selectMenuData = createSelector(
  [selectSlice],
  (state) => state.chooseMenu,
)

export const selectServiceData = createSelector(
  [selectSlice],
  (state) => state.chooseService,
)

export const selectCakeService = createSelector(
  [selectSlice],
  (state) => state.chooseService.cakeService,
)

export const selectFlowerService = createSelector(
  [selectSlice],
  (state) => state.chooseService.flowerService,
)

export const selectFreePictureService = createSelector(
  [selectSlice],
  (state) => state.chooseService.freePictureService,
)

export const selectHangingPictureService = createSelector(
  [selectSlice],
  (state) => state.chooseService.hangingPictureService,
)

export const selectHangingLetterService = createSelector(
  [selectSlice],
  (state) => state.chooseService.hangingLetterService,
)

export const selectSpearRoseService = createSelector(
  [selectSlice],
  (state) => state.chooseService.spreadRoseService,
)

export const selectDecorationService = createSelector(
  [selectSlice],
  (state) => state.chooseService.decorationService,
)

export const selectSelectedCake = createSelector(
  [selectSlice],
  (state) => state.chooseService.cakeService.selectedCake,
)

export const selectStep1Data = createSelector(
  [selectSlice],
  (state) => state.chooseBranch,
)

export const selectChooseMenu = createSelector(
  [selectSlice],
  (state) => state.chooseMenu,
)

export const selectChoosePayment = createSelector(
  [selectSlice],
  (state) => state.choosePayment,
)
