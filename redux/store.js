import { configureStore } from '@reduxjs/toolkit'
import { createWrapper } from 'next-redux-wrapper'
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'

import rootSaga from './sagas/index'
import rootReducer from './slices'

const makeStore = () => {
  const logger = createLogger()
  const sagaMiddleware = createSagaMiddleware()

  const store = configureStore({
    reducer: rootReducer,
    devTools: true,
    middleware: [sagaMiddleware, logger],
  })

  store.sagaTask = sagaMiddleware.run(rootSaga)
  return store
}

export const wrapper = createWrapper(makeStore)
