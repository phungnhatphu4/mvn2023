import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'

export const initialState = {
  chooseBranch: {},
  chooseMenu: {},
  chooseService: {
    cakeService: {
      selectedCake: null,
      message: '',
      candle: null,
    },
    flowerService: {
      selectedFlower: null,
      message: '',
      smallCardMessage: '',
    },
    freePictureService: {
      images: [],
      price: 0,
    },
    hangingPictureService: {
      images: [],
    },
    hangingLetterService: null,
    spreadRoseService: null,
    decorationService: null,
  },
  choosePosition: {},
  choosePayment: {},
}

export const bookingSlice = createSlice({
  name: 'booking',
  initialState,
  reducers: {
    setSelectedCake(state, action) {
      state.chooseService.cakeService.selectedCake = action.payload
    },
    setCakeMessage(state, action) {
      state.chooseService.cakeService.message = action.payload
    },
    setCakeCandle(state, action) {
      state.chooseService.cakeService.candle = action.payload
    },
    setSelectedFlower(state, action) {
      state.chooseService.flowerService.selectedFlower = action.payload
    },
    setFlowerMessage(state, action) {
      state.chooseService.flowerService.message = action.payload
    },
    setFlowerSmallCardMessage(state, action) {
      state.chooseService.flowerService.smallCardMessage = action.payload
    },
    setFreeImages(state, action) {
      state.chooseService.freePictureService = action.payload
    },
    setHangingPicture(state, action) {
      state.chooseService.hangingPictureService = action.payload
    },
    setHangingPicture(state, action) {
      state.chooseService.hangingPictureService = action.payload
    },
    setHangingLetter(state, action) {
      state.chooseService.hangingLetterService = action.payload
    },
    setSpreadRose(state, action) {
      state.chooseService.spreadRoseService = action.payload
    },
    setDecoration(state, action) {
      state.chooseService.decorationService = action.payload
    },
    setStep1Data(state, action) {
      state.chooseBranch = action.payload
    },
    setChoosePosition(state, action) {
      state.choosePosition = action.payload
    },
    setChooseMenu(state, action) {
      state.chooseMenu = action.payload
    },
    setChoosePayment(state, action) {
      state.choosePayment = action.payload
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload,
      }
    },
  },
})

export const { actions: bookingActions } = bookingSlice
export default bookingSlice.reducer
