import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

export const initialState = {
  list: [],
  loading: false
}

export const dummySlice = createSlice({
  name: 'dummy',
  initialState,
  reducers: {
    getDummies(state, action) {
      state.loading = true
    },
    getDummiesSuccess(state, action) {
      state.list = action.payload.data
      state.loading = false
    },
    removeAllDummy(state, action) {
      state.list = []
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload,
      }
    },
  },
})

export const { actions: dummyActions } = dummySlice
export default dummySlice.reducer