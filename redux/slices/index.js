// import branchReducer from './branch'
import dummyReducer from './dummy'
import authReducer from './auth.reducer'
import bookingReducer from './booking'

const rootReducer = {
  dummy: dummyReducer,
  // branch: branchReducer,
  auth: authReducer,
  booking: bookingReducer,
}
export default rootReducer
