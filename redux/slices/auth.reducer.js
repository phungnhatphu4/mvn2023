import {
  GET_USER_INFO,
  LOGIN,
  LOGOUT,
  REQUEST_OTP_TO_REGISTER,
  VERIFY_OTP,
  ADD_TEMP_ACCESS_TOKEN,
  UPDATE_FIRST_TIME_USER_INFO,
  UPDATE_USER_INFO,
  REQUEST_OTP_TO_RESET_PASSWORD,
  VERIFY_OTP_TO_RESET_PASSWORD,
  RESET_PASSWORD,
  VERIFY_TOKEN_FIREBASE,
  VERIFY_TOKEN_FIREBASE_RESET_PASSWORD,
  ADD_DEVICE_TOKEN,
} from '../constants/auth.constant'

const initialState = {
  deviceToken: '',
  tempAccessToken: '',
  accessToken: '',
  userInfo: {},
}

export default function authReducer(state = initialState, action) {
  const { type, payload } = action
  switch (type) {
    case LOGIN.SUCCESS:
      return {
        ...state,
        accessToken: payload.token,
        deviceToken: action.payload.deviceToken,
      }
    case ADD_TEMP_ACCESS_TOKEN: {
      return {
        ...state,
        tempAccessToken: payload.token,
        userInfo: payload.userInfo,
      }
    }
    case ADD_DEVICE_TOKEN: {
      return { ...state, deviceToken: action.deviceToken }
    }
    case RESET_PASSWORD.SUCCESS:
    case UPDATE_FIRST_TIME_USER_INFO.SUCCESS: {
      return {
        ...state,
        tempAccessToken: '',
        accessToken: state.tempAccessToken,
        userInfo: action.userInfo,
      }
    }

    case UPDATE_USER_INFO.SUCCESS: {
      return {
        ...state,
        userInfo: { ...state.userInfo, ...action.userInfo },
      }
    }
    case GET_USER_INFO.SUCCESS:
      return {
        ...state,
        userInfo: action.userInfo || {},
      }
    case LOGOUT.HANDLER: {
      return {
        ...initialState,
      }
    }
    case REQUEST_OTP_TO_RESET_PASSWORD.SUCCESS:
    case REQUEST_OTP_TO_REGISTER.SUCCESS: {
      return {
        ...state,
        deviceToken: action.payload.deviceToken,
      }
    }
    case VERIFY_OTP_TO_RESET_PASSWORD.SUCCESS:
    case VERIFY_TOKEN_FIREBASE.SUCCESS:
    case VERIFY_TOKEN_FIREBASE_RESET_PASSWORD.SUCCESS:
    case VERIFY_OTP.SUCCESS: {
      return { ...state, tempAccessToken: payload.token }
    }
    default:
      return state
  }
}
