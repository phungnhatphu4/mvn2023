import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'

export const initialState = {
  list: [],
  loading: false,
}

export const branchSlice = createSlice({
  name: 'branch',
  initialState,
  reducers: {
    getListBranch(state, action) {
      state.loading = true
    },
    getListBranchSuccess(state, action) {
      state.list = action.payload.data
      state.loading = false
    },
    getListBranchFail(state, action) {
      state.list = []
      state.loading = false
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload,
      }
    },
  },
})

export const { actions: branchActions } = branchSlice
export default branchSlice.reducer
