/* eslint-disable camelcase */
import { takeLatest, call, put, select } from 'redux-saga/effects'
import AuthService, { getUserInfoApi } from '../../services/auth.service'
import APIUtils from '../../utils/apiUtils'
import { handleQuery } from '../../helpers/handleQuery'

import {
  addDeviceToken,
  addTempAccessToken,
  changePasswordFailure,
  changePasswordSuccess,
  checkPhoneNumberFailure,
  checkPhoneNumberSuccess,
  createPasswordFailure,
  createPasswordSuccess,
  getUserInfoFailure,
  getUserInfoHandle,
  getUserInfoSuccess,
  loginFailure,
  loginSuccess,
  requestOtpToRegisterFailure,
  requestOtpToRegisterSuccess,
  requestOtpToResetPasswordFailure,
  requestOtpToResetPasswordSuccess,
  resetPasswordFailure,
  resetPasswordSuccess,
  updateFirstTimeUserInfoFailure,
  updateFirstTimeUserInfoSuccess,
  updateUserInfoFailure,
  updateUserInfoSuccess,
  verifyOtpFailure,
  verifyOtpResetPasswordFailure,
  verifyOtpResetPasswordSuccess,
  verifyOtpSuccess,
  verifyTokenFirebaseFailure,
  verifyTokenFirebaseResetPasswordFailure,
  verifyTokenFirebaseResetPasswordSuccess,
  verifyTokenFirebaseSuccess,
} from '../actions/auth.action'
import {
  CHANGE_PASSWORD,
  CHECK_EXIST_PHONE_NUMBER,
  CHECK_PHONE_NUMBER,
  CREATE_PASSWORD,
  GET_USER_INFO,
  LOGIN,
  REQUEST_OTP_TO_REGISTER,
  REQUEST_OTP_TO_RESET_PASSWORD,
  RESET_PASSWORD,
  UPDATE_FIRST_TIME_USER_INFO,
  UPDATE_USER_INFO,
  VERIFY_OTP,
  VERIFY_OTP_TO_RESET_PASSWORD,
  VERIFY_TOKEN_FIREBASE,
  VERIFY_TOKEN_FIREBASE_RESET_PASSWORD,
} from '../constants/auth.constant'
import { getDeviceTokenSelector, getTempTokenSelector } from '../selectors/auth'

// const login

export function* getDeviceTokenSaga() {
  // const fingerprint = yield call(getFingerprintPromise);
  // const signature = getSignature(fingerprint, TRACKING_SOURCE);
  const deviceTokenResponse = yield call(AuthService.getDeviceTokenApi, {
    fingerprint: '8c988c0862a7cb9e7cf8e778b99e939755',
    signature:
      'b6ba4e2e99b5e97550558c2c7eea495a56e87e1a40d1b7899c00b3c0bfa25ee1',
    source: 'be06e7c1a6656598baff9b01cdd2d76c2ef55ac9c6201b7b0f542bb1c20bd093',
  })
  yield put(addDeviceToken(deviceTokenResponse?.data?.device_tokens))
  return deviceTokenResponse?.data?.device_tokens
}

export function* loginSaga({ payload, onSuccess, onError }) {
  const { phone, password } = payload || {}
  try {
    let deviceToken = yield select(getDeviceTokenSelector)
    if (!deviceToken) {
      deviceToken = yield getDeviceTokenSaga()
    }
    const res = yield call(
      AuthService.loginApi,
      {
        phone,
        password,
      },
      { Authorization: `JWT ${deviceToken}` },
    )
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    if (!res.data.profile_need_update) {
      yield put(loginSuccess({ token: res.data.token, deviceToken }))
      localStorage.setItem('ACCESS_TOKEN', res.data.token)
      APIUtils.setAccessToken(res.data.token)
      yield put(getUserInfoHandle())
    } else {
      APIUtils.setAccessToken(res.data.token)
      yield put(
        addTempAccessToken({
          token: res.data.token,
          userInfo: { name: res.data.name, phone: res.data.phone_number },
        }),
      )
    }
  } catch (err) {
    yield put(loginFailure(err))
    if (typeof onError === 'function') {
      onError(err)
    }
  }
}

export function* requestOtpToRegisterSaga({ phone, onSuccess }) {
  try {
    let deviceToken = yield select(getDeviceTokenSelector)
    if (!deviceToken) {
      deviceToken = yield getDeviceTokenSaga()
    }
    const res = yield call(
      AuthService.requestOtpToRegisterApi,
      {
        phone,
      },
      { Authorization: `JWT ${deviceToken}` },
    )
    if (typeof onSuccess === 'function') {
      onSuccess(res)
    }
    yield put(requestOtpToRegisterSuccess({ deviceToken }))
  } catch (err) {
    yield put(requestOtpToRegisterFailure(err))
  }
}

export function* requestOtpToResetPasswordSaga({ phone, onSuccess }) {
  try {
    console.log('111111')
    let deviceToken = yield select(getDeviceTokenSelector)
    if (!deviceToken) {
      deviceToken = yield getDeviceTokenSaga()
    }
    const res = yield call(
      AuthService.requestOtpToResetPasswordApi,
      {
        phone,
      },
      { Authorization: `JWT ${deviceToken}` },
    )
    if (typeof onSuccess === 'function') {
      onSuccess(res)
    }
    yield put(requestOtpToResetPasswordSuccess({ deviceToken }))
  } catch (err) {
    yield put(requestOtpToResetPasswordFailure(err))
  }
}

export function* getUserInfoSaga() {
  try {
    const res = yield handleQuery(getUserInfoApi())
    yield put(getUserInfoSuccess(res.get_user))
  } catch (error) {
    yield put(getUserInfoFailure(error))
  }
}

export function* verifyOtpSaga(action) {
  const { payload, onSuccess, onError } = action
  try {
    const deviceToken = yield select(getDeviceTokenSelector)
    const res = yield AuthService.verifyOtpApi(
      {
        otp_value: payload.otpCode,
        otp_token: payload.otpToken,
      },
      { Authorization: `JWT ${deviceToken}` },
    )
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    yield put(verifyOtpSuccess(res.data))
  } catch (error) {
    if (typeof onError === 'function') {
      onError(error)
    }
    yield put(verifyOtpFailure(error.message))
  }
}

export function* createPasswordSaga(action) {
  const { payload, onSuccess } = action
  try {
    const tempAccessToken = yield select(getTempTokenSelector)

    const res = yield AuthService.createPasswordApi(payload, {
      Authorization: `JWT ${tempAccessToken}`,
    })
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    yield put(createPasswordSuccess(res))
  } catch (error) {
    yield put(createPasswordFailure(error.message))
  }
}

export function* verifyOtpToResetPasswordSaga(action) {
  const { payload, onSuccess, onError } = action
  try {
    const deviceToken = yield select(getDeviceTokenSelector)
    const res = yield AuthService.verifyOtpToResetPasswordApi(
      {
        otp_value: payload.otpCode,
        otp_token: payload.otpToken,
      },
      { Authorization: `JWT ${deviceToken}` },
    )
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    yield put(verifyOtpResetPasswordSuccess({ token: res.data.signinToken }))
  } catch (error) {
    if (typeof onError === 'function') {
      onError(error)
    }
    yield put(verifyOtpResetPasswordFailure(error.message))
  }
}

export function* resetPasswordSaga(action) {
  const { payload, onSuccess } = action
  try {
    const tempAccessToken = yield select(getTempTokenSelector)

    const res = yield AuthService.resetPasswordApi(payload, {
      Authorization: `JWT ${tempAccessToken}`,
    })
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    localStorage.setItem('ACCESS_TOKEN', tempAccessToken)
    APIUtils.setAccessToken(tempAccessToken)
    yield put(resetPasswordSuccess({}))
  } catch (error) {
    yield put(resetPasswordFailure(error.message))
  }
}

export function* changePasswordSaga(action) {
  const { payload, onSuccess, onError } = action
  try {
    const res = yield AuthService.changePasswordApi(payload)
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    yield put(changePasswordSuccess({}))
  } catch (error) {
    if (typeof onError === 'function') {
      onError(error)
    }
    yield put(changePasswordFailure(error.message))
  }
}

export function* checkExistPhoneNumberSaga(action) {
  const { phone, onSuccess, onError } = action
  try {
    const res = yield AuthService.checkExistPhoneNumberApi(phone)
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    yield put(changePasswordSuccess({}))
  } catch (error) {
    if (typeof onError === 'function') {
      onError(error)
    }
    yield put(changePasswordFailure(error.message))
  }
}

export function* verifyFirebaseTokenSaga(action) {
  const { token, phone, password, onSuccess, onError, name } = action
  try {
    let deviceToken = yield select(getDeviceTokenSelector)
    if (!deviceToken) {
      deviceToken = yield getDeviceTokenSaga()
    }
    const res = yield AuthService.verifyTokenFirebaseApi(
      { token, phone, password, name },
      { Authorization: `JWT ${deviceToken}` },
    )
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    yield put(verifyTokenFirebaseSuccess(res.data))
  } catch (error) {
    if (typeof onError === 'function') {
      onError(error)
    }
    yield put(verifyTokenFirebaseFailure(error.message))
  }
}

export function* verifyFirebaseTokenResetPasswordSaga(action) {
  const { token, phone, onSuccess, onError } = action
  try {
    let deviceToken = yield select(getDeviceTokenSelector)
    if (!deviceToken) {
      deviceToken = yield getDeviceTokenSaga()
    }
    const res = yield AuthService.verifyTokenFirebaseResetPasswordApi(
      { token, phone },
      { Authorization: `JWT ${deviceToken}` },
    )
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    yield put(
      verifyTokenFirebaseResetPasswordSuccess({ token: res.data.signinToken }),
    )
  } catch (error) {
    if (typeof onError === 'function') {
      onError(error)
    }
    yield put(verifyTokenFirebaseResetPasswordFailure(error.message))
  }
}

export function* updateFirstTimeUserInfoSaga(action) {
  const { userInfo, onSuccess } = action
  try {
    const tempAccessToken = yield select(getTempTokenSelector)
    let image_url = userInfo.avatar

    const res = yield AuthService.updateUserInfoApi(
      { ...userInfo, image_url },
      {
        Authorization: `JWT ${tempAccessToken}`,
      },
    )
    localStorage.setItem('ACCESS_TOKEN', tempAccessToken)
    APIUtils.setAccessToken(tempAccessToken)
    if (typeof onSuccess === 'function') {
      onSuccess(res.data)
    }
    yield put(updateFirstTimeUserInfoSuccess(userInfo))
  } catch (error) {
    yield put(updateFirstTimeUserInfoFailure(error.message))
  }
}

export function* updateUserInfoSaga(action) {
  const { userInfo, onSuccess } = action
  try {
    let image_url = userInfo.avatar
    yield AuthService.updateUserInfoApi({ ...userInfo, image_url })
    if (typeof onSuccess === 'function') {
      onSuccess({ ...userInfo, image_url })
    }
    yield put(updateUserInfoSuccess({ ...userInfo, image_url }))
  } catch (error) {
    yield put(updateUserInfoFailure(error.message))
  }
}

export function* checkPhoneNumberSaga(action) {
  const { phone, onSuccess, onError } = action
  try {
    let deviceToken = yield select(getDeviceTokenSelector)
    if (!deviceToken) {
      deviceToken = yield getDeviceTokenSaga()
    }

    const res = yield AuthService.checkPhoneNumberApi(phone, {
      Authorization: `JWT ${deviceToken}`,
    })
    if (typeof onSuccess === 'function') {
      onSuccess(res)
    }
    yield put(checkPhoneNumberSuccess(res))
  } catch (error) {
    yield put(checkPhoneNumberFailure(error.message))
  }
}

export default function* authSaga() {
  yield takeLatest(LOGIN.HANDLER, loginSaga)
  yield takeLatest(GET_USER_INFO.HANDLER, getUserInfoSaga)
  yield takeLatest(VERIFY_OTP.HANDLER, verifyOtpSaga)
  yield takeLatest(CREATE_PASSWORD.HANDLER, createPasswordSaga)
  yield takeLatest(REQUEST_OTP_TO_REGISTER.HANDLER, requestOtpToRegisterSaga)
  yield takeLatest(
    VERIFY_OTP_TO_RESET_PASSWORD.HANDLER,
    verifyOtpToResetPasswordSaga,
  )
  yield takeLatest(RESET_PASSWORD.HANDLER, resetPasswordSaga)
  yield takeLatest(CHANGE_PASSWORD.HANDLER, changePasswordSaga)
  yield takeLatest(
    REQUEST_OTP_TO_RESET_PASSWORD.HANDLER,
    requestOtpToResetPasswordSaga,
  )
  yield takeLatest(
    UPDATE_FIRST_TIME_USER_INFO.HANDLER,
    updateFirstTimeUserInfoSaga,
  )
  yield takeLatest(UPDATE_USER_INFO.HANDLER, updateUserInfoSaga)
  yield takeLatest(CHECK_EXIST_PHONE_NUMBER.HANDLER, checkExistPhoneNumberSaga)
  yield takeLatest(VERIFY_TOKEN_FIREBASE.HANDLER, verifyFirebaseTokenSaga)
  yield takeLatest(
    VERIFY_TOKEN_FIREBASE_RESET_PASSWORD.HANDLER,
    verifyFirebaseTokenResetPasswordSaga,
  )
  yield takeLatest(CHECK_PHONE_NUMBER.HANDLER, checkPhoneNumberSaga)
}
