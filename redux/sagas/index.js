import { fork } from 'redux-saga/effects'
import { dummySaga } from './dummy'
import authSaga from './authSaga'

export default function* rootSaga() {
  yield fork(dummySaga)
  yield fork(authSaga)
}
