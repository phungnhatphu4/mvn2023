/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './layouts/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Bitter', ...defaultTheme.fontFamily.sans],
      },
      colors: {},
      backgroundImage: {
        'primary-linear':
          'linear-gradient(90deg, #8E7A49 0%, #FCCF64 49.48%, #8E7A49 100%)',
      },
    },
  },
  corePlugins: {
    container: false,
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
    function ({ addComponents }) {
      addComponents({
        '.container': {
          maxWidth: '100%',
          margin: 'auto',
          '@screen lg': {
            maxWidth: '1366px',
          },
          '@screen xl': {
            paddingLeft: '150px',
            paddingRight: '150px',
          },
        },
      })
    },
  ],
}
