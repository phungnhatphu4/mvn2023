import { useState } from "react"
import client from "apollo-client";

import SwiperPerfectSpace from "components/swiper/SwiperPerfectSpace";
import Image from "next/image";
import BranchInfo from 'assets/images/branch/mobile-introduce.jpg'
import { Breadcrumb } from "components/Breadcrumb/index";

import { getNewsData } from 'services/service.listnews'
import SelectNews from "components/SelectNews/SelectNews";

const listBreadcrumb = [
  {
    link: '/',
    name: 'Trang chủ'
  },
  {
    name: 'Tin tức & Sự kiện'
  }
]

const News = ({ list_news }) => {
	
  const [selectedBranch, setSelectedBranch] = useState(list_news?.[0]);
	console.log(list_news);
 
  return (
    <>
      <Breadcrumb listBreadcrumb={listBreadcrumb} />
      <div className="container">
        <div className="text-center leading-[20px] text-[#F0C753] py-[16px] sm:py-[32px] font-[700]">Tin tức & Sự kiện</div>
        <div className="mb-6 lg:mt-[12px]">
          <div className="mb-4 lg:mb-6">
            <SelectNews
              ListNews={list_news}
              onClickBranch={setSelectedBranch}
              selectedBranch={selectedBranch}
              zoomSelected

            />
          </div>
         
        </div>
       
      </div>

      
    </>
  )
}

export async function getServerSideProps() {
  const { data } = await getNewsData(2);

  return {
    props: {
      list_news: data?.list_news?.data,
    },
  };
}

export default News;