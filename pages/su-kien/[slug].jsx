import { useState } from "react"
import client from "apollo-client";
import { GET_LIST_BRANCHES } from "graphql/branch";

import SelectBranch from "components/SelectBranch/SelectBranch";
import SwiperPerfectSpace from "components/swiper/SwiperPerfectSpace";
import Image from "next/image";
import BranchInfo from 'assets/images/branch/mobile-introduce.jpg'

import { Breadcrumb } from "components/Breadcrumb/index";
import { useRouter } from "next/router"
import { getNewsDataById } from 'services/service.newsbyid'

var id;
const listBreadcrumb = [
  {
    link: '/',
    name: 'Trang chủ'
  },
  {
    name: 'Chi nhánh'
  }
]

const Service = ({ service,id }) => {
  const [selectedBranch, setSelectedBranch] = useState(service?.[0]);
  //const router = useRouter()
  //const { query: { id },} = router

  console.log("//////////////////");
  console.log(id);
  return (
    <>
      <Breadcrumb listBreadcrumb={listBreadcrumb} />
      <div className="container">
        <div className="text-center leading-[20px] text-[#F0C753] py-[16px] sm:py-[32px] font-[700]">{service.name}</div>
        
        <div className="pb-[50px] lg:pt-[30px]">
        <div className="mt-6 text-[13px] text-white lg:px-[5%] px-[15px]">

       
        <p className="lg:mt-[24px] mt-[16px]">
        {service.description}
            </p>
          <div>
           </div>
          </div>
          
        </div>
      </div>
    </>
  )
}

export async function getServerSideProps({ params }) {

  const id = parseInt(params.slug,10);
  
 console.log(id);

 
  const { data } =  await getNewsDataById(id);
  return {
    props: {
      service:data.get_news,
      id:id
    },
  };

}

export default Service;