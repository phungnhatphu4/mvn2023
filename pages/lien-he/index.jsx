import { useState,useEffect, useRef } from "react"
import client from "apollo-client";
import { GET_LIST_BRANCHES } from "graphql/branch";

import SelectBranch from "components/SelectBranch/SelectBranch";
import SwiperPerfectSpace from "components/swiper/SwiperPerfectSpace";
import Image from "next/image";
import BranchInfo from 'assets/images/branch/mobile-introduce.jpg'

import { Breadcrumb } from "components/Breadcrumb/index";
import { useRouter } from "next/router"
import { getServiceDataById } from 'services/service.servicebyid'


import {Loader} from '@googlemaps/js-api-loader';

var id;
const listBreadcrumb = [
  {
    link: '/',
    name: 'Trang chủ'
  },
  {
    name: 'Liên hệ'
  }
]

const Service = ({ service }) => {
  const [selectedBranch, setSelectedBranch] = useState(service?.[0]);
  //const router = useRouter()
  //const { query: { id },} = router

  console.log("//////////////////");
  const googlemap = useRef(null);
  // useEffect(() => {
  //   const loader = new Loader({
  //     apiKey: 'AIzaSyD1hee4vAl_DY77JFpmIYQnUxH8h3YXBRs',
  //     version: 'weekly',
  //   });
  //   let map;
  //   loader.load().then(() => {
  //     map = new google.maps.Map(googlemap.current, {
  //       center: {lat: -34.397, lng: 150.644},
  //       zoom: 8,
  //     });
  //   });
  // });


  return (
    <>
      <Breadcrumb listBreadcrumb={listBreadcrumb} />
      <div className="container marfont">
        <div className="text-center leading-[20px] text-[#F0C753] py-[16px] sm:py-[32px] font-[700]">Liên hệ</div>
        
        <div className="flex flex-row">
            <div className="w-1/2">


            <div>
        
        <div className="mt-6 text-[13px] text-white lg:px-[5%] px-[15px]">
          
        <div className="text-left text-[#F0C753] py-[8px] sm:py-[8px] font-[700] text-[16px]">Dịa chỉ</div>

          <p className="lg:mt-[10px] mt-[10px] text-[16px] font-[400] marfont">
          Chi nhánh 1: 4/1 Hoàng Dư Khương, Quận 10, HCM

          
          </p>

          <p className="lg:mt-[10px] mt-[10px] text-[16px] font-[400]">
          Chi nhánh 2: 88 Nguyễn Đức Cảnh, P. Tân Phong, Quận 7, TP HCM (địa chỉ bến tàu ra đảo Mars Venus)
          </p>

          <p className="lg:mt-[10px] mt-[10px] text-[16px] font-[400]">
          Chi nhánh 3: 176/9 Lê Văn Sỹ, Q. Phú Nhuận, HCM
          </p>

          <div className="text-left text-[#F0C753] py-[8px] sm:py-[8px] font-[700] text-[16px]">Giờ làm việc</div>

         

                <p className="lg:mt-[6px]">
                5pm – 10pm hàng ngày
          </p>



          <div className="text-left text-[#F0C753] py-[15px] sm:py-[15px] font-[700] text-[16px]">Đặt bàn ngay</div>

          <p
                  style={{
                    background:
                      'linear-gradient(90deg, #8E7A49 0%, #FCCF64 49.48%, #8E7A49 100%)',
                  }}
                  className="px-[16px] py-[8px] w-[160px] rounded text-[24px] font-[500] text-white"
                 
                >
                 1900.277270
                </p>

                <p className="lg:mt-[6px]">
                Hoặc qua facbook tại: https://www.facebook.com/nhahanglangman
          </p>
          <div className="mt-[20px]" style={{borderRadius: '10px', overflow: 'hidden'}}>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.237407409542!2d106.66936101485709!3d10.793120461836525!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752fb348054ebb%3A0x8a2ef8b6e3ede66f!2sMars%20Venus%20Restaurant%20-%20Wine%20Cellar!5e0!3m2!1svi!2s!4v1672303774951!5m2!1svi!2s" style={{width:"100%", height:"400px", allowfullscreen:"", loading:"lazy", referrerpolicy:"no-referrer-when-downgrade"}}></iframe>
          </div>
        </div>
      </div>
      
            </div>

            <div className="w-1/2">
              <div>
              <div className="text-left text-[#F0C753] py-[15px] sm:py-[15px] font-[700] text-[16px]">Gửi ý kiến phản hồi</div>


              <form className="flex flex-col bg-[#1C1C1C] p-[16px] gap-[10px]" action="#" method="post">
              <label className="font-[500] text-[13px] marfont">Họ tên*</label>
              <input className="bg-[#1C1C1C] inputform py-[5px] px-[10px] marfont border border-white" type="text" id="name" name="name" placeholder="Họ tên"/>
              
              <label className="font-[500] text-[13px] marfont">Số điện thoại*</label>
              <input className="bg-[#1C1C1C] inputform py-[5px] px-[10px] marfont border border-white" type="text" id="phone" name="phone" placeholder="Số điện thoại"/>
              

              <label className="font-[500] text-[13px] marfont">Email*</label>
              <input className="bg-[#1C1C1C] inputform py-[5px] px-[10px] marfont border border-white" type="text" id="email" name="email" placeholder="Nhập địa chỉ Email"/>
              
              <label className="font-[500] text-[13px] marfont">Nội dung*</label>
              <textarea className="bg-[#1C1C1C] inputform py-[5px] px-[10px] marfont border border-white" id="content" rows="5" name="content" placeholder="Viết nội dung"></textarea>
              
              <button className="px-[16px] py-[8px] mt-[20px] rounded text-[16px] font-[500] text-white"  style={{
                    background:
                      'linear-gradient(90deg, #8E7A49 0%, #FCCF64 49.48%, #8E7A49 100%)',
                  }} type="submit">Chỉnh sửa</button>
            </form>
              </div>
            
              </div>
        </div>
        
      </div>
    </>
  )
}

export async function getServerSideProps({ params }) {

  //const id = parseInt(params.slug,10);
  
 //console.log(id);

 
  const { data } =  await getServiceDataById(52);
  return {
    props: {
      service:data.get_deal
    },
  };

}

export default Service;