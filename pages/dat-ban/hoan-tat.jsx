import React, { useEffect, useState } from 'react'
import ModalAlert from 'components/Modal/ModalAlert'
import {
  BigHeartPickIcon,
  MomoActive,
  VCB,
  VCBActive,
  Momo,
  MomoQR,
} from 'assets/index'
import RadioButton from 'components/common/RadioButton/RadioButton'
import Button from 'components/common/Button/Button'
import { useDispatch, useSelector } from 'react-redux'
import {
  selectChooseMenu,
  selectServiceData,
  selectStep1Data,
  selectChoosePayment,
  selectPositionData,
} from 'selectors/booking.selector'

import { useRouter } from 'next/router'
import { bookingApi } from 'services/booking.service'
import moment from 'moment'
import Title from 'components/Booking/Title/Title'
import { standardizedServiceDataApi } from 'helpers/standardizedData'
import toast from 'react-hot-toast'

const PAYMENT = [
  {
    value: 'vcb',
    active: <VCBActive />,
    image: <VCB />,
  },
  {
    value: 'mm',
    active: <MomoActive />,
    image: <Momo />,
  },
]

const Completed = () => {
  const [isOpenModal, setIsOpenModal] = useState(false)

  const dataStep1 = useSelector(selectStep1Data)
  const dataChooseMenu = useSelector(selectChooseMenu)
  const dataService = useSelector(selectServiceData)
  const dataPayment = useSelector(selectChoosePayment)
  const dataPosition = useSelector(selectPositionData)

  const dispatch = useDispatch()
  const router = useRouter()

  const handleCallBookingApi = async (params) => {
    try {
      const res = await bookingApi(params)
      setIsOpenModal(true)
    } catch (error) {
      toast.error(error.message)
    }
  }

  const onConfirm = () => {
    handleCallBookingApi({
      customer_name: dataPayment.customer_name,
      customer_phone: dataPayment.customer_phone,
      branch_id: dataStep1.selectedBranch.id,
      number_people: dataStep1.numberPeople,
      party_type: dataStep1.partyType,
      start_time: moment(dataStep1.startTime).format('HH:mm'),
      check_in_time: moment(dataStep1.checkInTime).format('YYYY-MM-DD'),
      price: dataPayment.price,
      pre_paid: dataPayment.pre_paid,
      menu_id: dataChooseMenu.id,
      deal_id: dataPosition.id,
      services: standardizedServiceDataApi(dataService),
    })
  }

  return (
    <>
      <ModalAlert
        isOpen={isOpenModal}
        // hasIconClose={false}
        icon={<BigHeartPickIcon />}
        message="Gửi yêu cầu thành công"
        subMessage="Cảm ơn quý khách đã sử dụng dịch vụ của nhà hàng Mars Venus."
        titleButton="Hoàn tất"
        onClickButton={() => {
          setIsOpenModal(false)
          router.push('/')
        }}
        closeModal={() => setIsOpenModal(false)}
      ></ModalAlert>
      <div className="px-3 bg-[#141416] py-6 text-white md:w-[521px] md:my-0 md:mx-[auto] min-height-content md:p-6">
        <div className="mb-4 flex justify-center">
          <Title title="HOÀN TẤT" checked />
        </div>

        <div className="text-[14px] mb-4 md:text-center">
          Đặt tiệc của bạn sắp hoàn tất. Bạn vui lòng hoàn tất thanh toán trong
          30 phút để hoàn thành việc đặt bàn.
        </div>

        <div className="border border-[#F0C753] rounded-xl p-3 mt-2">
          <span className="font-[700] text-[14px] text-white">Thanh toán</span>
          <div className="flex justify-between mt-2 mb-5">
            <div>
              <RadioButton
                label={
                  dataPayment.paymentTypeSelected === 1
                    ? 'Đặt cọc một phần'
                    : 'Thanh toán toàn bộ'
                }
                defaultChecked={true}
                name="type"
              />
            </div>
            <span className="text-white text-[14px]">
              {dataPayment.paymentTypeSelected === 1
                ? dataPayment?.pre_paid?.toLocaleString('de-DE')
                : dataPayment?.price?.toLocaleString('de-DE')}
              đ
            </span>
          </div>

          <div className="text-[14px] text-white mb-5">
            Chọn hình thức thanh toán{' '}
            {dataPayment.paymentSelected === 'vcb' ? 'chuyển khoản' : 'Momo'}
          </div>

          {dataPayment.paymentSelected === 'vcb' ? (
            <>
              <div className="flex items-center justify-between mb-4">
                <div className="">
                  <div className="text-[15px] font-[600] mb-1">Vietcombank</div>
                  <div className="text-[10px] font-[400]">
                    Ngân hàng TMCP Ngoại thương Việt Nam
                  </div>
                </div>
                <button className="px-[23px] py-2 border border-[rgba(255, 255, 255, 0.5)] rounded">
                  Copy
                </button>
              </div>
              <div className="flex justify-between items-center mb-2">
                <span>Tên tài khoản:</span>
                <span>Mars Venus</span>
              </div>
              <div className="flex justify-between items-center mb-2">
                <span>Số tài khoản:</span>
                <span>1025627476</span>
              </div>
              <div className="flex justify-between items-center mb-2">
                <span>Chi nhánh:</span>
                <span>Bến Nghé</span>
              </div>
            </>
          ) : (
            <>
              <div className="flex items-center justify-between mb-4">
                <div className="">
                  <div className="text-[15px] font-[600] mb-1">
                    Ví điện tử MoMo
                  </div>
                  <div className="text-[10px] font-[400]">
                    Siêu Ứng Dụng Thanh Toán số 1 Việt Nam
                  </div>
                </div>
                <button className="px-[23px] py-2 border border-[rgba(255, 255, 255, 0.5)] rounded">
                  Copy
                </button>
              </div>
              <div className="flex justify-between items-center">
                <span>Người nhận:</span>
                <span>Mars Venus</span>
              </div>
              <div className="flex justify-between items-center">
                <span>Số điện thoại:</span>
                <span>09456514325</span>
              </div>
            </>
          )}

          <div className="w-full h-px bg-slate-300 my-3"></div>

          <div className="mb-3 text-[15px]">
            Vui lòng chuyển khoản theo đúng nội dung
          </div>
          <div className="p-3 text-[13px] bg-[#FFFFFF] bg-opacity-10 rounded-[6px]">
            SĐT (dấu cách){' '}
            {dataPayment.paymentSelected === 'vcb' ? 'MÃ ĐẶT BÀN' : 'DATBAN'}
          </div>
          {dataPayment.paymentSelected === 'mm' ? (
            <div className="flex items-center justify-between mt-4">
              <MomoQR />
              <div>
                <div className="text-center mb-1">Tải mã QR để quét</div>
                <Button className="w-[183px]">Tải mã QR tại đây</Button>
              </div>
            </div>
          ) : (
            <></>
          )}
        </div>
        <div className="text-[12px] mt-5 text-center">
          Bạn thanh toán xong chưa?
        </div>
        <Button
          className="w-full mt-2 text-white py-[14px]"
          onClick={onConfirm}
        >
          Tôi xác nhận đã thanh toán
        </Button>
      </div>
    </>
  )
}

export default Completed
