import SelectService from 'components/Booking/SelectService/SelectService'
import BookingLayout from 'layouts/BookingLayout'

const StepThree = ({ serviceData }) => {
  return <SelectService serviceData={serviceData} />
}

StepThree.Layout = BookingLayout

export default StepThree
