import client from 'apollo-client'
import { BigHeartPickIcon } from 'assets/index'
import BranchInfo from 'components/BranchInfo/BranchInfo'
import ModalAlert from 'components/Modal/ModalAlert'
import SelectBranch from 'components/SelectBranch/SelectBranch'
import { GET_LIST_BRANCHES } from 'graphql/branch'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { bookingActions } from 'slices/booking'

import Button from 'components/common/Button/Button'
import BookingLayout from 'layouts/BookingLayout'
import _get from 'lodash/get'
import moment from 'moment'
import { getUserInfoSelector } from 'selectors/auth'

import Title from 'components/Booking/Title/Title'
import BookingInfoModal from 'components/BookingInfoModal/index'
import { isEmpty } from 'lodash'
import { useRouter } from 'next/router'
import { selectStep1Data } from 'selectors/booking.selector'
import { bookingApi } from 'services/booking.service'
import ClientOnly from 'components/ClientOnly'

const ChooseBranch = () => {
  const dispatch = useDispatch()

  const router = useRouter()
  const userInfo = useSelector(getUserInfoSelector)
  const step1Data = useSelector(selectStep1Data)

  const [isOpenModal, setIsOpenModal] = useState(false)
  const [isOpenModalBookingInfo, setOpenModalBookingInfo] = useState(false)
  const [listBranch, setListBranch] = useState([])

  const [startTime, setStartTime] = useState()
  const [checkInTime, setCheckInTime] = useState()
  const [selectedBranch, setSelectedBranch] = useState()
  const [numberPeople, setNumberPeople] = useState()
  const [partyType, setPartyType] = useState()

  const fetchData = async () => {
    const { data } = await client.query({
      query: GET_LIST_BRANCHES,
    })
    setListBranch(data?.list_branches?.data)
  }

  useEffect(() => {
    fetchData()
  }, [])

  useEffect(() => {
    if (!isEmpty(step1Data)) {
      setStartTime(step1Data.startTime)
      setCheckInTime(step1Data.checkInTime)
      setSelectedBranch(step1Data.selectedBranch)
      setNumberPeople(step1Data.numberPeople)
      setPartyType(step1Data.partyType)
    }
  }, [step1Data])

  const handleCallBookingApi = async (params) => {
    const res = await bookingApi(params)
    setIsOpenModal(true)
    if (isOpenModalBookingInfo) setOpenModalBookingInfo(false)
  }

  const handleNextStep = () => {
    if (numberPeople && numberPeople <= 2) {
      dispatch(
        bookingActions.setStep1Data({
          selectedBranch,
          startTime,
          checkInTime,
          numberPeople,
          partyType,
        }),
      )
      router.push('/dat-ban/chon-vi-tri')
    } else {
      // logined
      if (!isEmpty(userInfo)) {
        // call api booking
        handleCallBookingApi({
          customer_name: userInfo.name,
          customer_phone: userInfo.phone,
          branch_id: selectedBranch.id,
          number_people: numberPeople,
          party_type: partyType,
          start_time: moment(startTime).format('HH:mm'),
          check_in_time: moment(checkInTime).format('YYYY-MM-DD'),
        })
      } else {
        // show modal điền thông tin và call api
        setOpenModalBookingInfo(true)
      }
    }
  }

  return (
    <ClientOnly>
      <div className="min-height-content">
        <ModalAlert
          isOpen={isOpenModal}
          icon={<BigHeartPickIcon />}
          message="Gửi yêu cầu thành công"
          subMessage="Cảm ơn quý khách đã sử dụng dịch vụ của nhà hàng Mars Venus."
          titleButton="Hoàn tất"
          onClickButton={() => {
            setIsOpenModal(false)
            router.push('/')
          }}
          closeModal={() => setIsOpenModal(false)}
        ></ModalAlert>
        <BookingInfoModal
          isOpen={isOpenModalBookingInfo}
          closeModal={() => setOpenModalBookingInfo(false)}
          onSubmit={(bonusParams) => {
            handleCallBookingApi({
              ...bonusParams,
              branch_id: selectedBranch.id,
              number_people: numberPeople,
              party_type: partyType,
              start_time: moment(startTime).format('HH:mm'),
              check_in_time: moment(checkInTime).format('YYYY-MM-DD'),
            })
          }}
        />
        <div className="flex justify-center md:justify-start mb-4 md:mb-8">
          <Title title="Chọn chi nhánh" checked={true} />
        </div>
        <div className="mb-6 md:mb-12 -mx-3 lg:mx-0">
          <SelectBranch
            listBranch={listBranch}
            onClickBranch={setSelectedBranch}
            selectedBranch={selectedBranch}
          />
        </div>
        <div className="text-center">
          <div className="mb-6 md:mb-12">
            <BranchInfo
              selectedBranch={selectedBranch}
              startTime={startTime}
              setStartTime={setStartTime}
              numberPeople={numberPeople}
              setNumberPeople={setNumberPeople}
              partyType={partyType}
              setPartyType={setPartyType}
              checkInTime={checkInTime}
              setCheckInTime={setCheckInTime}
            />
          </div>
          <Button
            disabled={
              !selectedBranch ||
              !checkInTime ||
              !startTime ||
              !numberPeople ||
              !partyType
            }
            className="text-[14px] leading-[16.8px] text-white w-full md:w-[350px] py-[14px]"
            onClick={handleNextStep}
          >
            {numberPeople && numberPeople > 2
              ? 'Gửi yêu cầu đặt bàn'
              : '1/5 Chọn chi nhánh này'}
          </Button>
        </div>
      </div>
    </ClientOnly>
  )
}

ChooseBranch.Layout = BookingLayout

export default ChooseBranch
