import React, { useState } from 'react'
import ModalAlert from 'components/Modal/ModalAlert'
import {
  BigHeartPickIcon,
  MomoActive,
  VCB,
  VCBActive,
  Momo,
  Ale,
  Active,
  AleActive,
} from 'assets/index'
import RadioButton from 'components/common/RadioButton/RadioButton'
import Button from 'components/common/Button/Button'
import { useDispatch, useSelector } from 'react-redux'
import {
  selectChooseMenu,
  selectPositionData,
  selectServiceData,
  selectStep1Data,
} from 'selectors/booking.selector'
import { bookingActions } from 'slices/booking'

import { standardizedServiceData } from 'helpers/standardizedData'
import { useRouter } from 'next/router'
import Title from 'components/Booking/Title/Title'
import moment from 'moment'
import BookingLayout from 'layouts/BookingLayout'

export const PAYMENT_TYPE = {
  part: {
    value: 1,
    label: 'Đặt cọc một phần',
  },
  all: {
    value: 2,
    label: 'Thanh toán toàn bộ',
  },
}

const PAYMENT = [
  {
    value: 'vcb',
    active: <VCBActive />,
    image: <VCB />,
    isDisable: false,
  },
  {
    value: 'mm',
    active: <MomoActive />,
    image: <Momo />,
    isDisable: false,
  },
  {
    value: 'ale',
    image: <Ale />,
    active: <AleActive />,
    isDisable: true,
  },
]

const Payment = () => {
  const [isOpenModal, setIsOpenModal] = useState(false)

  const dispatch = useDispatch()

  const [name, setName] = useState('')
  const [isErrorName, setErrorName] = useState(false)

  const [phone, setPhone] = useState('')
  const [errorMessage, setErrorMessage] = useState('')

  const [paymentSelected, setPaymentSelected] = useState()
  const [paymentTypeSelected, setPaymentTypeSelected] = useState(1)

  const dataStep1 = useSelector(selectStep1Data)
  const dataPosition = useSelector(selectPositionData)
  const dataChooseMenu = useSelector(selectChooseMenu)
  const dataService = useSelector(selectServiceData)
  const standardDataService = standardizedServiceData(dataService)
  const servicePrice = standardDataService.reduce(function (acc, obj) {
    return acc + obj.salePrice
  }, 0)
  const partPrice = 200000 + servicePrice
  const fullPrice = servicePrice + (dataChooseMenu.salePrice * 1.7 || 0)

  const router = useRouter()

  const onChangeName = (e) => {
    const value = e.target.value
    setName(value)
    if (!value) {
      setErrorName(true)
    } else {
      setErrorName(false)
    }
  }

  const handleCheckPhone = (phoneNumber) => {
    if (!phoneNumber) {
      setErrorMessage('Vui lòng nhập số điện thoại')
    } else {
      let phoneNumReg = /^(\+84|\+840|84|084|840|0)([9235678]{1}[0-9]{8})$/
      const isPhoneNumberValidate = phoneNumReg.test(phoneNumber)
      if (phoneNumber && !isPhoneNumberValidate) {
        setErrorMessage('Số điện thoại sai định dạng')
      } else {
        setErrorMessage('')
      }
    }
  }

  const onChangePhone = (e) => {
    const value = e.target.value
    setPhone(value)
    handleCheckPhone(value)
  }

  const onNextStep = () => {
    dispatch(
      bookingActions.setChoosePayment({
        customer_name: name,
        customer_phone: phone,
        price: fullPrice,
        pre_paid: partPrice,
        paymentTypeSelected,
        paymentSelected,
      }),
    )
    router.push('/dat-ban/hoan-tat')
  }

  return (
    <>
      <ModalAlert
        isOpen={isOpenModal}
        // hasIconClose={false}
        icon={<BigHeartPickIcon />}
        message="Gửi yêu cầu thành công"
        subMessage="Cảm ơn quý khách đã sử dụng dịch vụ của nhà hàng Mars Venus."
        titleButton="Hoàn tất"
        onClickButton={() => setIsOpenModal(false)}
        closeModal={() => setIsOpenModal(false)}
      ></ModalAlert>
      <div>
        <div className="flex justify-center md:justify-start mb-4 md:mb-8">
          <Title title="CHỌN HÌNH THỨC THANH TOÁN" checked />
        </div>
        <div className="flex flex-col md:flex-row">
          <div className="mb-3 md:w-[49%] md:mr-[2%]">
            <span className="block text-white font-[400] text-[13px]">
              Họ và Tên<span className="text-[#FD3E30]">*</span>
            </span>
            <input
              defaultValue={name}
              className="mt-2 p-3 border border-[#F0C753] placeholder-slate-400 focus:outline-none focus:ring-sky-500 block w-full rounded-xl sm:text-sm focus:ring-1 bg-transparent text-white"
              placeholder="Nhập họ và tên"
              onChange={onChangeName}
            />
            {isErrorName ? (
              <div className="mt-2 text-[12px] text-[#FD3E30]">
                Vui lòng nhập tên
              </div>
            ) : (
              <></>
            )}
          </div>

          <div className="mb-5 md:w-[49%]">
            <span className="block text-white font-[400] text-[13px]">
              Số điện thoại<span className="text-[#FD3E30]">*</span>
            </span>
            <input
              defaultValue={phone}
              className="mt-2 p-3 border border-[#F0C753] placeholder-slate-400 focus:outline-none focus:ring-sky-500 block w-full rounded-xl sm:text-sm focus:ring-1 bg-transparent text-white"
              placeholder="Nhập số điện thoại"
              onChange={onChangePhone}
            />
            {errorMessage ? (
              <div className="mt-2 text-[12px] text-[#FD3E30]">
                {errorMessage}
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>

        <div className="border border-[#F0C753] rounded-xl p-3">
          <div>
            <span className="font-[400] text-[14px] text-white">
              Bạn chọn thời gian - địa điểm
            </span>
            <div className="text-white flex flex-wrap mb-[14px] mt-3 text-[13px]">
              <div className="px-3 rounded-[16px] border border-[#F0C753] mb-2 mr-[10px] py-[6px]">
                {moment(dataStep1?.startTime).format('HH:mm')} -{' '}
                {moment(dataStep1?.checkInTime).format('DD/MM/YYYY')}
              </div>
              <div className="px-3 rounded-[16px] border border-[#F0C753] mb-2 mr-[10px] py-[6px]">
                {dataStep1?.selectedBranch?.name}
              </div>
              <div className="px-3 rounded-[16px] border border-[#F0C753] mb-2 mr-[10px] py-[6px]">
                {dataStep1?.partyType}
              </div>
              <div className="px-3 rounded-[16px] border border-[#F0C753] mb-2 mr-[10px] py-[6px]">
                {dataStep1?.numberPeople} người
              </div>
              <div className="px-3 rounded-[16px] border border-[#F0C753] mb-2 mr-[10px] py-[6px]">
                {dataPosition.name}
              </div>
            </div>
            <div className="w-full text-right">
              <Button
                isSmallButton
                className="rounded-[16px] text-white"
                onClick={() => {
                  router.push('/dat-ban/chon-chi-nhanh')
                }}
              >
                Thay đổi
              </Button>
            </div>

            <div className="w-full h-px bg-slate-300 my-3"></div>

            <span className="font-[400] text-[14px] text-white">
              Bạn chọn món ngon
            </span>
            <div className="text-white flex flex-wrap mb-[16px] mt-3">
              <div className="px-3 rounded-[16px] border border-[#F0C753] py-[6px]">
                {dataChooseMenu.name}
              </div>
            </div>
            <div className="text-white text-[12px] mb-[19px]">
              +{(dataChooseMenu?.salePrice * 0.7)?.toLocaleString('de-DE')}đ
              (Phí dịch vụ: 7% chi phí món ăn, không thu phí VAT)
            </div>

            <div className="flex justify-between items-center">
              <span className="text-[13px] text-[#F0C753]">
                {(dataChooseMenu?.salePrice * 1.7).toLocaleString('de-DE')}đ
              </span>
              <Button
                isSmallButton
                className="rounded-[16px] text-white"
                onClick={() => {
                  router.push('/dat-ban/chon-menu')
                }}
              >
                Thay đổi
              </Button>
            </div>
            <div className="text-[10px] text-[#F0C753] mt-2">
              Nếu khách có nhu cầu xuất hóa đơn Quý khách vui lòng thanh toán
              thêm 8% VAT
            </div>

            <div className="w-full h-px bg-slate-300 my-3"></div>
            <span className="font-[400] text-[14px] text-white">
              Bạn chọn dịch vụ
            </span>
            <div className="text-white flex flex-wrap mb-[14px] mt-3 text-[13px]">
              {standardDataService.length ? (
                standardDataService.map((item) => (
                  <div
                    key={item.fieldName}
                    className="px-3 rounded-[16px] border border-[#F0C753] mb-2 mr-[10px] py-[6px]"
                  >
                    {item.fieldName}
                  </div>
                ))
              ) : (
                <span className="text-[13px]">Bạn chưa chọn dịch vụ nào!!</span>
              )}
            </div>
            <div className="flex justify-between items-center">
              <span className="text-[13px] text-[#F0C753]">
                {servicePrice?.toLocaleString('de-DE')}
              </span>
              <Button
                isSmallButton
                className="rounded-[16px] text-white"
                onClick={() => {
                  router.push('/dat-ban/chon-dich-vu')
                }}
              >
                Thay đổi
              </Button>
            </div>
          </div>
        </div>

        <div className="border border-[#F0C753] rounded-xl p-3 mt-2 ">
          <span className="text-white">Thanh toán</span>
          <div className="flex justify-between mt-4">
            <div>
              <RadioButton
                label={PAYMENT_TYPE.part.label}
                value={PAYMENT_TYPE.part.value}
                checked={paymentTypeSelected == PAYMENT_TYPE.part.value}
                onChange={(e) => setPaymentTypeSelected(e.target.value)}
                name="type"
              />
            </div>
            <span className="text-white text-[14px]">
              {partPrice?.toLocaleString('de-DE')}đ
            </span>
          </div>
          <ul className="list-disc px-5 text-white mt-3">
            <li>
              <span className="text-[12px]">
                Thanh toán tối thiểu: 200.000đ
              </span>
            </li>
            <li>
              <span className="text-[12px]">
                Hoa và Bánh kem là các khoản bắt buộc thanh toán trước.
              </span>
            </li>
            <li>
              <span className="text-[12px]">
                Phí đặt cọc không hoàn lại 200,000đ – sẽ được trừ vào bill sau
                bữa ăn
              </span>
            </li>
          </ul>
          <div className="flex justify-between my-4">
            <div>
              <RadioButton
                label={PAYMENT_TYPE.all.label}
                value={PAYMENT_TYPE.all.value}
                checked={paymentTypeSelected == PAYMENT_TYPE.all.value}
                onChange={(e) => setPaymentTypeSelected(e.target.value)}
                name="type"
              />
            </div>
            <span className="text-white text-[14px]">
              {fullPrice?.toLocaleString('de-DE')}đ
            </span>
          </div>
          <span className="text-[14px] text-white">
            Chọn hình thức thanh toán:
          </span>
          <div className="mt-4 flex justify-around md:justify-start">
            {PAYMENT.map((item, index) => (
              <div
                key={index}
                onClick={() =>
                  !item.isDisable && setPaymentSelected(item.value)
                }
                className="relative md:mr-3"
              >
                {item.value === paymentSelected ? item.active : item.image}
                <div className="absolute top-[-15px] right-[-15px]">
                  {item.value === paymentSelected ? <Active /> : <></>}
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="text-center mt-4">
          <Button
            className="text-[14px] leading-[16.8px] text-white w-full py-[14px] md:w-[350px]"
            onClick={onNextStep}
            disabled={
              !name || !phone || !paymentSelected || errorMessage || isErrorName
            }
          >
            Đặt bàn
          </Button>
        </div>
      </div>
    </>
  )
}

Payment.Layout = BookingLayout

export default Payment
