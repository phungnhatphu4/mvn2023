import ChoosePosition from 'components/Booking/ChoosePosition/ChoosePosition'
import ClientOnly from 'components/ClientOnly'
import BookingLayout from 'layouts/BookingLayout'

const StepTwo = () => {
  return (
    <ClientOnly>
      <ChoosePosition />
    </ClientOnly>
  )
}

StepTwo.Layout = BookingLayout

export default StepTwo
