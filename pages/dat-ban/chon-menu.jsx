import ChooseMenu from 'components/Booking/ChooseMenu/ChooseMenu'
import ClientOnly from 'components/ClientOnly'
import BookingLayout from 'layouts/BookingLayout'

const StepChooseMenu = () => {
  return (
    <ClientOnly>
      <ChooseMenu />
    </ClientOnly>
  )
}

StepChooseMenu.Layout = BookingLayout

export default StepChooseMenu
