import { ApolloProvider } from '@apollo/client'
import client from 'apollo-client'
import { AuthUserProvider } from 'context/AuthUserContext'
import { END } from 'redux-saga'
import { wrapper } from '../redux/store'
import '../styles/globals.css'
import 'swiper/css'
import 'react-datepicker/dist/react-datepicker.css'
import DefaultLayout from 'layouts/Layout'
import { Toaster } from 'react-hot-toast'

const MyApp = ({ Component, pageProps }) => {
  const Layout = Component.Layout || DefaultLayout
  return (
    <ApolloProvider client={client}>
      <AuthUserProvider>
        <Layout>
          <Toaster position="bottom-center" />
          <Component {...pageProps} />
        </Layout>
      </AuthUserProvider>
    </ApolloProvider>
  )
}

export const getInitialProps = wrapper.getInitialAppProps(
  (store) => async (context) => {
    const pageProps = {
      ...(await MyApp.getInitialProps(context).pageProps),
    }
    if (context.ctx.req) {
      store.dispatch(END)
      await store.sagaTask.toPromise()
    }

    return { pageProps }
  },
)

export default wrapper.withRedux(MyApp)
