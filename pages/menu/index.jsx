import { useState } from "react"
import client from "apollo-client";
import { GET_LIST_BRANCHES } from "graphql/branch";
import ChooseMenu from 'components/Menu/ChooseMenu/ChooseMenu'
import SelectBranch from "components/SelectBranch/SelectBranch";
import SwiperPerfectSpace from "components/swiper/SwiperPerfectSpace";
import Image from "next/image";
import BranchInfo from 'assets/images/branch/mobile-introduce.jpg'
import { Breadcrumb } from "components/Breadcrumb/index";

const listBreadcrumb = [
  {
    link: '/',
    name: 'Trang chủ'
  },
  {
    name: 'Chi nhánh'
  }
]

const Branch = ({ listBranch }) => {
  const [selectedBranch, setSelectedBranch] = useState(listBranch?.[0]);

  return (
    <>
      <Breadcrumb listBreadcrumb={listBreadcrumb} />
      <div className="container">
        <div className="text-center leading-[20px] text-[#F0C753] py-[16px] sm:py-[32px] font-[700]">menu</div>
        <div className="mb-6 lg:mt-[12px]">
          <div className="mb-0 lg:mb-6">
		  <ChooseMenu/>
            
          </div>
          
        </div>
        
      </div>
    </>
  )
}

export async function getServerSideProps() {
  const { data } = await client.query({
    query: GET_LIST_BRANCHES,
  });

  return {
    props: {
      listBranch: data?.list_branches?.data,
    },
  };
}

export default Branch;