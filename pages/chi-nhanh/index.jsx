import { useState } from "react"
import client from "apollo-client";
import { GET_LIST_BRANCHES } from "graphql/branch";

import SelectBranch from "components/SelectBranch/SelectBranch";
import SwiperPerfectSpace from "components/swiper/SwiperPerfectSpace";
import Image from "next/image";
import BranchInfo from 'assets/images/branch/mobile-introduce.jpg'
import { Breadcrumb } from "components/Breadcrumb/index";

const listBreadcrumb = [
  {
    link: '/',
    name: 'Trang chủ'
  },
  {
    name: 'Chi nhánh'
  }
]

const Branch = ({ listBranch }) => {
  const [selectedBranch, setSelectedBranch] = useState(listBranch?.[0]);

  return (
    <>
      <Breadcrumb listBreadcrumb={listBreadcrumb} />
      <div className="container">
        <div className="text-center leading-[20px] text-[#F0C753] py-[16px] sm:py-[32px] font-[700]">Chi nhánh</div>
        <div className="mb-6 lg:mt-[12px]">
          <div className="mb-4 lg:mb-6">
            <SelectBranch
              listBranch={listBranch}
              onClickBranch={setSelectedBranch}
              selectedBranch={selectedBranch}
              zoomSelected
            />
          </div>
          {selectedBranch ? <div className="lg:mt-[32px] text-white text-center">
            <h4 className="text-[15px] text-center text-white uppercase font-[700]">
              {selectedBranch?.name}
            </h4>
            <p className="lg:mt-[12px] mt-[6px] font-[400] text-[13px]">{selectedBranch?.address}</p>
          </div> : null
          }
        </div>
        <div className="pb-[50px] lg:pt-[30px]">
          <div className="text-[15px] leading-[21px] text-[#F0C753] text-center mb-4 lg:text-[28px] lg:leading-[39px]">
            Hình không gian
          </div>
          <div>
            <SwiperPerfectSpace
             selectedBranch={selectedBranch}
            />
          </div>
          <div className="mt-6 text-[13px] text-white lg:px-[5%] px-[15px]">
            <div className="w-full h-full text-center md:hidden">
              <Image
                src={BranchInfo}
                alt="title"
                className="object-cover w-10 h-10"
              />
            </div>
            <p className="lg:mt-[24px] mt-[16px]">
              Với những món ăn Tây và nhiều loại rượu vang đỏ thơm mộng như môi nàng, Mars Venus mang đến không gian cổ tích cùng ánh đèn vàng thơ mộng, du dương những nốt nhạc Cello chuẩn trời Âu, tâng bốc thêm cho vẻ đẹp quyến rũ và nồng cháy của tình yêu đôi lứa.
            </p>

            <p className="lg:mt-[24px] mt-[16px]">
              Mars Venus - với sứ mệnh cung cấp không gian lãng mạn cho những chàng trai sắp ngỏ lời cầu hôn người con gái của đời mình, chúng tôi đảm bảo mang đến sự hài lòng cho bạn và người ấy ngay từ những trải nghiệm đầu tiên.
            </p>

            <p className="lg:mt-[24px] mt-[16px]">
              ----
            </p>
            <p className="lg:mt-[6px]">
              Liên hệ để đặt bàn tại 3 chi nhánh: Quận 3 (không gian mái nhà ấm cúng), Quận 7 (không gian ngoài trời, ngắm hoàng hôn trên sông) và Quận Phú Nhuận (Không gian kết hợp sân vườn và trong nhà độc đáo với kiến trúc Hầm rượu)
              Chi nhá
            </p>
          </div>
        </div>
      </div>
    </>
  )
}

export async function getServerSideProps() {
  const { data } = await client.query({
    query: GET_LIST_BRANCHES,
  });

  return {
    props: {
      listBranch: data?.list_branches?.data,
    },
  };
}

export default Branch;