import Link from 'next/link'

const ComingSoon = () => {
  return (
    <div className="container">
      <div className="min-h-[80vh] flex items-center justify-center flex-col text-white">
        <div>
          <h1 className=" text-[24px] md:text-[32px]">COMING SOON!</h1>
        </div>
        <div>
          <Link href="/">
            <a className="text-[#F0C753] underline cursor-pointer text-[14px] md:text-[18px]">
              Quay lại trang chủ
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default ComingSoon
