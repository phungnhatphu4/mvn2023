import client from 'apollo-client'
import BannerHome from 'assets/images/banner-home.png'
import BranchInfo from 'components/BranchInfo/BranchInfo'
import ClientOnly from 'components/ClientOnly'
import SelectBranch from 'components/SelectBranch/SelectBranch'
import SwiperEventAndPost from 'components/swiper/SwiperEventAndPost'
import SwiperFeaturedService from 'components/swiper/SwiperFeaturedService'
import SwiperPerfectSpace from 'components/swiper/SwiperPerfectSpace'
import SwiperSpecialDelicacies from 'components/swiper/SwiperSpecialDelicacies'
import { GET_LIST_BRANCHES } from 'graphql/branch'
import Image from 'next/image'
import { useEffect, useState } from 'react'

const Home = () => {
  const [listBranch, setListBranch] = useState([])
  const [selectedBranch, setSelectedBranch] = useState()

  const fetchData = async () => {
    const { data } = await client.query({
      query: GET_LIST_BRANCHES,
    })
    setListBranch(data?.list_branches?.data)
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <ClientOnly>
      <div className="container">
        <div className="xl:-mx-[150px] flex">
          <Image src={BannerHome} />
        </div>
        <div className="mb-6 lg:mt-[72px]">
          <div className="mb-6">
            <SelectBranch
              listBranch={listBranch}
              onClickBranch={setSelectedBranch}
              selectedBranch={selectedBranch}
            />
          </div>
          <div className="px-3 xl:px-0">
            <BranchInfo
              selectedBranch={selectedBranch}
              isShowChooseOrderTime={false}
            />
          </div>
        </div>
        <div className="px-3 pb-10 xl:px-0">
          <div className="mb-6">
            <div className="text-[15px] leading-[21px] text-[#F0C753] text-center mb-2 lg:text-[28px] lg:leading-[39px]">
              Chúng tôi làm nên buổi tối hoàn hảo
            </div>
            <div className="text-[13px] leading-[18.2px] text-white text-center lg:text-base lg:leading-6">
              Mars Venus là một lựa chọn không thể chối từ. Với không gian bài
              trí cũng như chất lượng nguyên liệu của bữa ăn ưu ái cho cặp đôi.
            </div>
          </div>
          <div className="mb-6">
            <div className="text-[15px] leading-[21px] text-[#F0C753] text-center mb-4 lg:text-[28px] lg:leading-[39px]">
              Món ngon đặc biệt
            </div>
            <div>
              <SwiperSpecialDelicacies />
            </div>
          </div>
          <div className="mb-6">
            <div className="text-[15px] leading-[21px] text-[#F0C753] text-center mb-4 lg:text-[28px] lg:leading-[39px]">
              Không gian hoàn hảo
            </div>
            <div>
              <SwiperPerfectSpace />
            </div>
          </div>
          <div className="mb-6">
            <div className="text-[15px] leading-[21px] text-[#F0C753] text-center mb-4 lg:text-[28px] lg:leading-[39px]">
              Dịch vụ đặc sắc
            </div>
            <div>
              <SwiperFeaturedService />
            </div>
          </div>
          <div>
            <div className="text-[15px] leading-[21px] text-[#F0C753] text-center mb-4 lg:text-[28px] lg:leading-[39px]">
              Sự kiện &#38; Bài viết
            </div>
            <div>
              <SwiperEventAndPost />
            </div>
          </div>
        </div>
      </div>
    </ClientOnly>
  )
}

export default Home
