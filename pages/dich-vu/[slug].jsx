import { useState } from "react"
import client from "apollo-client";
import { GET_LIST_BRANCHES } from "graphql/branch";

import SelectBranch from "components/SelectBranch/SelectBranch";
import SwiperPerfectSpace from "components/swiper/SwiperPerfectSpace";
import Image from "next/image";
import BranchInfo from 'assets/images/branch/mobile-introduce.jpg'

import { Breadcrumb } from "components/Breadcrumb/index";
import { useRouter } from "next/router"
import { getServiceDataById } from 'services/service.servicebyid'

var id;
const listBreadcrumb = [
  {
    link: '/',
    name: 'Trang chủ'
  },
  {
    name: 'Chi nhánh'
  }
]

const Service = ({ service,id }) => {
  const [selectedBranch, setSelectedBranch] = useState(service?.[0]);
  //const router = useRouter()
  //const { query: { id },} = router

  console.log("//////////////////");
  console.log(id);
  return (
    <>
      <Breadcrumb listBreadcrumb={listBreadcrumb} />
      <div className="container">
        <div className="text-center leading-[20px] text-[#F0C753] py-[16px] sm:py-[32px] font-[700]">{service.name}</div>
        
        <div className="pb-[50px] lg:pt-[30px]">
        <div className="mt-6 text-[13px] text-white lg:px-[5%] px-[15px]">
        <p className="lg:mt-[24px] mt-[16px]">
        {service.description}
            </p>
          <div>
           </div>
          </div>
          <div className="mt-6 text-[13px] text-white lg:px-[5%] px-[15px]">
            <div className="w-full h-full text-center">
            <img
                  src={service.image_urls[0]}
                  alt={service.name}
                  className="w-full h-full object-contain"
                />
             
            </div>
            <p className="lg:mt-[24px] mt-[16px]">
              Với những món ăn Tây và nhiều loại rượu vang đỏ thơm mộng như môi nàng, Mars Venus mang đến không gian cổ tích cùng ánh đèn vàng thơ mộng, du dương những nốt nhạc Cello chuẩn trời Âu, tâng bốc thêm cho vẻ đẹp quyến rũ và nồng cháy của tình yêu đôi lứa.
            </p>

            <p className="lg:mt-[24px] mt-[16px]">
              Mars Venus - với sứ mệnh cung cấp không gian lãng mạn cho những chàng trai sắp ngỏ lời cầu hôn người con gái của đời mình, chúng tôi đảm bảo mang đến sự hài lòng cho bạn và người ấy ngay từ những trải nghiệm đầu tiên.
            </p>

            <p className="lg:mt-[24px] mt-[16px]">
              ----
            </p>
            <p className="lg:mt-[6px]">
              Liên hệ để đặt bàn tại 3 chi nhánh: Quận 3 (không gian mái nhà ấm cúng), Quận 7 (không gian ngoài trời, ngắm hoàng hôn trên sông) và Quận Phú Nhuận (Không gian kết hợp sân vườn và trong nhà độc đáo với kiến trúc Hầm rượu)
              Chi nhá
            </p>
          </div>
        </div>
      </div>
    </>
  )
}

export async function getServerSideProps({ params }) {

  const id = parseInt(params.slug,10);
  
 console.log(id);

 
  const { data } =  await getServiceDataById(id);
  return {
    props: {
      service:data.get_deal,
      id:id
    },
  };

}

export default Service;