import { useState } from "react"
import client from "apollo-client";
import { GET_LIST_BRANCHES } from "graphql/branch";


import SelectBranch from "components/SelectService/SelectService";
import SwiperPerfectSpace from "components/swiper/SwiperPerfectSpace";
import Image from "next/image";
import BranchInfo from 'assets/images/branch/mobile-introduce.jpg'
import { Breadcrumb } from "components/Breadcrumb/index";

import { getServiceDataAll } from 'services/service.serviceall'


const listBreadcrumb = [
  {
    link: '/',
    name: 'Trang chủ'
  },
  {
    name: 'Chi nhánh'
  }
]

const Branch = ({ list_deals }) => {
	
  const [selectedBranch, setSelectedBranch] = useState(list_deals?.[0]);
	console.log(list_deals);
 
  return (
    <>
      <Breadcrumb listBreadcrumb={listBreadcrumb} />
      <div className="container">
        <div className="text-center leading-[20px] text-[#F0C753] py-[16px] sm:py-[32px] font-[700]">Dịch vụ</div>
        <div className="mb-6 lg:mt-[12px]">
          <div className="mb-4 lg:mb-6">
            <SelectBranch
              listBranch={list_deals}
              onClickBranch={setSelectedBranch}
              selectedBranch={selectedBranch}
              zoomSelected

            />
          </div>
         
        </div>
       
      </div>

      
    </>
  )
}

export async function getServerSideProps() {
  const { data } = await getServiceDataAll();

  return {
    props: {
      list_deals: data?.list_deals?.data,
    },
  };
}

export default Branch;