import apolloClient from 'apollo-client'
import { LIST_BOOKED_POSITIONS, LIST_POSITIONS } from 'graphql/position'

export function getListPositions(branchId) {
  console.log('get list positio---');
  console.log(branchId);
  return apolloClient.query({
    query: LIST_POSITIONS,
    variables: {
      branchId,
    },
  })
}

export function getListBookedPositions(branchId, checkInTime) {
  return apolloClient.query({
    query: LIST_BOOKED_POSITIONS,
    variables: {
      branchId,
      checkInTime,
    },
  })
}
