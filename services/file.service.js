import config from 'config'
import APIUtils from 'utils/apiUtils'

export const uploadFileApi = async (file) => {
  const formData = new FormData()
  formData.append('file', file)
  return await APIUtils.postFormData(
    `${config.apiEndpoint}/upload/files`,
    formData,
  )
}
