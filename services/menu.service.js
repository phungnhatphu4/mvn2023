import apolloClient from 'apollo-client'
import { LIST_MENUS } from 'graphql/menu'

export function getListMenus(branchId) {
  return apolloClient.query({
    query: LIST_MENUS,
    variables: {
      branchId,
    },
  })
}
