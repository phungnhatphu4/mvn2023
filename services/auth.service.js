/* eslint-disable no-return-await */
// import { API_URL } from 'src/constants'
import getConfig from 'next/config'

import APIUtils from '../utils/apiUtils'
import { getUserInfoQuery } from '../graphql/auth'
import apolloClient from '../apollo-client'

const { publicRuntimeConfig } = getConfig()

const API_URL = publicRuntimeConfig.apiEndpoint

export function getUserInfoApi() {
  return apolloClient.query({
    query: getUserInfoQuery,

    context: {
      service: 'auth',
    },
  })
}

const getDeviceTokenApi = async (data) => {
  return await APIUtils.post(`${API_URL}/device_tokens`, data, {
    'Content-Type': 'application/json',
  })
}

const loginApi = async (data, header) => {
  return await APIUtils.post(`${API_URL}/phone/verify_password`, data, {
    'Content-Type': 'application/json',
    ...header,
  })
}

const requestOtpToRegisterApi = async (data, header) => {
  return await APIUtils.post(`${API_URL}/phone/request_phone_ownership`, data, {
    'Content-Type': 'application/json',
    ...header,
  })
}

const verifyOtpApi = async (data, header) => {
  return await APIUtils.post(`${API_URL}/phone/verify_otp`, data, {
    'Content-Type': 'application/json',
    ...header,
  })
}

const createPasswordApi = async (data, header) => {
  return await APIUtils.post(`${API_URL}/user/update`, data, {
    'Content-Type': 'application/json',
    ...header,
  })
}

const requestOtpToResetPasswordApi = async (data, header) => {
  return await APIUtils.post(`${API_URL}/phone/request_reset_password`, data, {
    'Content-Type': 'application/json',
    ...header,
  })
}

const verifyOtpToResetPasswordApi = async (data, header) => {
  return await APIUtils.post(
    `${API_URL}/phone/verify_otp_reset_password`,
    data,
    {
      'Content-Type': 'application/json',
      ...header,
    },
  )
}

const resetPasswordApi = async (data, header) => {
  return await APIUtils.post(`${API_URL}/user/update`, data, {
    'Content-Type': 'application/json',
    ...header,
  })
}

const changePasswordApi = async (data) => {
  return await APIUtils.post(`${API_URL}/user/update_password`, data)
}

const checkExistPhoneNumberApi = async (phone) => {
  return await APIUtils.get(
    `${API_URL}/authen/check_phone?phone_number=${phone}`,
  )
}

const verifyTokenFirebaseApi = async (
  { phone, token, password, name },
  header,
) => {
  return await APIUtils.post(
    `${API_URL}/authen/verify_firebase`,
    {
      name,
      phone_number: phone,
      token,
      password,
    },
    {
      'Content-Type': 'application/json',
      ...header,
    },
  )
}

const verifyTokenFirebaseResetPasswordApi = async (
  { phone, token },
  header,
) => {
  return await APIUtils.post(
    `${API_URL}/authen/verify_reset`,
    {
      phone_number: phone,
      token,
    },
    {
      'Content-Type': 'application/json',
      ...header,
    },
  )
}

const updateUserInfoApi = async (data, header) => {
  return await APIUtils.post(`${API_URL}/user/update`, data, {
    'Content-Type': 'application/json',
    ...header,
  })
}

const checkPhoneNumberApi = async (phone, header) => {
  return await APIUtils.get(
    `${API_URL}/authen/check_phone?phone_number=${phone}`,
    null,
    header,
  )
}

const AuthService = {
  getDeviceTokenApi,
  loginApi,
  requestOtpToRegisterApi,
  verifyOtpApi,
  createPasswordApi,
  updateUserInfoApi,
  verifyOtpToResetPasswordApi,
  resetPasswordApi,
  requestOtpToResetPasswordApi,
  changePasswordApi,
  checkExistPhoneNumberApi,
  verifyTokenFirebaseApi,
  verifyTokenFirebaseResetPasswordApi,
  checkPhoneNumberApi,
}

export default AuthService
