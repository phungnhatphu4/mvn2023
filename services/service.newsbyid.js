import apolloClient from 'apollo-client'
import { GET_NEWS_DATA_BY_ID } from 'graphql/newsbyid'


export function getNewsDataById(id) {
  return apolloClient.query({
    query: GET_NEWS_DATA_BY_ID,
    variables: {
      id,
    },
  })
}
