import apolloClient from 'apollo-client'
import { GET_SERVICE_DATA_BY_ID } from 'graphql/servicebyid'


export function getServiceDataById(id) {
  return apolloClient.query({
    query: GET_SERVICE_DATA_BY_ID,
    variables: {
      id,
    },
  })
}
