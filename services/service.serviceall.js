import apolloClient from 'apollo-client'
import { GET_SERVICE_DATA_ALL } from 'graphql/serviceall'

export function getServiceDataAll() {
  return apolloClient.query({
    query: GET_SERVICE_DATA_ALL,
  })
}
