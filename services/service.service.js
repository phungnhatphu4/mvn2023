import apolloClient from 'apollo-client'
import { GET_SERVICE_DATA } from 'graphql/service'
export function getServiceData(category_id) {
  return apolloClient.query({
    query: GET_SERVICE_DATA,
    variables: {
      category_id,
    },
  })
}
