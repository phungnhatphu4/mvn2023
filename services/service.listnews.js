import apolloClient from 'apollo-client'
import { GET_NEWS_DATA } from 'graphql/news'
export function getNewsData(news_type) {
  return apolloClient.query({
    query: GET_NEWS_DATA,
    variables: {
      news_type,
    },
  })
}
