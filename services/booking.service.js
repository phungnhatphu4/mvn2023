import config from 'config'
import APIUtils from 'utils/apiUtils'

export const bookingApi = async (params) => {
  return await APIUtils.post(`${config.apiEndpoint}/bookings`, params, {
    'Content-Type': 'application/json',
  })
}
