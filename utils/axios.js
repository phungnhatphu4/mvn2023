import axios from 'axios'
import getConfig from 'next/config'
import { getAccessTokenFromLocal } from './localStorage'
const { publicRuntimeConfig } = getConfig()

const createService = (config, skipInterceptorResponse = false) => {
  const instance = axios.create(config)

  instance.interceptors.request.use((requestConfig) => {
    const token = getAccessTokenFromLocal()
    const { headers = {} } = requestConfig
    if (token) {
      headers['Authorization'] = 'Bearer ' + token
    }

    return { ...requestConfig, headers }
  })

  if (!skipInterceptorResponse) {
    instance.interceptors.response.use(
      (response) => response, // wrap response, error
      (error) => error,
    )
  }
  return instance
}

const axiosService = createService({
  baseURL: publicRuntimeConfig.apiEndpoint,
  headers: {
    'Content-Type': 'application/json',
  },
  timeout: 60000,
})
export default axiosService
