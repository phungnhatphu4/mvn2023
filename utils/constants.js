export const DISPLAY_TYPE = {
  ENTER_PHONE: 'enter-phone',
  ENTER_OTP: 'enter-otp',
  ENTER_PASSWORD: 'enter-password',
  ENTER_USER_INFO: 'enter-user-info',
}
