/* eslint-disable no-useless-return */
/* eslint-disable prefer-promise-reject-errors */
import axios from 'axios'

const REQUEST_TIMEOUT = 10000

export default class APIUtils {
  accessTokenUser = ''

  constructor() {
    this.accessToken = ''
  }

  static setAccessToken(token) {
    console.log('token>>>', token)
    this.accessToken = `${token}`
  }

  static getAccessToken() {
    return this.accessToken
  }

  static get(uri, params, headers) {
    console.log(
      '>>>>>request',

      {
        uri,
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.accessToken}`,
        ...headers,
      },
      params,
    )
    return new Promise((resolve, reject) => {
      let headersDefault = {
        'Content-Type': 'application/json',
      }
      if (this.accessToken) {
        headersDefault = {
          ...headersDefault,
          Authorization: `Bearer ${this.accessToken}`,
        }
      }
      return axios
        .get(uri, {
          headers: {
            ...headersDefault,
            ...headers,
          },
          params,
        })
        .then((response) => {
          console.log('>>>>>>> Response>>>>>> : ', response)
          const { data } = response
          resolve(data)
        })
        .catch((err) => {
          console.log('[error]', { err })
          reject(err)
        })
    })
  }

  static post(uri, postData, headers) {
    return new Promise((resolve, reject) => {
      console.log(
        '>>>>>>> Request post>>>>>> : ',
        uri,
        {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.accessToken}`,
          ...headers,
        },
        postData,
      )
      axios
        .post(uri, postData, {
          timeout: REQUEST_TIMEOUT,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${this.accessToken}`,
            ...headers,
          },
        })
        .then((response) => {
          console.log('>>>>>>> Response>>>>>> : ', response)
          const { data } = response
          if (response && response.status >= 200 && response.status <= 299) {
            resolve(data)
            return
          }
        })
        .catch((err) => {
          console.log('>>>>>>> Error>>>>>> : ', err)
          if (err.response) {
            reject({
              message:
                err.response?.data?.message ||
                'Có lỗi xảy ra, vui lòng thử lại',
            })
            return
          }
          reject(err)
        })
    })
  }

  static postFormData(uri, postData) {
    console.log('>>>>>>> Request>>>>>> : ', postData)
    return new Promise((resolve, reject) => {
      axios
        .post(uri, postData, {
          timeout: REQUEST_TIMEOUT,
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${this.accessToken}`,
          },
        })
        .then((response) => {
          console.log('>>>>>>> Response>>>>>> : ', response)
          const { data } = response
          resolve(data)
        })
        .catch((err) => {
          console.log('[error]', { err })
          reject(err)
        })
    })
  }

  static put(uri, updateData, headers) {
    return new Promise((resolve, reject) => {
      console.log(
        '>>>>>>> Request put>>>>>> : ',
        uri,
        {
          'Content-Type': 'application/json',
          Authorization: this.accessToken,
          ...headers,
        },
        updateData,
      )
      return axios
        .put(uri, updateData, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: this.accessToken,
          },
        })
        .then((response) => {
          const { data } = response
          console.log('>>>>>>> Response>>>>>> : ', response)

          resolve(data)
        })
        .catch((err) => {
          console.log('[error]', { err })
          reject(err)
        })
    })
  }

  static delete(uri, params, headers) {
    console.log(
      '>>>>>request',
      {
        uri,

        'Content-Type': 'application/json',
        Authorization: this.accessToken,
        ...headers,
      },
      params,
    )
    return new Promise((resolve, reject) =>
      axios
        .delete(uri, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: this.accessToken,
            ...headers,
          },
          params,
        })
        .then((response) => {
          console.log('>>>>>>> Response>>>>>> : ', response)
          const { data } = response
          resolve(data)
        })
        .catch((err) => {
          console.log('[error]', { err })
          reject(err)
        }),
    )
  }
}
