const ACCESS_TOKEN = 'token'

export function getAccessTokenFromLocal() {
  return localStorage.get(ACCESS_TOKEN)
}
