/* eslint-disable no-throw-literal */
/* eslint-disable prefer-const */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
/* eslint-disable default-case */
/* eslint-disable import/prefer-default-export */
export function* handleQuery(query) {
  const res = yield query;
  if (res.errors) {
    console.log('>>>>>handleQuery errors', res);

    for (let err of res.errors) {
      switch (err.extensions.code) {
        // Apollo Server sets code to UNAUTHENTICATED
        // when an AuthenticationError is thrown in a resolver
        case '401':
          throw { code: 'UNAUTHORIZED' };
        // Modify the operation context with a new token
      }
    }
    throw res.errors;
  }
  console.log('>>>>>handleQuery res', res);

  return res.data;
}
