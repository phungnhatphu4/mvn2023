import moment from "moment"

export const standardizedServiceData = (chooseServiceData = {}) => {
  const {
    cakeService,
    flowerService,
    freePictureService,
    hangingPictureService,
    hangingLetterService,
    spreadRoseService,
    decorationService,
  } = chooseServiceData
  const data = []
  if (cakeService?.selectedCake) {
    const { selectedCake } = cakeService
    data.push({
      fieldName: 'Bánh kem',
      fieldValue: `${selectedCake?.name} - ${selectedCake?.selectedVariant?.type}`,
      price: selectedCake?.selectedVariant?.price,
      salePrice: selectedCake?.selectedVariant?.salePrice,
    })
  }
  if (flowerService?.selectedFlower) {
    const { selectedFlower } = flowerService
    data.push({
      fieldName: 'Hoa tươi',
      fieldValue: selectedFlower?.name,
      price: selectedFlower?.price,
      salePrice: selectedFlower?.salePrice,
    })
  }
  if (freePictureService?.images?.length) {
    data.push({
      fieldName: 'Rửa hình miễn phí',
      fieldValue: `[${freePictureService.images.length} hình]`,
      price: freePictureService?.price ?? 0,
      salePrice: freePictureService?.salePrice ?? 0,
    })
  }
  if (hangingPictureService?.images?.length) {
    data.push({
      fieldName: 'Rửa hình treo dây',
      fieldValue: `[${hangingPictureService.images.length} hình]`,
      price: hangingPictureService?.price ?? 0,
      salePrice: hangingPictureService?.salePrice ?? 0,
    })
  }
  if (hangingLetterService) {
    data.push({
      fieldName: 'Chữ theo yêu cầu',
      fieldValue: hangingLetterService?.text,
      price: hangingLetterService?.price,
      salePrice: hangingLetterService?.salePrice,
    })
  }
  if (spreadRoseService) {
    data.push({
      fieldName: 'Rải hoa hồng trên bàn',
      fieldValue: 'Rải hoa hồng trên bàn',
      price: spreadRoseService?.price,
      salePrice: spreadRoseService?.salePrice,
    })
  }
  if (decorationService) {
    data.push({
      fieldName: 'Dịch vụ trang trí',
      fieldValue: 'Dịch vụ trang trí',
      price: decorationService?.price,
      salePrice: decorationService?.salePrice,
    })
  }
  return data
}

export const standardizedServiceDataApi = (chooseServiceData = {}) => {
  const {
    cakeService,
    flowerService,
    freePictureService,
    hangingPictureService,
    hangingLetterService,
    spreadRoseService,
    decorationService,
  } = chooseServiceData
  const data = []
  if (cakeService?.selectedCake) {
    const { selectedCake, candle, message } = cakeService
    data.push({
      id: selectedCake?.id,
      deal_ext_id: selectedCake?.selectedVariant?.id,
      ...(candle && {
        candle: {
          type: candle.label,
          number: candle.number,
        },
      }),
      text: message,
    })
  }
  if (flowerService?.selectedFlower) {
    const { selectedFlower, message, smallCardMessage } = flowerService
    data.push({
      id: selectedFlower?.id,
      text: message,
      card_text: smallCardMessage,
    })
  }
  if (freePictureService?.images?.length) {
    data.push({
      id: freePictureService?.id,
      image_urls: freePictureService?.images,
    })
  }
  if (hangingPictureService?.images?.length) {
    data.push({
      id: hangingPictureService?.id,
      image_urls: hangingPictureService?.images,
    })
  }
  if (hangingLetterService) {
    data.push({
      id: hangingLetterService?.id,
      text: hangingLetterService?.text,
    })
  }
  if (spreadRoseService) {
    data.push({
      id: spreadRoseService?.id,
    })
  }
  if (decorationService) {
    data.push({
      id: decorationService?.id,
    })
  }
  return data
}

export const standardizedStepChooseBranchData = (chooseBranchData = {}) => {
  const { selectedBranch, startTime, checkInTime, numberPeople, partyType } =
    chooseBranchData
  const data = []
  if (selectedBranch) {
    data.push({
      fieldName: 'Chi nhánh',
      fieldValue: selectedBranch.name,
    })
  }
  if (startTime && checkInTime) {
    data.push({
      fieldName: 'Thời gian đặt bàn',
      fieldValue: `${moment(startTime).format('HH:mm')} - ${moment(
        checkInTime,
      ).format('DD/MM/YYYY')}`,
    })
  }
  if (partyType) {
    data.push({
      fieldName: 'Loại tiệc',
      fieldValue: partyType,
    })
  }
  if (numberPeople) {
    data.push({
      fieldName: 'Số người',
      fieldValue: numberPeople,
    })
  }
  return data
}

export const standardizedStepChoosePositionData = (choosePositionData = {}) => {
  const data = []
  if (choosePositionData) {
    data.push({
      fieldName: 'Không gian',
      fieldValue: choosePositionData.name,
    })
  }
  return data
}

export const standardizedStepChooseMenuData = (chooseMenuData = {}) => {
  const data = []
  if (chooseMenuData) {
    data.push({
      fieldName: 'Set Menu',
      fieldValue: chooseMenuData.name,
      price: chooseMenuData.price,
      salePrice: chooseMenuData.salePrice,
    })
  }
  return data
}
