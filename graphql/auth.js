import gql from 'graphql-tag'

export const getUserInfoQuery = gql`
  query get_user {
    get_user {
      id
      name
      image_url
      gender
      email
      dob
      address
      phone_number
      is_insurance
      insurance_card_code
      balance
      insurance_created_at
      point
    }
  }
`
