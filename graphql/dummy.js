import { gql } from '@apollo/client'

export const dummyQuery = gql`
  query NextLaunch {
    launchNext {
      mission_name
      launch_date_local
      launch_site {
        site_name_long
      }
    }
  }
`
