import { gql } from '@apollo/client'

export const GET_NEWS_DATA = gql`
  query list_news($news_type: Int) {
    list_news(
      size: 1000
      filter: { news_type: $news_type }
    ) {
      data {
        id
        name
        cover_img
        description
      }
    }
  }
`

