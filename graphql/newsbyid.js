import { gql } from '@apollo/client'

export const GET_NEWS_DATA_BY_ID = gql`
  query get_news($id:Int!) {
    get_news(
      id: $id
    ) {
      
        id
        name
        cover_img
       
        description
    }
  }
`
