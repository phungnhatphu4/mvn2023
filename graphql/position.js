import { gql } from '@apollo/client'

export const LIST_POSITIONS = gql`
  query list_deals($branchId: Int) {
    list_deals(
      size: 1000
      filter: { type_deal: "position", branch_id: $branchId }
    ) {
      data {
        id
        name
        image_urls
        number_of_tables
      }
    }
  }
`
export const LIST_BOOKED_POSITIONS = gql`
  query list_served_positions($branchId: Int!, $checkInTime: String) {
    list_served_positions(
      filter: { check_in_time: $checkInTime, branch_id: $branchId }
    ) {
      data {
        deal_id
        booked_count
      }
    }
  }
`
