import { gql } from '@apollo/client'

export const GET_LIST_BRANCHES = gql`
  query GetListBranch {
    list_branches(page: 1, size: 3) {
      data {
        id
        name
        image_url
        image_urls
        address
        description
        open_time
        close_time
        rating
        feedback_link
        number_of_feedback
      }
      metadata {
        total
      }
    }
  }
`
