import { gql } from '@apollo/client'

export const GET_SERVICE_DATA_ALL = gql`
  query list_deals {
    list_deals(
      size: 1000
      filter: { type_deal: "service"}
    ) {
      data {
        id
        name
        image_urls
        sale_price
        price
        description
        deal_exts {
          id
          name
          sale_price
          price
          image_urls
        }
      }
    }
  }
`
