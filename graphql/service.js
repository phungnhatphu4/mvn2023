import { gql } from '@apollo/client'

export const GET_SERVICE_DATA = gql`
  query list_deals($category_id: Int) {
    list_deals(
      size: 1000
      filter: { type_deal: "service", category_id: $category_id }
    ) {
      data {
        id
        name
        image_urls
        sale_price
        price
        description
        deal_exts {
          id
          name
          sale_price
          price
          image_urls
        }
      }
    }
  }
`

