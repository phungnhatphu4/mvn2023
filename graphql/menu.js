import { gql } from '@apollo/client'

export const LIST_MENUS = gql`
  query {
    list_deals(size: 1000, filter: { type_deal: "menu" }) {
      data {
        id
        name
        image_urls
        sale_price
        price
        description
      }
    }
  }
`
