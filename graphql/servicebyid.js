import { gql } from '@apollo/client'

export const GET_SERVICE_DATA_BY_ID = gql`
  query get_deal($id:Int!) {
    get_deal(
      id: $id
    ) {
      
        id
        name
        image_urls
        sale_price
        price
        description
        deal_exts {
          id
          name
          sale_price
          price
          image_urls
        }
      
    }
  }
`
